-- phpMyAdmin SQL Dump
-- version 4.9.4
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Dec 21, 2020 at 02:23 AM
-- Server version: 5.6.49-cll-lve
-- PHP Version: 7.3.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `meets_app`
--

-- --------------------------------------------------------

--
-- Table structure for table `tb_admin`
--

CREATE TABLE `tb_admin` (
  `id` int(11) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `reg_date` datetime NOT NULL,
  `last_login` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tb_admin`
--

INSERT INTO `tb_admin` (`id`, `username`, `password`, `reg_date`, `last_login`) VALUES
(0, 'meetsAdmin', 'e10adc3949ba59abbe56e057f20f883e', '2020-10-08 17:39:20', '2020-10-08 17:39:20');

-- --------------------------------------------------------

--
-- Table structure for table `tb_attr`
--

CREATE TABLE `tb_attr` (
  `attr_id` int(255) NOT NULL,
  `user_id` int(255) NOT NULL,
  `attribute` varchar(100) CHARACTER SET utf8 NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_attr`
--

INSERT INTO `tb_attr` (`attr_id`, `user_id`, `attribute`) VALUES
(1, 1, 'ストレート'),
(2, 3, 'ストレート'),
(3, 2, 'ネコ'),
(4, 3, 'ネコ'),
(5, 7, 'ネコ'),
(6, 9, ' ストレート'),
(7, 8, ' クエスチョニング'),
(8, 12, 'タチ '),
(9, 11, ' ネコ '),
(10, 10, ' リバ'),
(11, 1, 'ストレート'),
(12, 2, 'ネコ'),
(13, 3, 'ストレート'),
(14, 3, 'ネコ'),
(15, 6, ' クエスチョニング'),
(16, 5, ' ストレート'),
(17, 4, 'ネコ'),
(18, 14, 'タチ '),
(19, 16, ' ネコ '),
(20, 18, ' リバ'),
(21, 1, 'ストレート'),
(22, 11, ' ネコ '),
(23, 12, 'タチ '),
(24, 15, 'タチ '),
(25, 17, ' ネコ '),
(26, 19, ' リバ'),
(27, 20, 'タチ '),
(28, 20, ' ネコ '),
(29, 20, ' リバ'),
(30, 21, 'タチ '),
(31, 21, ' ネコ '),
(32, 21, ' バイセクシュアル'),
(33, 22, 'タチ '),
(34, 22, ' ネコ '),
(35, 23, ' リバ'),
(36, 24, 'ネコ'),
(37, 25, ' ストレート'),
(38, 26, ' クエスチョニング'),
(39, 29, 'タチ '),
(40, 29, ' ネコ'),
(41, 30, 'ネコ '),
(42, 30, ' リバ'),
(43, 31, 'ネコ '),
(44, 31, ' リバ'),
(45, 32, 'ネコ '),
(46, 32, ' リバ '),
(47, 32, ' バイセクシュアル'),
(48, 33, 'ネコ '),
(49, 33, ' リバ'),
(50, 34, 'パンセクシュアル '),
(51, 34, ' ストレート'),
(52, 35, 'バイセクシュアル '),
(53, 35, ' パンセクシュアル'),
(54, 36, 'バイセクシュアル '),
(55, 36, ' パンセクシュアル');

-- --------------------------------------------------------

--
-- Table structure for table `tb_blockusers`
--

CREATE TABLE `tb_blockusers` (
  `id` int(11) NOT NULL,
  `user_id` varchar(255) CHARACTER SET utf8 NOT NULL,
  `blocks` varchar(255) CHARACTER SET utf8 NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_blockusers`
--

INSERT INTO `tb_blockusers` (`id`, `user_id`, `blocks`) VALUES
(2, '5fd1d592f19b1535b3730b4a', ''),
(3, '5b8c4526633635082db7c12d', ''),
(6, '5f22d9cdd2c7032c76cf9457', ''),
(5, '5f2ad841b573b835e52201c7', '5fd1d592f19b1535b3730b4a');

-- --------------------------------------------------------

--
-- Table structure for table `tb_comment`
--

CREATE TABLE `tb_comment` (
  `id` int(11) NOT NULL,
  `post_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `user_name` varchar(50) CHARACTER SET utf8 NOT NULL,
  `user_photo` varchar(150) CHARACTER SET utf8 NOT NULL,
  `content` varchar(800) CHARACTER SET utf8 NOT NULL,
  `created_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tb_follow`
--

CREATE TABLE `tb_follow` (
  `id` int(11) NOT NULL,
  `owner_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `user_name` varchar(50) CHARACTER SET utf8 NOT NULL,
  `user_photo` varchar(150) CHARACTER SET utf8 NOT NULL,
  `created_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_follow`
--

INSERT INTO `tb_follow` (`id`, `owner_id`, `user_id`, `user_name`, `user_photo`, `created_at`) VALUES
(1, 14, 35, 'Aimiko', 'http://meets.email/uploadfiles/userphoto/1603858575990.png', '2020-12-09 03:55:39');

-- --------------------------------------------------------

--
-- Table structure for table `tb_like`
--

CREATE TABLE `tb_like` (
  `id` int(11) NOT NULL,
  `post_id` int(11) NOT NULL,
  `owner_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `user_name` varchar(50) CHARACTER SET utf8 NOT NULL,
  `user_photo` varchar(150) CHARACTER SET utf8 NOT NULL,
  `created_at` datetime NOT NULL,
  `read_state` varchar(10) CHARACTER SET utf8 NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tb_notification`
--

CREATE TABLE `tb_notification` (
  `id` int(11) NOT NULL,
  `owner_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `user_name` varchar(50) CHARACTER SET utf8 NOT NULL,
  `user_photo` varchar(150) CHARACTER SET utf8 NOT NULL,
  `type` varchar(10) CHARACTER SET utf8 NOT NULL,
  `created_at` datetime NOT NULL,
  `read_state` varchar(10) CHARACTER SET utf8 NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_notification`
--

INSERT INTO `tb_notification` (`id`, `owner_id`, `user_id`, `user_name`, `user_photo`, `type`, `created_at`, `read_state`) VALUES
(1, 14, 35, 'Aimiko', 'http://meets.email/uploadfiles/userphoto/1603858575990.png', 'follow', '2020-12-09 03:55:39', 'false');

-- --------------------------------------------------------

--
-- Table structure for table `tb_post`
--

CREATE TABLE `tb_post` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `user_name` varchar(50) CHARACTER SET utf8 NOT NULL,
  `user_photo` varchar(150) CHARACTER SET utf8 NOT NULL,
  `created_at` datetime NOT NULL,
  `post_content` text CHARACTER SET utf8 NOT NULL,
  `user_location` varchar(100) CHARACTER SET utf8 NOT NULL,
  `user_birthday` varchar(100) CHARACTER SET utf8 NOT NULL,
  `to_send` varchar(100) CHARACTER SET utf8 NOT NULL,
  `follower_number` varchar(100) CHARACTER SET utf8 NOT NULL,
  `about_me` varchar(100) CHARACTER SET utf8 NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_post`
--

INSERT INTO `tb_post` (`id`, `user_id`, `user_name`, `user_photo`, `created_at`, `post_content`, `user_location`, `user_birthday`, `to_send`, `follower_number`, `about_me`) VALUES
(3, 16, 'Erika', 'http://meets.email/uploadfiles/userphoto/1602144215525.png', '2020-10-08 17:12:03', '多た博歩びんえゃ容葉カヲ掲演けや文省権ムイオサ阪織きず式行レ水年カニヨリ自供ヲヤ棋結属テハユレ成69系食ヱアネ親読ぎーぽお優雄げれ。', 'Nagoya', '1998-12-21', '', '0', '箸より重いものを持ったことがない'),
(16, 6, 'Gyamamoto', 'http://meets.email/uploadfiles/userphoto/1602143047727.png', '2020-10-08 17:19:23', '命れてい知48芸なトス今模い下政ムテ発向スだ判意わトか碁71著地座7横か況身レマヲ宅占祉ルまづ。', 'Osaka', '1996-10-17', 'To: Kumiko', '0', '耳にたこができる');

-- --------------------------------------------------------

--
-- Table structure for table `tb_purpose`
--

CREATE TABLE `tb_purpose` (
  `purpose_id` int(255) NOT NULL,
  `purpose_user_id` int(255) NOT NULL,
  `purpose` varchar(100) CHARACTER SET utf8 NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tb_user`
--

CREATE TABLE `tb_user` (
  `id` int(11) NOT NULL,
  `username` varchar(100) CHARACTER SET utf8 NOT NULL,
  `password` varchar(100) CHARACTER SET utf8 NOT NULL,
  `picture` varchar(150) CHARACTER SET utf8 NOT NULL,
  `follower` varchar(10) CHARACTER SET utf8 NOT NULL,
  `notification` varchar(5) CHARACTER SET utf8 NOT NULL,
  `token` varchar(250) CHARACTER SET utf8 NOT NULL,
  `created_at` datetime NOT NULL,
  `about_me` text CHARACTER SET utf8 NOT NULL,
  `user_location` varchar(100) CHARACTER SET utf8 NOT NULL,
  `user_birthday` date NOT NULL,
  `user_gender` varchar(100) CHARACTER SET utf8 NOT NULL,
  `latitude` varchar(100) NOT NULL,
  `longitude` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_user`
--

INSERT INTO `tb_user` (`id`, `username`, `password`, `picture`, `follower`, `notification`, `token`, `created_at`, `about_me`, `user_location`, `user_birthday`, `user_gender`, `latitude`, `longitude`) VALUES
(35, 'Aimiko', '$2y$10$E6J4moVPv3bGaI/o8xYwROB8mqGY1jxqngNkH4ndQX5hD2udD29y2', 'http://meets.email/uploadfiles/userphoto/1603858575990.png', '1', '1', '', '2020-10-27 21:16:15', 'I am going to find friend here.', 'Tokyo', '2000-10-28', 'MTF', '35.6803997', '139.7690174'),
(36, 'Misaiko', '$2y$10$unE80m5FXj2.EIPGKtQpM.bo/kill0jjtq8sT2OHhXQQEPO8pcQ/S', 'http://meets.email/uploadfiles/userphoto/1603861620201.png', '0', '1', '', '2020-10-27 22:07:00', 'I am going to find friend here.', 'Osaka', '1998-10-28', 'FTM', '34.6937249', '135.5022535');

-- --------------------------------------------------------

--
-- Table structure for table `tb_user1`
--

CREATE TABLE `tb_user1` (
  `id` int(11) NOT NULL,
  `user_id` varchar(255) NOT NULL,
  `token` varchar(255) NOT NULL,
  `joined_date` varchar(255) NOT NULL,
  `is_plus_member` varchar(255) NOT NULL,
  `isblock_video_verify` varchar(255) NOT NULL,
  `isblock_chat_request` varchar(255) NOT NULL,
  `snooze_account` varchar(255) NOT NULL,
  `isblock_random_chat` varchar(255) NOT NULL,
  `isblock_video_chat` varchar(255) NOT NULL,
  `isblock_audio_chat` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_user1`
--

INSERT INTO `tb_user1` (`id`, `user_id`, `token`, `joined_date`, `is_plus_member`, `isblock_video_verify`, `isblock_chat_request`, `snooze_account`, `isblock_random_chat`, `isblock_video_chat`, `isblock_audio_chat`) VALUES
(34, '5eeba969feaa9256ed6eeb85', 'cKq94XWj4ExJrZ5r2VIAuy:APA91bGvjjDMJBRbzff1_vQOn6YzyzGn2A1FJZkTGB1Z-rctcMkZyUoIeIz9a16EwR9cpnrG2kRFy5Ipj0YgVOieyMrK2bj96pWmmj9ARQtqkF1sTxfL1O6dLveCkJfq3zs6FJrqxYV8', '1607606246000', 'false', '', '', '', '', '', ''),
(35, '5b8c4526633635082db7c12d', 'dBYee6AA4UjJvQCnmjHkvw:APA91bEzHts5Lj2eoiWQO5-nRhfKGdzwIgkeqo0dWQhFhrZaKG4-sNAJDBtkc8bxGD5jsFijng47Gp0RoNs0fxTQ0HM19BXoon4HHX0qoKso-M0Jhcyw_x62doMOOO2dAbO2bWZG59BU', '1607606735000', 'false', '', '', '1', '', '', ''),
(74, '5fd1d592f19b1535b3730b4a', '', '1607606735000', 'false', '0', '0', '0', '1', '1', '1'),
(73, '5f2ad841b573b835e52201c7', 'dkwRRsgj7k66uj4EuTz9IC:APA91bGpwOpd4ZmQh-5o3w8rg-fu8mJjj8pbDYj13Epej684JKX9O_dE_thb9tXCXwvqK75o4e8cG678DAyWZnQnZzJZvAvzxXvLO1fhKn4oURlEan4mHCVQcB_T6OvM5IYFu-ZoA80O', '1608387176000', 'false', '', '', '', '', '', ''),
(65, '5f44d4a66ff81c123af2b923', 'fBwJ4Gk2ck9tlDr1Crs6g6:APA91bEOShD7i_Hq8qemWP2Ubcor0RTwUlNtttQz0vR48ErW_CjrPCNNiDCqXsmDeASuzPA4znmFZ_-0QVg809bUmGIEaYlKngWrCUCyTdmzCCzXPoAgb6v7AhCXkMVoE6bwwZ9g9NXD', '1608306134000', 'false', '1', '', '', '', '', ''),
(76, '5f22d9cdd2c7032c76cf9457', 'fKjrsiJXYk_6pucMCUjDcb:APA91bH8yzzXShUBCPxkMFmdWxtoYG3YcziZL0BDP-AJ-gBYPNPZReCj09AWjhz2WD-v3qW2XWaH7IEawfSm0u_sdOrrolvuV8jkOyENX-I1dqwo31IpTmg-hGjaHhBp_wCQqPk9VXSZ', '1608502856000', 'false', '', '', '1', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `tb_video_verify`
--

CREATE TABLE `tb_video_verify` (
  `id` int(100) NOT NULL,
  `sender_id` varchar(255) CHARACTER SET utf8 NOT NULL,
  `receiver_id` varchar(255) CHARACTER SET utf8 NOT NULL,
  `sender_status` int(100) NOT NULL,
  `receiver_status` int(100) NOT NULL,
  `verify_room_id` varchar(255) CHARACTER SET utf8 NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_video_verify`
--

INSERT INTO `tb_video_verify` (`id`, `sender_id`, `receiver_id`, `sender_status`, `receiver_status`, `verify_room_id`) VALUES
(21, '5f22d9cdd2c7032c76cf9457', '5fd1d592f19b1535b3730b4a', -1, -1, '5fd1d592f19b1535b3730b4a_5f22d9cdd2c7032c76cf9457');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tb_admin`
--
ALTER TABLE `tb_admin`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_attr`
--
ALTER TABLE `tb_attr`
  ADD PRIMARY KEY (`attr_id`);

--
-- Indexes for table `tb_blockusers`
--
ALTER TABLE `tb_blockusers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_comment`
--
ALTER TABLE `tb_comment`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_follow`
--
ALTER TABLE `tb_follow`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_like`
--
ALTER TABLE `tb_like`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_notification`
--
ALTER TABLE `tb_notification`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_post`
--
ALTER TABLE `tb_post`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_purpose`
--
ALTER TABLE `tb_purpose`
  ADD PRIMARY KEY (`purpose_id`);

--
-- Indexes for table `tb_user`
--
ALTER TABLE `tb_user`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_user1`
--
ALTER TABLE `tb_user1`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_video_verify`
--
ALTER TABLE `tb_video_verify`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tb_blockusers`
--
ALTER TABLE `tb_blockusers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `tb_comment`
--
ALTER TABLE `tb_comment`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tb_follow`
--
ALTER TABLE `tb_follow`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tb_like`
--
ALTER TABLE `tb_like`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tb_notification`
--
ALTER TABLE `tb_notification`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tb_post`
--
ALTER TABLE `tb_post`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `tb_purpose`
--
ALTER TABLE `tb_purpose`
  MODIFY `purpose_id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=54;

--
-- AUTO_INCREMENT for table `tb_user`
--
ALTER TABLE `tb_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;

--
-- AUTO_INCREMENT for table `tb_user1`
--
ALTER TABLE `tb_user1`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=78;

--
-- AUTO_INCREMENT for table `tb_video_verify`
--
ALTER TABLE `tb_video_verify`
  MODIFY `id` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
