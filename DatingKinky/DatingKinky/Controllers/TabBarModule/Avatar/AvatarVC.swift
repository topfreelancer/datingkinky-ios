//
//  CoachVC.swift
//  DatingKinky
//
//  Created by Ubuntu on 7/24/20.
//  Copyright © 2020 Ubuntu. All rights reserved.
//

import UIKit
import SwiftyJSON

class AvatarVC: BaseVC {
    
    @IBOutlet weak var lbl_content: UILabel!
    
    var imagePickerCamera: ImagePicker!
    var imagePickerLibrary: ImagePicker!
    var pickerType: PickerType? = .normal
    var cropVC: CropVC!
    
    //public var delegate: CropPhotoDelegate?

    override func viewDidLoad() {
        
        super.viewDidLoad()
        self.addNavBarImage()
        self.addBackButtonNavBar()
        self.imagePickerCamera = ImagePicker(presentationController: self, delegate: self, cameraShow: true)
        self.imagePickerLibrary = ImagePicker(presentationController: self, delegate: self, cameraShow: false)
        
        NotificationCenter.default.addObserver(self, selector: #selector(photosend),name: NSNotification.Name(rawValue: "photosend"), object: nil)
        self.lbl_content.text = "All media go through a moderation process. \n \nThe photo you upload will be lightly watermarked by our system for your protection. Explicit photos cannot be used as avatars. \n \nFor more details and information about photo policies, protecting your privacy, and more,"
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        self.hideLoadingView()
        //NotificationCenter.default.removeObserver(self)
    }
    
    deinit {
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navBarHidden()
        self.getUploadPhotoURL()
    }
    
    func getUploadPhotoURL() {
        self.showLoadingView(vc: self)
        ApiManager.getPhotoURL { (isSuccess, data) in
            self.hideLoadingView()
            if isSuccess{
                print(data as Any)
                let json = JSON(data as Any)
                let dataone = json["data"].object
                let jsondata = JSON(dataone as Any)
                // send fileName and url for the upload action in the cropPhotoVC
                fileName = jsondata["filename"].stringValue
                fileURL = jsondata["url"].stringValue
                //fileURL = fileURL!.removingPercentEncoding
                fileId = jsondata["_id"].stringValue
                print("this is upload photo name and  URL===>", "\(fileName ?? "") \n \(fileURL ?? "")\n \(fileId ?? "")")
            }else{
                
            }
        }
    }

    
    @IBAction func cameraBtnClicked(_ sender: Any) {
        self.pickerType = .camera
        self.imagePickerCamera.present(from: self.view)
    }
    
    @IBAction func galleryBtnClicked(_ sender: Any) {
        self.pickerType = .library
        self.imagePickerLibrary.present(from: self.view)
        //self.openMenu(.photoCrop)
    }
    @IBAction func gotoPhotoTerms(_ sender: Any) {
        //self.gotoWebViewWithProgressBar(Constants.PHOTO_TERMS_LINK)
        self.gotoWebViewWithProgressBarModal(Constants.PHOTO_TERMS_LINK)
    }
    
    // getting local noti instead of delegate
    @objc func photosend(){ // show bottom cropphoto view
        // must set the show view, if view is hidden can't be set image
        self.dismiss(animated: true, completion: nil)
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
            //self.openMenu(.photoCrop)
            self.openCropPhotoVC()
            NotificationCenter.default.post(name:Notification.Name(rawValue: "photoSendCropPhotoVC"), object: nil)
        }
    }
    
    func openCropPhotoVC()  {
        let configuration = NBBottomSheetConfiguration(animationDuration: 0.4, sheetSize: .fixed(Constants.SCREEN_HEIGHT))
        let bottomSheetController = NBBottomSheetController(configuration: configuration)
        cropPhotoVC = (UIStoryboard(name: StoryBoards.MAIN, bundle: nil).instantiateViewController(withIdentifier: "CropPhotoVC") as! CropPhotoVC)
        cropPhotoVC.isTab = true
        bottomSheetController.present(cropPhotoVC, on: self)
    }
}

extension AvatarVC : ImagePickerDelegate {
    
    public func didSelect(image: UIImage?) {
        
        if image != nil{
            //if self.pickerType == .camera{
                tocropImage = image
                let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                self.cropVC = storyBoard.instantiateViewController( withIdentifier: "CropVC") as? CropVC
                self.cropVC!.modalPresentationStyle = .fullScreen
                self.cropVC!.cropPhotoDelegate? = self
                self.present(self.cropVC!, animated: false, completion: nil)
            /*}else{
                tocropImage = image
            }*/
            /*NotificationCenter.default.post(name:Notification.Name(rawValue: "photoSend"), object: nil)
            DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
                self.openMenu(.photoCrop)
            }*/
        }
    }
}


extension AvatarVC: CropPhotoDelegate {
    func didSelectImage(image: UIImage) {
        print("get image")
    }
}

