//
//  DeleteVC.swift
//  DatingKinky
//
//  Created by top Dev on 20.11.2020.
//  Copyright © 2020 Ubuntu. All rights reserved.
//


import UIKit
import AVFoundation

class DeleteVC: BaseVC{

    @IBOutlet weak var uiv_dlg: UIView!
    @IBOutlet weak var lbl_content: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUI()
        let downSwipe111 = UISwipeGestureRecognizer(target: self, action: #selector(swipeDown111))
        downSwipe111.direction = .down
        view.addGestureRecognizer(downSwipe111)
    }
    
    @objc func swipeDown111() {
        self.dismiss(animated: true, completion: nil)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        uiv_dlg.roundCorners([.topLeft, .topRight], radius: 20)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    func setUI()  {
        self.lbl_content.text = "If you delete your, it can't be recovered. It's gone for good.\n\nNo, \"Oops.\" No \"Can you help me get it back?\"\n\nGone. Totally. Completely.\n\nAnd we'll miss you.\n\nSo, we're just making super-sure you wanna go."
    }
    @IBAction func deleteBtnclicked(_ sender: Any) {
        self.showLoadingView(vc: self)
        ApiManager.delecteAccount { (isSuccess, message) in
            self.hideLoadingView()
            if isSuccess{
                self.gotoVC(VCs.LOGINNAV)
            }else{
                self.showToast(message ?? "Something went wrong")
            }
        }
        
    }
    @IBAction func nopeBtnClicked(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func topBtnClicked(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func spaceBtnClicked(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
}

