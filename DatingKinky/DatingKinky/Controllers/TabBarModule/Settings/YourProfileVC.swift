//
//  YourProfileVC.swift
//  DatingKinky
//
//  Created by top Dev on 10.12.2020.
//  Copyright © 2020 Ubuntu. All rights reserved.
//
import SwiftyJSON
import UIKit
import Firebase
import FirebaseAuth
import FirebaseStorage
import FirebaseDatabase
import YHPlaceHolderView

class YourProfileVC: BaseVC {
    
    @IBOutlet weak var imv_avatar: UIImageView!
    @IBOutlet weak var lbl_userName: UILabel!
    @IBOutlet weak var lbl_profileContent: UILabel!
    @IBOutlet weak var lbl_lookingFor: UILabel!
    @IBOutlet weak var lbl_orientation: UILabel!
    @IBOutlet weak var lbl_status: UILabel!
    @IBOutlet weak var lbl_relationShip: UILabel!
    @IBOutlet weak var lbl_hereFor: UILabel!
    @IBOutlet weak var lbl_kinkRole: UILabel!
    @IBOutlet weak var lbl_lastSeen: UILabel!
    @IBOutlet weak var lbl_chatcaption: UILabel!
    var ds_peoples = [PeopleModel]()// getting users with username
    var self_selectedUser: PeopleModel!
    let requestPath = Database.database().reference().child("request")
    var is_myprofle: Bool = true
    var user_name: String?
    var user_id: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navBarHidden()
        if self.is_myprofle{
            self.lbl_chatcaption.text = "CHAT REQUEST"
            self.getUserProfile()
        }else{
            if let userid = self.user_id,let username = self.user_name{
                self.lbl_chatcaption.text = "CHAT START"
                self.getOtherUserProfile(userid, username: username)
            }
        }
    }
    
    func getOtherUserProfile(_ userid: String, username: String) {
        self.showLoadingView(vc: self)
        ApiManager.filterByUserName(username: username) { (isSuccess, data) in
            self.hideLoadingView()
            if isSuccess{
                //print(data as Any)
                //self.ds_peoples.removeAll()
                let json = JSON(data as Any)
                //print(json)
                let dataobjectArr = json["data"].arrayObject
                if let arr = dataobjectArr{
                    self.ds_peoples.removeAll()
                    var num = 0
                    for one in arr{
                        num += 1
                        let onePeople = JSON(one as Any)
                        let id = onePeople["_id"].stringValue
                        let userName = onePeople["username"].stringValue
                        let realName = onePeople["displayName"].stringValue
                        let email = onePeople["email"].stringValue
                        let currentAge = onePeople["currentAge"].stringValue
                        let isPlusMember = onePeople["hasPlusMembership"].boolValue
                        let lastActive = onePeople["lastActive"].stringValue
                        var timeAgo = ""
                        let now = Date()
                        print(now)
                        let calendar = Calendar.current
                        if let lastDate = getDateSpecialFormatFromString(strDate: lastActive){
                            print(lastDate as Any)
                            let ageComponents = calendar.dateComponents([.second], from: lastDate, to: now)
                            let seconds = ageComponents.second ?? 0
                            print(seconds)
                            let day = secondsToHoursMinutesSeconds(seconds: seconds).0
                            let hour = secondsToHoursMinutesSeconds(seconds: seconds).1
                            let min = secondsToHoursMinutesSeconds(seconds: seconds).2
                            let secondss = secondsToHoursMinutesSeconds(seconds: seconds).3
                            if secondss > 0{
                                timeAgo = "\(secondss)" + "s"
                            }
                            if min > 0{
                                timeAgo = "\(min)" + "m"
                            }
                            if hour > 0{
                                timeAgo = "\(hour)" + "h"
                            }
                            if day > 0{
                                timeAgo = "\(day)" + "d"
                            }
                        }
                        
                        
                        let profile = onePeople["profile"].object
                        let profilejson = JSON(profile as Any)
                        
                        let location = profilejson["location"].stringValue
                        let genderObject = profilejson["gender"].object
                        let genderjson = JSON(genderObject as Any)
                        let gender = genderjson["lowerCaseName"].stringValue
                        let kinkRoleObject = profilejson["kinkRole"].object
                        let kinkRolejson = JSON(kinkRoleObject as Any)
                        let kinkRole = kinkRolejson["name"].stringValue
                        
                        let pronounObject = profilejson["pronoun"].object
                        let pronounjson = JSON(pronounObject as Any)
                        let pronoun = pronounjson["name"].stringValue
                        
                        let matchingObject = profilejson["matching"].object
                        let matchingjson = JSON(matchingObject as Any)
                        let lookingForgenderObjectArr = matchingjson["genders"].arrayObject
                        
                        var lookingForgenders = [String]()
                        if let lookingForgenderObjectArr = lookingForgenderObjectArr{
                            lookingForgenders.removeAll()
                            for one in lookingForgenderObjectArr{
                                let jsonOne = JSON(one)
                                lookingForgenders.append(jsonOne["lowerCaseName"].stringValue)
                            }
                        }
                        
                        let orientationsObjectArr = matchingjson["orientations"].arrayObject
                        
                        var orientations = [String]()
                        if let orientationsObjectArr = orientationsObjectArr{
                            orientations.removeAll()
                            for one in orientationsObjectArr{
                                let jsonOne = JSON(one)
                                orientations.append(jsonOne["lowerCaseName"].stringValue)
                            }
                        }
                        
                        let relationShipStatusObjectArr = matchingjson["relationshipStatuses"].arrayObject
                        
                        var relationshipStatus = [String]()
                        if let relationShipStatusObjectArr = relationShipStatusObjectArr{
                            relationshipStatus.removeAll()
                            for one in relationShipStatusObjectArr{
                                let jsonOne = JSON(one)
                                relationshipStatus.append(jsonOne["lowerCaseName"].stringValue)
                            }
                        }
                        
                        let relationShipStyleObjectArr = matchingjson["relationshipStyles"].arrayObject
                        
                        var relationshipStyles = [String]()
                        if let relationShipStyleObjectArr = relationShipStyleObjectArr{
                            relationshipStyles.removeAll()
                            for one in relationShipStyleObjectArr{
                                let jsonOne = JSON(one)
                                relationshipStyles.append(jsonOne["lowerCaseName"].stringValue)
                            }
                        }
                        
                        let hereForObjectArr = matchingjson["hereFor"].arrayObject
                        
                        var hereFor = [String]()
                        if let hereForObjectArr = hereForObjectArr{
                            hereFor.removeAll()
                            for one in hereForObjectArr{
                                let jsonOne = JSON(one)
                                hereFor.append(jsonOne["lowerCaseName"].stringValue)
                            }
                        }
                        
                        let kinkroleObjectArr = matchingjson["kinkRoles"].arrayObject
                        
                        var kinkRoles = [String]()
                        if let hereForObjectArr = kinkroleObjectArr{
                            kinkRoles.removeAll()
                            for one in hereForObjectArr{
                                let jsonOne = JSON(one)
                                kinkRoles.append(jsonOne["lowerCaseName"].stringValue)
                            }
                        }
                        
                        var identifies = [String]()
                        let identifiesAsArr = profilejson["identifiesAs"].arrayObject
                        if let identifiesArr = identifiesAsArr{
                            for one in identifiesArr{
                                identifies.append(JSON(one)["name"].stringValue)
                            }
                        }
                        
                        let avatar = onePeople["avatar"].object
                        let avatarjson = JSON(avatar as Any)
                        let file = avatarjson["file"].object
                        let filejson = JSON(file as Any)
                        let url = filejson["url"].object
                        let urljson = JSON(url as Any)
                        let useravatar = urljson["smallPhotoURL"].stringValue
                                                 
                        //print("one paramers  ==== > \(userName) \n \(realName) \n \(email) \n \(currentAge) \n \(isPlusMember) \n \(location) \n \(gender) \n \(useravatar)")
                        
                        self.ds_peoples.append(PeopleModel(id,realName: realName, userName: userName, email: email,pronoun: pronoun, userAvatar: useravatar, gender: gender, currentAge: currentAge, location: location, userkinkRole: kinkRole, lookingForGender: lookingForgenders, orientation: orientations, status: relationshipStatus, relationshipStyle: relationshipStyles, hereFor: hereFor, kinkRole:kinkRoles, identifiAs: identifies, isPlusMember: isPlusMember, chatRequest: false, lastActive: "Last Seen" + " " + timeAgo + " " + "ago"))
                        
                        if num == arr.count{
                            for one in self.ds_peoples{
                                if one.id == userid{
                                    self.self_selectedUser = one
                                    self.setUI()
                                }
                            }
                        }
                    }
                }else{
                    print("error")
                }
            }else{
                print("error")
            }
        }
    }
    
    func getUserProfile()  {
        self.showLoadingView(vc: self)
        ApiManager.getUserProfile { (isSuccess, data) in
            self.hideLoadingView()
            if isSuccess{
                //let datajson = JSON(JSON(data as Any))["data"].object
                self.self_selectedUser = PeopleModel(thisuser!.id!, realName: "realname", userName: thisuser!.username , email: "email", pronoun: thisuser!.pronoun , userAvatar: thisuser!.userAvatar , gender: thisuser!.gender , currentAge: thisuser!.currentAge, location: thisuser!.location, userkinkRole: thisuser!.kinkRole, lookingForGender: [], orientation: [], status: [], relationshipStyle: [], hereFor: [], kinkRole: [], identifiAs: [], isPlusMember: thisuser!.hasPlusMembership, chatRequest: false, lastActive: "now")
                self.setUI()
            }
        }
    }
    
    // test part
    /*@objc func callFirstPromotion() {
        print("callled with 20 mins")
        // first promotion show
        NotificationCenter.default.post(Notification(name: .show_20mins_promotion, object: nil))
    }*/
    
    func setUI()  {
        //TODO: maybe change later
        //selectedUser = nil
        let url = URL(string: self_selectedUser.userAvatar ?? "")
        imv_avatar.kf.setImage(with: url,placeholder: UIImage.init(named: "placeholder"))
        lbl_userName.text = self_selectedUser.userName
        let gender = self_selectedUser.gender == "woman" ? "W" : "M"
        let showhim = self_selectedUser.pronoun ?? "He/His"
        let spaces = String(repeating: " ", count: 1)
        lbl_profileContent.text = self_selectedUser.currentAge! + gender + "," + spaces + self_selectedUser.userKinkRole! + spaces + "|" + spaces + showhim + spaces + "|" + spaces + self_selectedUser.location!
        if !self.is_myprofle{
            var str_lookingForGender = " "
            var str_orientation = " "
            var str_status = " "
            var str_relationShipStyle = " "
            var str_hereFor = " "
            var str_kinkRole = " "
            
            if let common = self_selectedUser.lookingForGender{
                var num = 0
                for one in common{
                    num += 1
                    str_lookingForGender += one
                    if num != common.count{
                        let spaces = String(repeating: " ", count: 1)
                        str_lookingForGender =  str_lookingForGender + "," + spaces
                    }
                }
            }
            
            if let common = self_selectedUser.orientation{
                var num = 0
                for one in common{
                    num += 1
                    str_orientation += one
                    if num != common.count{
                        let spaces = String(repeating: " ", count: 1)
                        str_orientation = str_orientation + "," + spaces
                    }
                }
            }
            
            if let common = self_selectedUser.status{
                var num = 0
                for one in common{
                    num += 1
                    str_status += one
                    if num != common.count{
                        let spaces = String(repeating: " ", count: 1)
                        str_status = str_status + "," + spaces
                    }
                }
            }
            
            if let common = self_selectedUser.relationshipStyle{
                var num = 0
                for one in common{
                    num += 1
                    str_relationShipStyle += one
                    if num != common.count{
                        let spaces = String(repeating: " ", count: 1)
                        str_relationShipStyle = str_relationShipStyle + "," + spaces
                    }
                }
            }
            
            if let common = self_selectedUser.hereFor{
                var num = 0
                for one in common{
                    num += 1
                    str_hereFor += one
                    if num != common.count{
                        let spaces = String(repeating: " ", count: 1)
                        str_hereFor = str_hereFor + "," + spaces
                    }
                }
            }
            
            if let common = self_selectedUser.identifiAs{
                var num = 0
                for one in common{
                    num += 1
                    str_kinkRole += one
                    if num != common.count{
                        let spaces = String(repeating: " ", count: 1)
                        str_kinkRole = str_kinkRole + "," + spaces
                    }
                }
            }
            lbl_lookingFor.text = str_lookingForGender
            lbl_orientation.text = str_orientation
            lbl_status.text = str_status
            lbl_relationShip.text = str_relationShipStyle
            lbl_hereFor.text = str_hereFor
            //lbl_kinkRole.text = self.removeSpecialCharsFromString(text: str_kinkRole)
            lbl_kinkRole.text = str_kinkRole
            //print(str_kinkRole)
            lbl_lastSeen.text = self.self_selectedUser.lastActive
        }else{
            if let lookingfor = usermembership.lookingfor{
                if lookingfor.isEmpty{
                    lbl_lookingFor.text = " "
                }else{
                    lbl_lookingFor.text = usermembership.lookingfor
                }
            }
            if let orientation = usermembership.orientation{
                if orientation.isEmpty{
                    lbl_orientation.text = " "
                }else{
                    lbl_orientation.text = usermembership.orientation
                }
            }
            if let status = usermembership.status{
                if status.isEmpty{
                    lbl_status.text = " "
                }else{
                    lbl_status.text = usermembership.status
                }
            }
            if let relationship_style = usermembership.relationship_style{
                if relationship_style.isEmpty{
                    lbl_relationShip.text = " "
                }else{
                    lbl_relationShip.text = usermembership.relationship_style
                }
            }
            if let herefor = usermembership.herefor{
                if herefor.isEmpty{
                    lbl_hereFor.text = " "
                }else{
                    lbl_hereFor.text = usermembership.herefor
                }
            }
            if let kinkroles = usermembership.kinkroles{
                if kinkroles.isEmpty{
                    lbl_kinkRole.text = " "
                }else{
                    lbl_kinkRole.text = usermembership.kinkroles
                }
            }
            lbl_lastSeen.text = "now"
        }
    }
    
    func sendChatRequest()  {
        memberName4ChatRequest = self.self_selectedUser.userName
        let gender = self.self_selectedUser.gender == "woman" ? "W" : "M"
        let showhim = self.self_selectedUser.pronoun ?? "He/His"
        let kinkRole = self.self_selectedUser.userKinkRole ?? ""
        let spaces = String(repeating: " ", count: 1)
        //memberProfile4ChatRequest = self.self_selectedUser.currentAge! + gender + spaces + "|" + spaces + showhim + spaces + "|" + spaces + self.self_selectedUser.location!
        memberProfile4ChatRequest = self.self_selectedUser.currentAge! + gender + spaces + kinkRole + spaces + "•" + spaces + showhim + spaces + "•" + spaces + self.self_selectedUser.location! + spaces + "•" + spaces + "Last seen"
        partnerAge4ChatRequestAccept = self.self_selectedUser.currentAge
        partnerAvatar4ChatRequest = self.self_selectedUser.userAvatar
        partnerGender4ChatRequest = gender
        partnerKinkRole4ChatRequest = self.self_selectedUser.userKinkRole
        partnerLocation4ChatRequest = self.self_selectedUser.location
        partnerPronoun4ChatRequest = self.self_selectedUser.pronoun
        //lastSeenChatList = self.self_selectedUser.lastActive
        friendID = self.self_selectedUser.id
        //NotificationCenter.default.post(name:.chatRequest, object: nil)
        self.showBottomDlg(storyboard_name: StoryBoards.CHAT, vcname: VCs.CHAT_REQUEST_SEND)
    }
    
    @IBAction func viewFullProfileBtnClicked(_ sender: Any) {
        self.navBarShow()
        if let username = self.self_selectedUser.userName, let authToken = UserDefault.getString(key: PARAMS.X_AUTH_TOKEN, defaultValue: ""){
            let link = "https://members.datingkinky.com/auth/users?user=" + username + "&authToken=" + authToken
            self.gotoWebViewWithProgressBar(link)
        }
    }
    
    @IBAction func closeBtnClicked(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func chatStartBtnClicked(_ sender: Any) {
        if !self.is_myprofle{
            memberName4ChatRequest = self.self_selectedUser.userName
            partnerGender4ChatRequest = self.self_selectedUser.gender
            partnerLocation4ChatRequest = self.self_selectedUser.location
            partnerPronoun4ChatRequest = self.self_selectedUser.pronoun
            let spaces = String(repeating: " ", count: 1)
            let gender = partnerGender4ChatRequest! == "woman" ? "W" : "M"
            let kinkRole = self.self_selectedUser.userKinkRole!
            //memberProfile4ChatRequest = self.ds_chatRequests[index].userCurrentAge! + gender + spaces + "|" + spaces + partnerPronoun4ChatRequest! + spaces + "|" + spaces + partnerLocation4ChatRequest!
            memberProfile4ChatRequest = self.self_selectedUser.currentAge! + gender + spaces + kinkRole + spaces + "•" + spaces + partnerPronoun4ChatRequest! + spaces + "•" + spaces + partnerLocation4ChatRequest! + spaces + "•" + spaces + "Last seen"
            partnerAge4ChatRequestAccept = self.self_selectedUser.currentAge
            partnerAvatar4ChatRequest = self.self_selectedUser.userAvatar
            partnerKinkRole4ChatRequest = self.self_selectedUser.userKinkRole
            friendID = self.self_selectedUser.id!
            chattingOption = .direct
            
            // last seen show function
            lastSeenChatRequest = self.self_selectedUser.lastActive
            // send push notification for accept chatting
            self.showLoadingView(vc: self)
            ApiManager.chatRequest(receiver_id: self.self_selectedUser.id!, requestType: RequestType.chat_accept.rawValue) { (isSuccess, data) in
                self.hideLoadingView()
                if isSuccess{
                    // set firebase node value
                    self.setStatus4partner("accept", partnerId: friendID!) { (isSuccess1) in
                        if isSuccess1{
                            self.removeNode4me4accept(friendID!)
                            // MARK: for list view for my list object - - listobject
                            var listObject = [String: String]()
                            let timeNow = Int(NSDate().timeIntervalSince1970) * 1000
                            listObject["id"]   = friendID!
                            listObject["message"]        = "Hi!"
                            if let friendid = friendID{
                               listObject["sender_id"]     = friendid
                            }
                            if let senderName = memberName4ChatRequest{
                               listObject["sender_name"]    = senderName
                            }
                            if let senderPhoto = partnerAvatar4ChatRequest{
                               listObject["sender_photo"]  = senderPhoto
                            }
                            if let senderAge = partnerAge4ChatRequestAccept{
                               listObject["sender_age"]  = senderAge
                            }
                            if let senderGender = partnerGender4ChatRequest{
                               listObject["sender_gender"] = senderGender == "woman" ? "W" : "M"
                            }
                            if let senderKinkrole = partnerKinkRole4ChatRequest{
                               listObject["sender_kinkRole"]  = senderKinkrole
                            }
                            if let senderLocation = partnerLocation4ChatRequest{
                               listObject["sender_location"]  = senderLocation
                            }
                            if let senderPronoun = partnerPronoun4ChatRequest{
                               listObject["sender_pronoun"]  = senderPronoun
                            }
                            listObject["time"]           = "\(timeNow)" as String
                            let messageNumLocal = "0"
                            listObject["messageNum"]         = messageNumLocal
                            listObject["isBlock"]            = "false"
                            FirebaseAPI.sendListUpdate(listObject, "u" + "\(thisuser!.id!)", partnerid: "u" + friendID!){
                                (status,message) in
                                if status{
                                    var listObject1 = [String: String]()
                                    let partnerid   = "\(friendID ?? "0")"
                                    // MARK:  for list view for partner's list object - listobject1
                                    listObject1["id"]              = "\(thisuser!.id!)"
                                    listObject1["message"]         = "Hi!"
                                    listObject1["sender_id"]       = "\(thisuser!.id!)"
                                    listObject1["sender_name"]     = "\(thisuser!.username)"
                                    listObject1["sender_photo"]    = thisuser!.userAvatar
                                    listObject1["sender_age"]      = thisuser!.currentAge
                                    listObject1["sender_gender"]   = thisuser!.gender  == "woman" ? "W" : "M"
                                    listObject1["sender_kinkRole"] = thisuser!.kinkRole
                                    listObject1["sender_location"] = thisuser!.location
                                    listObject1["sender_pronoun"]  = thisuser!.pronoun
                                    listObject1["time"]            = "\(timeNow)" as String
                                    listObject1["messageNum"]      = messageNumLocal
                                    listObject1["isBlock"]         = "false"
                                    FirebaseAPI.sendListUpdate(listObject1, "u" + partnerid, partnerid: "u" + "\(thisuser!.id!)"){
                                       (status,message) in
                                       if status{
                                            self.gotoStoryBoardVC(StoryBoards.CHAT, name: VCs.MESSAGESENDNAV, fullscreen: true)
                                        }
                                    }
                                }
                            }
                            // self.closeMenu(.chatRequest)
                            /*self.setStatus4me("finish", partnerId: friendID!) { (isSuccess1) in
                                if isSuccess1{
                                    self.gotoStoryBoardVC(StoryBoards.CHAT, name: VCs.MESSAGESENDNAV, fullscreen: true)
                                    self.closeMenu(.chatRequest)
                                }
                            }*/
                        }
                    }
                }else{
                    self.showToast("Network issue")
                }
            }
        }
    }
    
    func removeNode4me4accept(_ partnerId: String){
        requestPath.child("u" + thisuser!.id!).child("u" + partnerId).removeValue()
    }
    
    func setStatus4partner(_ state: String, partnerId: String, completion: @escaping (_ success: Bool) -> ()) {
        requestPath.child("u" + partnerId).child("u" + thisuser!.id!).child("state").setValue(state)
        completion(true)
    }
}

