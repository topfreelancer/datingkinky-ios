//
//  AccouontInfoVC.swift
//  DatingKinky
//
//  Created by top Dev on 10.12.2020.
//  Copyright © 2020 Ubuntu. All rights reserved.
//

import UIKit

class AccouontInfoVC: BaseVC {

    @IBOutlet weak var lbl_joineddate: UILabel!
    @IBOutlet weak var lbl_plusmembershipcontent: UILabel!
    var joindate: String = ""
    var joinDate: Date!
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navBarShow()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        print(usermembership.joined_date!)
        //let joineddate = getDateFromString(strDate: usermembership.joined_date!)
        if let joined = getDateSpecialFormatFromString(strDate: usermembership.joined_date!) {
            joinDate = joined
            joindate = getStringFormDate(date: joined, format: "MMM, dd yyyy")
            self.lbl_joineddate.text = "Joined \(joindate)"
        }
        
        if let startdate = getDateSpecialFormatFromString(strDate: usermembership.trialstart ?? ""), let enddate = getDateSpecialFormatFromString(strDate: usermembership.trialend ?? ""){
            var components = DateComponents()
            components.setValue(7, for: .day)
            let expirationDate = Calendar.current.date(byAdding: components, to: joinDate)
            
            self.lbl_plusmembershipcontent.text = "PLUS Membership Trial: \(joindate)~\(getStringFormDate(date: expirationDate!, format: "MMM, dd yyyy"))\nPLUS Membership: \(getStringFormDate(date: startdate, format: "MMM, dd yyyy")) - Current\nNext renew date: \(getStringFormDate(date: enddate, format: "MMM, dd yyyy"))"
        }else{
            var components = DateComponents()
            components.setValue(7, for: .day)
            let expirationDate = Calendar.current.date(byAdding: components, to: joinDate)
            self.lbl_plusmembershipcontent.text = "PLUS Membership Trial: \(joindate)~\(getStringFormDate(date: expirationDate!, format: "MMM, dd yyyy"))\nPLUS Membership: \("")\nNext renew date: \("")"
        }
    }
    @IBAction func contactBtnClicked(_ sender: Any) {
        self.gotoWebViewWithProgressBar(Constants.CONTACTUS_LINK)
    }
}
