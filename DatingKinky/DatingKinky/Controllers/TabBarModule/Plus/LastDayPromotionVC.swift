//
//  LastDayPromotionVC.swift
//  DatingKinky
//
//  Created by top Dev on 11/1/20.
//  Copyright © 2020 Ubuntu. All rights reserved.
//

import UIKit

class LastDayPromotionVC: BaseVC {

    @IBOutlet weak var uiv_dlg: UIView!
    @IBOutlet weak var lbl_title: UILabel!
    @IBOutlet weak var lbl_content: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUI()
        let downSwipe1 = UISwipeGestureRecognizer(target: self, action: #selector(swipeDown))
        downSwipe1.direction = .down
        view.addGestureRecognizer(downSwipe1)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        uiv_dlg.roundCorners([.topLeft, .topRight], radius: 20)
    }
    
    @objc func swipeDown() {
        self.dismiss(animated: true, completion: nil)
    }
    
    func setUI()  {
        self.lbl_title.text = "Hi \(thisuser!.username ?? "Username")!"
        self.lbl_content.text = "...of your PLUS membership, that it.\n\nAnd that's cool. You'll be able to use the site as a free member without any issues. I hate when site stry to bully me into paying by handicapping their features, so I'm not going to do that to you.\n\nYou'll get keep your favorite fun features, though, if you can find it in your heart(and wallet!) to help support Dating Kinky & what we're about."
    }
    
    @IBAction func duhBtnClicked(_ sender: Any) {
        gotoNextVC()
    }
    
    @IBAction func notTodayBtnClicked(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    func gotoNextVC()  {
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let toVC = storyBoard.instantiateViewController( withIdentifier: VCs.HOME_TAB_BAR) as! UITabBarController
        toVC.selectedIndex = 3
        toVC.modalPresentationStyle = .fullScreen
        self.present(toVC, animated: false, completion: nil)
    }

}
