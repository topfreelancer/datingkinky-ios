//
//  Last2DayPromotionVC.swift
//  DatingKinky
//
//  Created by top Dev on 11/1/20.
//  Copyright © 2020 Ubuntu. All rights reserved.
//

import UIKit

class Last2DayPromotionVC: BaseVC {

    @IBOutlet weak var uiv_dlg: UIView!
    @IBOutlet weak var lbl_title: UILabel!
    @IBOutlet weak var lbl_content: UILabel!
    @IBOutlet weak var lbl_bottom_text: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUI()
        let downSwipe1 = UISwipeGestureRecognizer(target: self, action: #selector(swipeDown))
        downSwipe1.direction = .down
        view.addGestureRecognizer(downSwipe1)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        uiv_dlg.roundCorners([.topLeft, .topRight], radius: 20)
    }
    @objc func swipeDown() {
        self.dismiss(animated: true, completion: nil)
    }
    
    func setUI()  {
        self.lbl_title.text = "Hi \(thisuser!.username ?? "Username")!"
        self.lbl_content.text = "...of PLUS membership left.\n\nAnd last time I reminded you, I think I forgot to mention that if you become a PLUS member before your free time urns out, we have a very speical offer for you. You might want to check it out."
        self.lbl_bottom_text.text = "And remember, Dating Kinky is FREE. You don't have to support us to use it."
        self.lbl_bottom_text.font = self.lbl_bottom_text.font.italic
    }
    @IBAction func gimmeBtnClicked(_ sender: Any) {
        gotoNextVC()
    }
    
    @IBAction func notTodayBtnClicked(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    func gotoNextVC()  {
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let toVC = storyBoard.instantiateViewController( withIdentifier: VCs.HOME_TAB_BAR) as! UITabBarController
        toVC.selectedIndex = 3
        toVC.modalPresentationStyle = .fullScreen
        self.present(toVC, animated: false, completion: nil)
    }
}
