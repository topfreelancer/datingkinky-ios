//
//  HomeTabBarVC.swift
//  DatingKinky
//
//  Created by Ubuntu on 7/24/20.
//  Copyright © 2020 Ubuntu. All rights reserved.
//

import UIKit
import SwiftyJSON

var runCount = 0
class HomeTabBarVC: UITabBarController {

    override func viewDidLoad() {
        super.viewDidLoad()
        ApiManager.getMyProfile { (isSuccess, data) in
            if isSuccess{
                print(JSON(data as Any))
                let json = JSON(data as Any)
                let basic = JSON(JSON(json["data"])["basic"].object)
                let joined_date = basic["createdAt"].stringValue
                let plus_payment_record = basic["plusPaymentRecord"].stringValue// null = no
                let plusSubscriptionId = basic["plusSubscriptionId"].stringValue
                let freetrial_start_date = JSON(basic["Subscription"].object)["startDate"].stringValue
                let freetrial_expire_date = JSON(basic["Subscription"].object)["expDate"].stringValue
                
                let profile = JSON(JSON(json["data"])["profile"].object)
                let matchingObject = profile["matching"].object
                let matchingjson = JSON(matchingObject as Any)
                
                let lookingForgenderObjectArr = matchingjson["genders"].arrayObject
                var lookingForgenders: String = ""
                if let lookingForgenderObjectArr = lookingForgenderObjectArr{
                    var num = 0
                    for one in lookingForgenderObjectArr{
                        num += 1
                        let jsonOne = JSON(one)
                        lookingForgenders += jsonOne["lowerCaseName"].stringValue
                        if num != lookingForgenderObjectArr.count{
                            let spaces = String(repeating: " ", count: 1)
                            lookingForgenders = lookingForgenders + "," + spaces
                        }
                    }
                }
                
                let orientationsObjectArr = matchingjson["orientations"].arrayObject
                var orientations: String = ""
                if let orientationsObjectArr = orientationsObjectArr{
                    var num = 0
                    for one in orientationsObjectArr{
                        num += 1
                        let jsonOne = JSON(one)
                        orientations += jsonOne["lowerCaseName"].stringValue
                        if num != orientationsObjectArr.count{
                            let spaces = String(repeating: " ", count: 1)
                            orientations = orientations + "," + spaces
                        }
                    }
                }
                
                let relationShipStatusObjectArr = matchingjson["relationshipStatuses"].arrayObject
                var relationshipStatus: String = ""
                if let relationShipStatusObjectArr = relationShipStatusObjectArr{
                    var num = 0
                    for one in relationShipStatusObjectArr{
                        num += 1
                        let jsonOne = JSON(one)
                        relationshipStatus += jsonOne["lowerCaseName"].stringValue
                        if num != relationShipStatusObjectArr.count{
                            let spaces = String(repeating: " ", count: 1)
                            relationshipStatus = relationshipStatus + "," + spaces
                        }
                    }
                }
                
                let relationShipStyleObjectArr = matchingjson["relationshipStyles"].arrayObject
                var relationshipStyles: String = ""
                if let relationShipStyleObjectArr = relationShipStyleObjectArr{
                    var num = 0
                    for one in relationShipStyleObjectArr{
                        num += 1
                        let jsonOne = JSON(one)
                        relationshipStyles += jsonOne["lowerCaseName"].stringValue
                        if num != relationShipStyleObjectArr.count{
                            let spaces = String(repeating: " ", count: 1)
                            relationshipStyles = relationshipStyles + "," + spaces
                        }
                    }
                }
                
                let hereForObjectArr = matchingjson["hereFor"].arrayObject
                var hereFor: String = ""
                if let hereForObjectArr = hereForObjectArr{
                    var num = 0
                    for one in hereForObjectArr{
                        num += 1
                        let jsonOne = JSON(one)
                        hereFor += jsonOne["lowerCaseName"].stringValue
                        if num != hereForObjectArr.count{
                            let spaces = String(repeating: " ", count: 1)
                            hereFor = hereFor + "," + spaces
                        }
                    }
                }
                
                let kinkroleObjectArr = matchingjson["kinkRoles"].arrayObject
                var kinkRoles: String = ""
                if let kinkroleObjectArr = kinkroleObjectArr{
                    var num = 0
                    for one in kinkroleObjectArr{
                        num += 1
                        let jsonOne = JSON(one)
                        kinkRoles += jsonOne["lowerCaseName"].stringValue
                        if num != kinkroleObjectArr.count{
                            let spaces = String(repeating: " ", count: 1)
                            kinkRoles = kinkRoles + "," + spaces
                        }
                    }
                }
                
                usermembership = UserMembershipModel(joined_date: joined_date, current_membership: plusSubscriptionId, renew_date: plus_payment_record, lookingfor: lookingForgenders, orientation: orientations, status: relationshipStatus, relationship_style: relationshipStyles, herefor: hereFor, kinkroles:kinkRoles, trilastart: freetrial_start_date, trialend: freetrial_expire_date)
                usermembership.saveUserInfo()
                usermembership.loadUserInfo()
            }
        }
        
        // for testing part
        /*Timer.scheduledTimer(withTimeInterval: 60.0, repeats: false) { timer in
            runCount += 1
            if runCount == 1 {
                NotificationCenter.default.post(Notification(name: .show_20mins_promotion, object: nil))
                runCount += 1
                Timer.scheduledTimer(withTimeInterval: 60.0, repeats: false) { timer in
                    if runCount == 2{
                        NotificationCenter.default.post(Notification(name: .show_2days_before_promotion, object: nil))
                        runCount += 1
                        Timer.scheduledTimer(withTimeInterval: 60.0, repeats: false) { timer in
                            if runCount == 3{
                               NotificationCenter.default.post(Notification(name: .show_last_day_promotion, object: nil))
                               timer.invalidate()
                            }
                        }
                    }
                }
            }
        }*/
        /*ApiManager.getMembershipInfo { (isSuccess, data) in
            if isSuccess{
                let lastSeenTimeStamp = JSON(data as Any)["joined_date"].stringValue
                let is_plus_member = JSON(data as Any)["is_plus_member"].stringValue
                
                lastSeenChatRequest = ""
                let now = Date()
                let calendar = Calendar.current
                if let lastDate = getDateAndTime(lastSeenTimeStamp){
                    print(lastDate as Any)
                    let ageComponents = calendar.dateComponents([.second], from: lastDate, to: now)
                    let seconds = ageComponents.second ?? 0
                    print(seconds)
                    let day = secondsToHoursMinutesSeconds(seconds: seconds).0
                    let hour = secondsToHoursMinutesSeconds(seconds: seconds).1
                    let min = secondsToHoursMinutesSeconds(seconds: seconds).2
                    let secondss = secondsToHoursMinutesSeconds(seconds: seconds).3
                    
                    if day > 0{
                        lastSeenChatRequest! = "\(day)" + "d"
                    }
                    if hour > 0{
                        lastSeenChatRequest! = " " + "\(hour)" + "h"
                    }
                    if min > 0{
                        lastSeenChatRequest! = " " + "\(min)" + "m"
                    }
                    lastSeenChatRequest! = " " + "\(secondss)" + "s"
                }
            }else{
                self.showAlerMessage(message: Messages.NETWORK_ISSUE)
            }
        }*/
        
        if let count = self.tabBar.items?.count {
            
            /*self.tabBar.items![1].setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.init(named: "color_people_title") ?? UIColor.darkGray], for: .normal)
            self.tabBar.items![1].setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.init(named: "color_people_title") ?? UIColor.darkGray], for: .selected)*/
            
            /*self.tabBar.items![3].setTitleTextAttributes([NSAttributedString.Key.font: UIFont.boldSystemFont(ofSize: 20)], for: .normal)
            self.tabBar.items![3].setTitleTextAttributes([NSAttributedString.Key.font: UIFont.boldSystemFont(ofSize: 21)], for: .selected)*/
            
            if #available(iOS 11.0, *) {
                self.tabBar.items![3].setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.init(named: "color_plus_title") ?? UIColor.darkGray], for: .normal)
            } else {
                self.tabBar.items![3].setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.blue ], for: .normal)
            }
            
            if #available(iOS 11.0, *) {
                self.tabBar.items![3].setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.init(named: "color_plus_title") ?? UIColor.darkGray], for: .selected)
            } else {
                self.tabBar.items![3].setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.blue], for: .selected)
            }
           
            
            // set red as selected background color
//            let numberOfItems = CGFloat(tabBar.items!.count)
//            let tabBarItemSize = CGSize(width: tabBar.frame.width / numberOfItems, height: tabBar.frame.height)
//            
//            tabBar.backgroundImage = UIImage.imageWithColor(color: UIColor.white, size: tabBarItemSize).resizableImage(withCapInsets: UIEdgeInsets.zero)
//
//            // remove default border
//            tabBar.frame.size.width = self.view.frame.width + 4
//            tabBar.frame.origin.x = -2
            
            UINavigationBar.appearance().tintColor = UIColor.white
            UINavigationBar.appearance().titleTextAttributes = [NSAttributedString.Key.foregroundColor : UIColor.white]
        }
    }
     
   func tabBarController(_ tabBarController: UITabBarController, didSelect viewController: UIViewController) {
        //print("\nIn  >  MyTabBarViewController  >  tabBarController() ..................................9-9-9-9-9\n\n")
       
        if viewController is PlusVC {
            print("Tab One Pressed")
            let numberOfItems = CGFloat(tabBar.items!.count)
            let tabBarItemSize = CGSize(width: tabBar.frame.width / numberOfItems, height: tabBar.frame.height)
            self.tabBar.backgroundImage = UIImage.imageWithColor(color: UIColor.white, size: tabBarItemSize).resizableImage(withCapInsets: UIEdgeInsets.zero)
        }
    }
}
