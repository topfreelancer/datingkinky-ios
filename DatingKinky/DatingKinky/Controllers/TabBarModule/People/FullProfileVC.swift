//
//  FullProfileVC.swift
//  DatingKinky
//
//  Created by top Dev on 8/14/20.
//  Copyright © 2020 Ubuntu. All rights reserved.
//

import UIKit

class FullProfileVC: BaseVC {
    
    @IBOutlet weak var imv_avatar: UIImageView!
    @IBOutlet weak var lbl_userName: UILabel!
    @IBOutlet weak var lbl_profileContent: UILabel!
    @IBOutlet weak var lbl_lookingFor: UILabel!
    @IBOutlet weak var lbl_orientation: UILabel!
    @IBOutlet weak var lbl_status: UILabel!
    @IBOutlet weak var lbl_relationShip: UILabel!
    @IBOutlet weak var lbl_hereFor: UILabel!
    @IBOutlet weak var lbl_kinkRole: UILabel!
    @IBOutlet weak var lbl_lastSeen: UILabel!
    var self_selectedUser: PeopleModel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(animated)
        self.navBarHidden()
        self.setUI()
    }
    
    // test part
    /*@objc func callFirstPromotion() {
        print("callled with 20 mins")
        // first promotion show
        NotificationCenter.default.post(Notification(name: .show_20mins_promotion, object: nil))
    }*/
    
    func setUI()  {
        if let selectedUser_this = selectedUser{
            self.self_selectedUser = selectedUser_this
            //TODO: maybe change later
            //selectedUser = nil
            let url = URL(string: self_selectedUser.userAvatar ?? "")
            imv_avatar.kf.setImage(with: url,placeholder: UIImage.init(named: "placeholder"))
            lbl_userName.text = self_selectedUser.userName
            let gender = self_selectedUser.gender == "woman" ? "W" : "M"
            let showhim = self_selectedUser.pronoun ?? "He/His"
            let spaces = String(repeating: " ", count: 1)
            lbl_profileContent.text = self_selectedUser.currentAge! + gender + "," + spaces + self_selectedUser.userKinkRole! + spaces + "|" + spaces + showhim + spaces + "|" + spaces + self_selectedUser.location!
            var str_lookingForGender = " "
            var str_orientation = " "
            var str_status = " "
            var str_relationShipStyle = " "
            var str_hereFor = " "
            var str_kinkRole = " "
            
            if let common = self_selectedUser.lookingForGender{
                var num = 0
                for one in common{
                    num += 1
                    str_lookingForGender += one
                    if num != common.count{
                        let spaces = String(repeating: " ", count: 1)
                        str_lookingForGender =  str_lookingForGender + "," + spaces
                    }
                }
            }
            
            if let common = self_selectedUser.orientation{
                var num = 0
                for one in common{
                    num += 1
                    str_orientation += one
                    if num != common.count{
                        let spaces = String(repeating: " ", count: 1)
                        str_orientation = str_orientation + "," + spaces
                    }
                }
            }
            
            if let common = self_selectedUser.status{
                var num = 0
                for one in common{
                    num += 1
                    str_status += one
                    if num != common.count{
                        let spaces = String(repeating: " ", count: 1)
                        str_status = str_status + "," + spaces
                    }
                }
            }
            
            if let common = self_selectedUser.relationshipStyle{
                var num = 0
                for one in common{
                    num += 1
                    str_relationShipStyle += one
                    if num != common.count{
                        let spaces = String(repeating: " ", count: 1)
                        str_relationShipStyle = str_relationShipStyle + "," + spaces
                    }
                }
            }
            
            if let common = self_selectedUser.hereFor{
                var num = 0
                for one in common{
                    num += 1
                    str_hereFor += one
                    if num != common.count{
                        let spaces = String(repeating: " ", count: 1)
                        str_hereFor = str_hereFor + "," + spaces
                    }
                }
            }
            
            if let common = self_selectedUser.kinkRole{
                var num = 0
                for one in common{
                    num += 1
                    str_kinkRole += one
                    if num != common.count{
                        let spaces = String(repeating: " ", count: 1)
                        str_kinkRole = str_kinkRole + "," + spaces
                    }
                }
            }
            
            lbl_lookingFor.text = str_lookingForGender
            lbl_orientation.text = str_orientation
            lbl_status.text = str_status
            lbl_relationShip.text = str_relationShipStyle
            lbl_hereFor.text = str_hereFor
            //lbl_kinkRole.text = self.removeSpecialCharsFromString(text: str_kinkRole)
            lbl_kinkRole.text = str_kinkRole
            //print(str_kinkRole)
            lbl_lastSeen.text = self.self_selectedUser.lastActive
        }
    }
    
    func sendChatRequest()  {
        
        memberName4ChatRequest = self.self_selectedUser.userName
        let gender = self.self_selectedUser.gender == "woman" ? "W" : "M"
        let showhim = self.self_selectedUser.pronoun ?? "He/His"
        let kinkRole = self.self_selectedUser.userKinkRole ?? ""
        let spaces = String(repeating: " ", count: 1)
        //memberProfile4ChatRequest = self.self_selectedUser.currentAge! + gender + spaces + "|" + spaces + showhim + spaces + "|" + spaces + self.self_selectedUser.location!
        memberProfile4ChatRequest = self.self_selectedUser.currentAge! + gender + spaces + kinkRole + spaces + "•" + spaces + showhim + spaces + "•" + spaces + self.self_selectedUser.location! + spaces + "•" + spaces + "Last seen"
        partnerAge4ChatRequestAccept = self.self_selectedUser.currentAge
        partnerAvatar4ChatRequest = self.self_selectedUser.userAvatar
        partnerGender4ChatRequest = gender
        partnerKinkRole4ChatRequest = self.self_selectedUser.userKinkRole
        partnerLocation4ChatRequest = self.self_selectedUser.location
        partnerPronoun4ChatRequest = self.self_selectedUser.pronoun
        //lastSeenChatList = self.self_selectedUser.lastActive
        friendID = self.self_selectedUser.id
        //NotificationCenter.default.post(name:.chatRequest, object: nil)
        self.showBottomDlg(storyboard_name: StoryBoards.CHAT, vcname: VCs.CHAT_REQUEST_SEND)
    }
    
    @IBAction func chatRequestBtnClicked(_ sender: Any) {
        self.sendChatRequest()
    }
    
    @IBAction func viewFullProfileBtnClicked(_ sender: Any) {
        self.navBarShow()
        if let username = self.self_selectedUser.userName, let authToken = UserDefault.getString(key: PARAMS.X_AUTH_TOKEN, defaultValue: ""){
            let link = "https://members.datingkinky.com/auth/users?user=" + username + "&authToken=" + authToken
            self.gotoWebViewWithProgressBar(link)
        }
    }
    
    @IBAction func closeBtnClicked(_ sender: Any) {
        self.dismiss(animated: false, completion: nil)
    }
}
