//
//  ChatRequestPopUpVC.swift
//  DatingKinky
//
//  Created by top Dev on 8/8/20.
//  Copyright © 2020 Ubuntu. All rights reserved.
//

import UIKit

class ChatRequestSendVC: BaseVC {

    var ds_allChats1 = [AllChatModel]()
    //var ds_allChats = [AllChatModel]()
    var available_ids = [String]()
    var userlistHandle: UInt?
    
    @IBOutlet weak var lbl_content: UILabel!
    @IBOutlet weak var uiv_dlg: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if let memebername = memberName4ChatRequest{
            self.lbl_content.text = "\(memebername) has been asked if they would like to chat with you. if they agree, you'll get a notification.\n \nIf not, better luck next time!"
        }
        let chatRequest = "u" + "\(thisuser!.id ?? "")"
        self.userlistListner(chatRequest)
        //NotificationCenter.default.addObserver(self, selector: #selector(chatRequest),name:.chatRequest, object: nil)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        //NotificationCenter.default.removeObserver(self)
        if let id = thisuser?.id,let userlistHandle = userlistHandle {
            FirebaseAPI.removeUserlistObserver(userRoomid: "u" + id, userlistHandle)
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        uiv_dlg.roundCorners([.topLeft, .topRight], radius: 20)
    }
    
    /*@objc func chatRequest(){
        if let memebername = memberName4ChatRequest{
            self.lbl_content.text = "\(memebername) has been asked if they would like to chat with you. if they agree, you'll get a notification.\n \nIf not, better luck next time!"
        }
    }*/
    
    @IBAction func spaceBtnClicked(_ sender: Any) {
        selectedUser = nil
        friendID = nil
        memberProfile4ChatRequest = nil
        memberName4ChatRequest = nil
        partnerAge4ChatRequestAccept = nil
        partnerAvatar4ChatRequest = nil
        partnerGender4ChatRequest = nil
        partnerKinkRole4ChatRequest = nil
        partnerLocation4ChatRequest = nil
        partnerPronoun4ChatRequest = nil
        chattingOption = .normal
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func gotitBtnClicked(_ sender: Any) {
        if let receiverUser = selectedUser{
            self.evaluateSentStatus(receiverUser.id!, ids: self.available_ids)
        }
    }
    
    func userlistListner(_ userRoomid : String)  {
       self.ds_allChats1.removeAll()
            userlistHandle = FirebaseAPI.getUserList(userRoomid : userRoomid ,handler: { (userlist) in
            if let userlist = userlist{
                guard userlist.id != nil else{
                    return
                }
            }

            if let userlist = userlist{
                self.ds_allChats1.append(userlist)
            }

            if self.ds_allChats1.count != 0{
                var ids = [String]()
                for one in self.ds_allChats1{
                    ids.append(one.id!)
                }
                var num = 0
                self.available_ids.removeAll()
                for  one in ids.removeDuplicates(){
                    num += 1
                    if let allchat = self.getDataFromID(one){
                        self.available_ids.append(allchat.id ?? "")
                    }
                }
            }
        })
    }
    
    func getBlockStatus(_ id: String) -> Bool {
        var status = false
        if let blocks = blockusers.getBlockUsers_BlockedUsers(){
            if blocks.count > 0{
                for one in blocks{
                    if one == id{
                        status = true
                        break
                    }
                }
            }
        }
        return status
    }
    
    func evaluateSentStatus(_ id: String, ids: [String]) {
        var status = false
        if ids.count > 0{
            var num = 0
            for one in ids{
                num += 1
                if one == id{
                    status = true
                }
                if num == ids.count{
                    if status{
                        self.showToast("You have already contact with this member!")
                    }else{
                        self.sendChatRequest()
                    }
                }
            }
        }else{
            self.sendChatRequest()
        }
    }
    
    func getDataFromID(_ id: String) -> AllChatModel? {
        var returnModel : AllChatModel?
        for one in self.ds_allChats1{
            if id == one.id{
                returnModel = one
            }
        }
        return returnModel
    }
    
    func sendChatRequest() {
        self.showLoadingView(vc: self)
        let timeNow = Int(NSDate().timeIntervalSince1970) * 1000
        if let receiverUser = selectedUser{
            var requestObject = [String: String]()
            // MARK: for list view for my list object - - listobject
            requestObject["receiver_id"]            = receiverUser.id
            requestObject["receiver_name"]          = receiverUser.userName
            requestObject["receiver_avatar"]        = receiverUser.userAvatar
            requestObject["receiver_currentAge"]    = receiverUser.currentAge
            requestObject["receiver_gender"]        = receiverUser.gender
            requestObject["receiver_kinkRole"]      = receiverUser.userKinkRole
            requestObject["receiver_pronoun"]       = receiverUser.pronoun
            requestObject["receiver_location"]      = receiverUser.location
            requestObject["last_seen"]              = "\(timeNow)" as String
            requestObject["state"]                  = ChattingRuquest.SEND
            
            FirebaseAPI.sendChatRequestListUpdate(requestObject, myid: "u" + thisuser!.id!, partnerid: "u" + receiverUser.id!){
                (status,message) in
                if status{
                    var requestObject1 = [String: String]()
                    // MARK:  for list view for partner's list object - listobject1
                    requestObject1["receiver_id"]             = thisuser!.user?._id
                    requestObject1["receiver_name"]           = thisuser!.username
                    requestObject1["receiver_avatar"]         = thisuser!.userAvatar
                    requestObject1["receiver_currentAge"]     = thisuser!.currentAge
                    requestObject1["receiver_gender"]         = thisuser!.gender
                    requestObject1["receiver_kinkRole"]       = thisuser!.kinkRole
                    requestObject1["receiver_pronoun"]        = thisuser!.pronoun
                    requestObject1["receiver_location"]       = thisuser!.location
                    requestObject1["last_seen"]               = "\(timeNow)" as String
                    requestObject1["state"]                   = ChattingRuquest.PENDING
                    
                    FirebaseAPI.sendChatRequestListUpdate(requestObject1, myid: "u" + receiverUser.id! , partnerid: "u" + thisuser!.id!) { (isSuccess2, data2) in
                        self.hideLoadingView()
                        if isSuccess2{
                            self.showLoadingView(vc: self)
                            ApiManager.chatRequest(receiver_id: receiverUser.id!, requestType: RequestType.chat_send.rawValue) { (isSuccess3, data) in
                                self.hideLoadingView()
                                if isSuccess3{
                                    self.dismiss(animated: true, completion: nil)
                                }
                            }
                            
                        }else{
                            self.dismiss(animated: true, completion: nil)
                        }
                    }
                }
            }
        }
    }
    
    @IBAction func topBtnClicked(_ sender: Any) {
        
        selectedUser = nil
        friendID = nil
        memberProfile4ChatRequest = nil
        memberName4ChatRequest = nil
        partnerAge4ChatRequestAccept = nil
        partnerAvatar4ChatRequest = nil
        partnerGender4ChatRequest = nil
        partnerKinkRole4ChatRequest = nil
        partnerLocation4ChatRequest = nil
        partnerPronoun4ChatRequest = nil
        chattingOption = .normal
        self.dismiss(animated: true, completion: nil)
    }
}
