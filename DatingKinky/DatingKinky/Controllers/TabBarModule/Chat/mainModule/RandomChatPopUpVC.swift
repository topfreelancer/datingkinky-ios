//
//  RandomChatPopUpVC.swift
//  DatingKinky
//
//  Created by Ubuntu on 8/4/20.
//  Copyright © 2020 Ubuntu. All rights reserved.
//

import UIKit

class RandomChatPopUpVC: BaseVC {
    
    @IBOutlet weak var lbl_boldContent: UILabel!
    @IBOutlet weak var lbl_randomChatContent: UILabel!
    @IBOutlet weak var lbl_italic: UILabel!
    @IBOutlet weak var uiv_dlg: UIView!
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        setUpUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.creatNav(.randomChat1)
        self.uiv_dlg.isHidden = false
    }
    
    override func viewDidAppear(_ animated: Bool) {
           
       super.viewDidAppear(animated)
       uiv_dlg.roundCorners([.topLeft, .topRight], radius: 20)
   }
    
    func setUpUI()  {
        self.lbl_randomChatContent.text = "Random Chat for PLUS Memers is a fun way to connect with other kinksters online and interested in chatting right now. \n  \nOnce you turn Random Chat to on, it will stay on for 90 minutes , then ask you if you'd like to remain connected. This is how we assure that everyone is on and ready to chat. If you leave, and can remember to turn Random Chat off, that would be awesome! \n \nRandom Chat is 100% anonymous. It is also temporary. Random Chats are not saved in your history, unless both you and your chat partner choose to unlock your pofiles for each other."
        self.lbl_boldContent.text = "NOTE:We DO welcome adult chat! Not everyone is here for that (or in the mood), so if that's your primary interest, please do get consent before launching into your explicit fantasies. Thanks!"
        self.lbl_italic.text = "This is the only time you'll see this message."
        self.lbl_italic.font = self.lbl_italic.font.italic
        
    }
    @IBAction func topBtnClicked(_ sender: Any) {
        self.closeRandomChat(random_switch: 0)
    }
    
    @IBAction func spaceBtnclicked(_ sender: Any) {
        self.closeRandomChat(random_switch: 0)
    }
    @IBAction func doBtnClicked(_ sender: Any) {
        animVC = .randomChat1
        self.uiv_dlg.isHidden = true
        if darkVC != nil{
            darkVC.view.removeFromSuperview()
        }
        self.openMenu(.randomChat1)
    }
    
    @IBAction func notBtnClicked(_ sender: Any) {
        self.closeRandomChat(random_switch: 0)
    }
}
