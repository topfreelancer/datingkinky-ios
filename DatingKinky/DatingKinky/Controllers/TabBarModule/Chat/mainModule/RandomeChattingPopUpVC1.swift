//
//  RandomeChattingPopUpVC1.swift
//  DatingKinky
//
//  Created by Ubuntu on 8/4/20.
//  Copyright © 2020 Ubuntu. All rights reserved.
//

import UIKit
import FirebaseDatabase

class RandomeChatPopUpVC1: BaseVC {

    @IBOutlet weak var lbl_content: UILabel!
    @IBOutlet weak var uiv_dlg: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.lbl_content.text = "Someone is ready to chat with you. Do you accept (2 minute limit)?"
    }
    
    override func viewDidAppear(_ animated: Bool) {
            
        creatNav(.randomChat1)
        super.viewDidAppear(animated)
        uiv_dlg.roundCorners([.topLeft, .topRight], radius: 20)
    }
    
    @IBAction func connectBtnClicked(_ sender: Any) {
        // here you must call random chatting function, and set random node stats as false
        // call api for random chatting with 30S once
        // set firebase node as false for random status
        Database.database().reference().child("random_chat").child(thisuser!.id!).child("status").setValue("0")
        self.closeMenu(.randomChat1, randomSwitch: 1)
        
    }
    
    @IBAction func nextOnebtnClicked(_ sender: Any) {
        self.closeMenu(.randomChat1)
        //self.closeMenu(.randomChat)
    }
    
    @IBAction func turnoffBtnClicked(_ sender: Any) {
        self.closeMenu(.randomChat1)
        self.createRandomChat()
    }
    
    @IBAction func spaceBtnClicked(_ sender: Any) {
        self.closeMenu(.randomChat1)
        //self.closeMenu(.randomChat)
    }
    @IBAction func topBtnClicked(_ sender: Any) {
        self.closeMenu(.randomChat1)
    }
}
