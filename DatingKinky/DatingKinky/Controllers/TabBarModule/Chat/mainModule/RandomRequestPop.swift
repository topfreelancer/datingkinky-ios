//
//  RandomRequestPop.swift
//  DatingKinky
//
//  Created by top Dev on 07.12.2020.
//  Copyright © 2020 Ubuntu. All rights reserved.
//

import UIKit
import AVFoundation
import FirebaseDatabase

class RandomRequestPop: BaseVC{

    @IBOutlet weak var uiv_dlg: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        uiv_dlg.roundCorners([.topLeft, .topRight], radius: 20)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    @available(iOS 11.0, *)
    @IBAction func yupBtnClicked(_ sender: Any) {
        // setting true for response, and setting status true goto random chatting room
        Database.database().reference().child("random_chat").child(thisuser!.id!).child("status").setValue("0")
        let tovc = self.createViewControllerwithStoryBoardName(StoryBoards.CHAT, controllerName: VCs.MESSAGESEND) as! MessageSendVC
        tovc.is_random_chatting = true
        self.navigationController?.pushViewController(tovc, animated: true)
    }
    
    @IBAction func notrightBtnClicked(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
        // setting false for response, and setting status true
        Database.database().reference().child("random_chat").child(thisuser!.id!).child("status").setValue("1")
    }
    @IBAction func topBtnClicked(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
        Database.database().reference().child("random_chat").child(thisuser!.id!).child("status").setValue("1")
    }
}

