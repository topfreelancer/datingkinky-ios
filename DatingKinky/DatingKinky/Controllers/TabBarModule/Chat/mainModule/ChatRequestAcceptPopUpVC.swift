//
//  ChatRequestAcceptPopUpVC.swift
//  DatingKinky
//
//  Created by top Dev on 8/8/20.
//  Copyright © 2020 Ubuntu. All rights reserved.
//

import UIKit

class ChatRequestAcceptPopUpVC: BaseVC {

    @IBOutlet weak var lbl_content: UILabel!
    @IBOutlet weak var uiv_dlg: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //NotificationCenter.default.addObserver(self, selector: #selector(acceptRequest),name:.acceptRequest, object: nil)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        
        //NotificationCenter.default.removeObserver(self)
    }
    
    override func viewDidAppear(_ animated: Bool) {
            
        super.viewDidAppear(animated)
        uiv_dlg.roundCorners([.topLeft, .topRight], radius: 20)
    }
    
    @objc func acceptRequest(){
        
        if let memebername = memberName4AccepttRequest{
            self.lbl_content.text = "\(memebername) has been asked if they would like to chat with you. if they agree, you'll get a notification.\n \nIf not, better luck next time!"
        }
    }
    
    @IBAction func spaceBtnClicked(_ sender: Any) {
        self.closeMenu(.acceptRequest)
    }
    
    @IBAction func yupBtnClicked(_ sender: Any) {
        self.closeMenu(.acceptRequest)
    }
    
    @IBAction func topBtnClicked(_ sender: Any) {
        self.closeMenu(.acceptRequest)
    }
    
    @IBAction func notrightBtnClicked(_ sender: Any) {
        self.closeMenu(.acceptRequest)
    }
    
    @IBAction func declineBtnClicked(_ sender: Any) {
        self.closeMenu(.acceptRequest)
    }
}
