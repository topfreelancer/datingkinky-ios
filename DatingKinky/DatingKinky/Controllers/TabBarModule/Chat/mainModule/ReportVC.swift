//
//  ReportVC.swift
//  DatingKinky
//
//  Created by top Dev on 09.12.2020.
//  Copyright © 2020 Ubuntu. All rights reserved.
//

import UIKit
import Photos
import DKImagePickerController
import DKCamera
import SwiftyJSON

class ReportVC: BaseVC {
    var pictureData  = [PortfolioModel]()
    let pickerController = DKImagePickerController()
    //var imageFils = [String]()
    var reportImages = [String]()
    var assets: [DKAsset] = []
    var numm = 0
    var reason: String = ""
    var num = 0
    
    @IBOutlet weak var uiv_picturecol: UIView!
    @IBOutlet weak var uiv_tap: dropShadowView!
    @IBOutlet weak var cus_drop_down: MSDropDown!
    @IBOutlet weak var lbl_content: UILabel!
    @IBOutlet weak var txv_more: UITextView!
    @IBOutlet weak var ui_picture_coll: UICollectionView!
    
    let report_Options : [KeyValueModel] = [KeyValueModel(key: "0", value: "Scammer/Spammer"),
    KeyValueModel(key: "1", value: "Offensive"),
    KeyValueModel(key: "2", value: "Hate Speech"),
    KeyValueModel(key: "3", value: "Illegal"),KeyValueModel(key: "4", value: "Copyright"),
    KeyValueModel(key: "5", value: "Pro"),
    KeyValueModel(key: "6", value: "Harassment"),
    KeyValueModel(key: "7", value: "Other")]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.lbl_content.text = "We care about your experience on Dating Kinky, and want to hear from you whenever anything feels hinky.\n\nBecause, frankly, we get enough of that on other sites, amirite?"
        self.cus_drop_down.keyvalueCount = self.report_Options.count
        self.cus_drop_down.delegate = self
        self.cus_drop_down.keyValues = self.report_Options
        self.cus_drop_down.isMultiSelect = false
        self.addNavBarImage()
        self.addLeftButton4NavBar1()
        self.txv_more.delegate = self
        txv_more.text = "Tell us more..."
        txv_more.textContainerInset = UIEdgeInsets(top: 12, left: 10, bottom: 10, right: 10)

        txv_more.textColor = UIColor.lightGray
        self.uiv_picturecol.isHidden = true
        uiv_tap.addTapGesture(tapNumber: 1, target: self, action: #selector(onClickPhoto))
    }
    
    func getUploadPhotoURL4ReportandUpload(selectedImage: UIImage?) {
        self.showLoadingView(vc: self)
        ApiManager.getPhotoURL4Report { (isSuccess, data) in
            if isSuccess{
                print(data as Any)
                let json = JSON(data as Any)
                let dataone = json["data"].object
                let jsondata = JSON(dataone as Any)
                // send fileName and url for the upload action in the cropPhotoVC
                let fileName: String? = jsondata["filename"].stringValue
                let fileURL: String? = jsondata["url"].stringValue
                //fileURL = fileURL!.removingPercentEncoding
                let fileId: String? = jsondata["_id"].stringValue
                print("this is upload photo name and  URL===>", "\(fileName ?? "") \n \(fileURL ?? "")\n \(fileId ?? "")")
                if let filename = fileName, let url = fileURL, let id = fileId{
                    print("this is the send side fileName and url", "\(filename) \n \(url)")
                    if let cropimage = selectedImage {
                        ApiManager.s3Upload(presignedURL: url, fileName: saveBinaryWithName(image: cropimage, name: filename)) { (isSuccess, data) in
                            if isSuccess{
                                print("upload Success")
                                self.num += 1
                                self.reportImages.append(id)
                                if self.num == self.pictureData.count{
                                    ApiManager.report(reason: self.reason, reasonDetails: self.txv_more.text, attachments: self.reportImages) { (isSuccess, data) in
                                        self.hideLoadingView()
                                        print("report success")
                                        let alertController = UIAlertController(title: Constants.AppName, message: "Successfully reported", preferredStyle: .alert)
                                        let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in
                                            // Code in this block will trigger when OK button tapped.
                                            self.gotoTabIndex(0)
                                        }
                                        alertController.addAction(OKAction)
                                        self.present(alertController, animated: true, completion:nil)
                                    }
                                }
                            }else{
                                self.showAlerMessage(message: Messages.SERVERERROR)
                            }
                        }
                    }
                }
            }else{
                
            }
        }
    }
    
    func addLeftButton4NavBar1() {
        // if needed i will add
        let btn_back = UIButton(type: .custom)
        btn_back.setImage(UIImage (named: "btn_back")!.withRenderingMode(.alwaysTemplate), for: .normal)
        btn_back.addTarget(self, action: #selector(addTappedLeftBtn1), for: .touchUpInside)
        btn_back.imageEdgeInsets = UIEdgeInsets(top: 5, left: 5, bottom:5, right: 5)
        btn_back.tintColor = UIColor.white
        let barButtonItemBack = UIBarButtonItem(customView: btn_back)
        barButtonItemBack.customView?.widthAnchor.constraint(equalToConstant: 35).isActive = true
        barButtonItemBack.customView?.heightAnchor.constraint(equalToConstant: 35).isActive = true
        self.navigationItem.leftBarButtonItem = barButtonItemBack
    }
    
    @objc func addTappedLeftBtn1() {
        //self.navigationController?.popViewController(animated: true)
        self.gotoTabIndex(0)
    }
        
    @IBAction func reportBtnClicked(_ sender: Any) {
        num = 0
        self.reportImages.removeAll()
        for one in pictureData{
            self.getUploadPhotoURL4ReportandUpload(selectedImage: one.imageFile)
        }
    }
    
    @IBAction func removeBtnClicked(_ sender: Any) {
        let button = sender as! UIButton
        let index = button.tag
        //self.imageFils.remove(at: index)
        self.pictureData.remove(at: index)
        self.ui_picture_coll.reloadData()
        if self.pictureData.count == 0{
            self.uiv_tap.isHidden = false
            self.uiv_picturecol.isHidden = true
        }
    }
    
    @objc func onClickPhoto(gesture: UITapGestureRecognizer) -> Void {
        self.openProfile()
    }
    
    func openProfile() {
        self.initDkimgagePicker()
        pickerController.maxSelectableCount = 5
        pickerController.dismissCamera()
        pickerController.showsCancelButton = true
        pickerController.setSelectedAssets(assets: assets)
        pickerController.didSelectAssets = {(assets: [DKAsset]) in self.onSelectAssets(assets: assets)}
        pickerController.modalPresentationStyle = .fullScreen
        pickerController.assetType = .allPhotos
        present(pickerController, animated: true)
    }
    
    func initDkimgagePicker(){
       pickerController.assetType = .allAssets
       pickerController.allowSwipeToSelect = true
       pickerController.sourceType = .both
    }
    
    func onSelectAssets(assets : [DKAsset]) {
        self.assets.removeAll()
        self.assets = assets
        dump(assets, name: "this is the assets for dkimage picker")
        //self.imageFils.removeAll()
        self.pictureData.removeAll()
        numm = 0
        if assets.count > 0 {
           for asset in assets {
                numm += 1
                print(numm)
                asset.fetchOriginalImage(options: nil, completeBlock: { image, info in
                   self.gotoUploadProfile(image)
                })
           }
       }
       else {}
   }
    
    func gotoUploadProfile(_ image: UIImage?) {
        if let image = image{
            //imageFils.append(saveToFile(image:image,filePath:"photo",fileName:randomString(length: 2))) // Image save action
            self.pictureData.append(PortfolioModel(image))
        }
        if numm == assets.count{
            uiv_tap.isHidden = true
            self.uiv_picturecol.isHidden = false
            self.ui_picture_coll.reloadData()
        }
    }
}

extension ReportVC: MSDropDownDelegate{
    func dropdownSelected(tagId: Int, answer: String, value: String, isSelected: Bool) {
        self.reason = answer
    }
}

extension ReportVC: UITextViewDelegate{
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.textColor == UIColor.lightGray {
            textView.text = nil
            textView.textColor = UIColor.black
        }
    }
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            textView.text = "Tell us more..."
            textView.textColor = UIColor.lightGray
        }
    }
}

//MARK: UICollectionViewDataSource
extension ReportVC: UICollectionViewDataSource{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return pictureData.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "uploadPictureCell", for: indexPath) as! uploadPictureCell
            
        cell.entity = pictureData[indexPath.row]
        cell.btn_close.tag = indexPath.row
        return cell
    }
}

extension ReportVC: UICollectionViewDelegate{
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let pos = indexPath.row
        // TODO: goto detail show vc
    }
}

extension ReportVC: UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let h = collectionView.frame.size.height * 0.9
        let w = h / 4 * 3
        return CGSize(width: w, height: h)
    }
}


