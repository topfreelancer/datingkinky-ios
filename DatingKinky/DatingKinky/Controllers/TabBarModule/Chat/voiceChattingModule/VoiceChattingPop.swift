//
//  VoiceChattingPop.swift
//  DatingKinky
//
//  Created by top Dev on 16.11.2020.
//  Copyright © 2020 Ubuntu. All rights reserved.
//

import UIKit
import AVFoundation

class VoicePopDlg: BaseVC{

    @IBOutlet weak var uiv_dlg: UIView!
    @IBOutlet weak var imv_profile: UIImageView!
    var str_iamge_url: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if let url = self.str_iamge_url{
            setImageWithURL(url, imv: self.imv_profile)
        }
        let downSwipee = UISwipeGestureRecognizer(target: self, action: #selector(handleSwipess(_:)))
        downSwipee.direction = .down
        view.addGestureRecognizer(downSwipee)
    }
    
    @objc func handleSwipess(_ sender:UISwipeGestureRecognizer) {
        if (sender.direction == .down) {
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        uiv_dlg.roundCorners([.topLeft, .topRight], radius: 20)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    @IBAction func shareBtnClicked(_ sender: Any) {
        if let friendid = friendID{
            self.showLoadingView(vc: self)
            ApiManager.chatRequest(receiver_id: friendid, requestType: RequestType.voice_chat_send.rawValue) { (isSuccess, data) in
                self.hideLoadingView()
                if isSuccess{
                    let storyBoard : UIStoryboard = UIStoryboard(name: StoryBoards.CHAT, bundle: nil)
                    let toVC = storyBoard.instantiateViewController( withIdentifier: "VoiceChatViewController") as! VoiceChatViewController
                    var roomId = "voice_default"
                    if let userid = thisuser!.id , let friendid = friendID{
                        if userid > friendid{
                            roomId = userid + "_" + friendid
                        }else{
                            roomId = friendid + "_" + userid
                        }
                    }
                    toVC.roomID = "voice_" + roomId
                    print("this is room id for voice chattingroom ===>", toVC.roomID)
                    toVC.str_profile = self.str_iamge_url
                    //self.present(toVC, animated: false, completion: nil)
                    let navViewController = UINavigationController(rootViewController: toVC)
                    navViewController.modalPresentationStyle = .fullScreen
                    self.present(navViewController, animated: false, completion: nil)
                }
            }
        }
    }
    @IBAction func endBtnClicked(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func topBtnClicked(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
}
