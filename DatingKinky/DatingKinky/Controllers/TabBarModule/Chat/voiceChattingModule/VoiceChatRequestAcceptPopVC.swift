//
//  VoiceChatRequestPopVC.swift
//  DatingKinky
//
//  Created by top Dev on 09.12.2020.
//  Copyright © 2020 Ubuntu. All rights reserved.
//

import UIKit

class VoiceChatRequestAcceptPopVC: BaseVC {

    @IBOutlet weak var lbl_content: UILabel!
    @IBOutlet weak var uiv_dlg: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.lbl_content.text = "\(request_user_name ?? "") has requested to audio chat with you.What do you think? Are you game?"
        let downSwipee = UISwipeGestureRecognizer(target: self, action: #selector(handleSwipess(_:)))
        downSwipee.direction = .down
        view.addGestureRecognizer(downSwipee)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        uiv_dlg.roundCorners([.topLeft, .topRight], radius: 20)
    }
    
    @objc func handleSwipess(_ sender:UISwipeGestureRecognizer) {
        if (sender.direction == .down) {
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    @IBAction func connnectBtnClicked(_ sender: Any) {
        if let friendid = request_user_id{
            self.showLoadingView(vc: self)
            ApiManager.chatRequest(receiver_id: friendid, requestType: RequestType.voice_chat_accept.rawValue) { (isSuccess, data) in
                self.hideLoadingView()
                let storyBoard : UIStoryboard = UIStoryboard(name: StoryBoards.CHAT, bundle: nil)
                let toVC = storyBoard.instantiateViewController( withIdentifier: "VoiceChatViewController") as! VoiceChatViewController
                var roomId = "voice_default"
                if let userid = thisuser!.id , let friendid = friendID{
                    if userid > friendid{
                        roomId = userid + "_" + friendid
                    }else{
                        roomId = friendid + "_" + userid
                    }
                }
                toVC.roomID = "voice_" + roomId
                print("this is room id for voice chattingroom ===>", toVC.roomID)
                toVC.str_profile = partnerAvatar4ChatRequest
                //self.present(toVC, animated: false, completion: nil)
                let navViewController = UINavigationController(rootViewController: toVC)
                navViewController.modalPresentationStyle = .fullScreen
                self.present(navViewController, animated: false, completion: nil)
            }
        }
    }
    @IBAction func noBtnClicked(_ sender: Any) {
        if let friendid = request_user_id{
            self.showLoadingView(vc: self)
            ApiManager.chatRequest(receiver_id: friendid, requestType: RequestType.voice_chat_reject.rawValue) { (isSuccess, data) in
                self.hideLoadingView()
                if isSuccess{
                    self.dismiss(animated: true, completion: nil)
                }else{
                    self.dismiss(animated: true, completion: nil)
                }
            }
        }
    }
    @IBAction func turnoffBtnClicked(_ sender: Any) {
        self.gotoTabIndex(4)
    }
    @IBAction func topBtnClicked(_ sender: Any) {
        if let friendid = request_user_id{
            self.showLoadingView(vc: self)
            ApiManager.chatRequest(receiver_id: friendid, requestType: RequestType.voice_chat_reject.rawValue) { (isSuccess, data) in
                self.hideLoadingView()
                if isSuccess{
                    self.dismiss(animated: true, completion: nil)
                }else{
                    self.dismiss(animated: true, completion: nil)
                }
            }
        }
    }
}
