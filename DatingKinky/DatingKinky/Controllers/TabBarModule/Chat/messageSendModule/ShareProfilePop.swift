//
//  ShareProfilePop.swift
//  DatingKinky
//
//  Created by top Dev on 07.12.2020.
//  Copyright © 2020 Ubuntu. All rights reserved.
//

import UIKit
import AVFoundation

class ShareProfilePop: BaseVC{

    @IBOutlet weak var uiv_dlg: UIView!
    @IBOutlet weak var lbl_content: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUI()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        uiv_dlg.roundCorners([.topLeft, .topRight], radius: 20)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    func setUI()  {
        self.lbl_content.text = "Woohoo! So the chat is going well, I take it?\n\nGreat! Your chat mate will get a notification that you've unlocked your details.\n\nIf they choose to unlock their dtails as well, you will both get to see each other's information, access profiles, and more.\n\nOh, and your chat will be saved.\n\nAnd you can audio or video chat, and do all the things!\n\nWe're keeping our fingers crossed that this works out!"
    }
    
    @IBAction func shareBtnClicked(_ sender: Any) {
        let dataDict:[String: Any] = ["is_random": 1]
        NotificationCenter.default.post(Notification(name: .share_profile, object: nil,userInfo: dataDict))
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func notrightBtnClicked(_ sender: Any) {
        let dataDict:[String: Any] = ["is_random": 0]
        NotificationCenter.default.post(Notification(name: .share_profile, object: nil,userInfo: dataDict))
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func topBtnClicked(_ sender: Any) {
        let dataDict:[String: Any] = ["is_random": 0]
        NotificationCenter.default.post(Notification(name: .share_profile, object: nil,userInfo: dataDict))
        self.dismiss(animated: true, completion: nil)
    }
}
