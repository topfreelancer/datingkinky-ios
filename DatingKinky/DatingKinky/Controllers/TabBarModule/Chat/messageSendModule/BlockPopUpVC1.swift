//
//  BlockPopUpVC1.swift
//  DatingKinky
//
//  Created by top Dev on 8/19/20.
//  Copyright © 2020 Ubuntu. All rights reserved.
//

import UIKit
import UIKit
import Firebase
import FirebaseAuth
import FirebaseStorage
import FirebaseDatabase
import SwiftyJSON

class BlockPopUpVC1: BaseVC {

    var partner_id: String?
    @IBOutlet weak var lbl_content: UILabel!
    @IBOutlet weak var uiv_dlg: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.lbl_content.text = "If you decline further messages and block this user, you won't see them and they won't see you, unless you unblock them in your preferences.\n \nThey will get a nice message letting them know that you're no longer available to chat.\n \nJust checking in, though: Is there anything you would like to report, or something problematic you'd like to let us know about?\n \nBecause we care about your experiences on Dating Kinky."
    }
    
    override func viewDidAppear(_ animated: Bool) {
            
        super.viewDidAppear(animated)
        uiv_dlg.roundCorners([.topLeft, .topRight], radius: 20)
    }
    
    @IBAction func spaceBtnClicked(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func reportBtnClicked(_ sender: Any) {
        let tovc = self.createViewControllerwithStoryBoardName(StoryBoards.CHAT, controllerName: "ReportNav")
        /*self.navigationController?.pushViewController(tovc, animated: true)*/
        tovc.modalPresentationStyle = .fullScreen
        self.present(tovc, animated: true, completion: nil)
    }
    
    @IBAction func declineBtnClicked(_ sender: Any) {
        // send decline message to other patner
        let timeNow = Int(NSDate().timeIntervalSince1970) * 1000
         var chatObject = [String: String]()
         // MARK: for message object for partner - chatObject
        chatObject["message"]     = "Thank you for the conversation. I’m flattered. At this time, I don’t think we’re a good match. Best of luck to you! \(thisuser!.username) is no longer available to chat."
        chatObject["image"]       = ""
        chatObject["photo"]       = thisuser!.userAvatar
        chatObject["sender_id"]   = "\(thisuser!.id!)"
        chatObject["time"]        = "\(timeNow)" as String
        chatObject["name"]        = "\(thisuser!.username)"
        let roomId1   = friendID! + "_" + "\(thisuser!.id!)"
        // MARK: for partner message send action
        FirebaseAPI.sendMessage(chatObject, roomId1) { (status, message) in
             if status {
                //goto decline
                let ref = Database.database().reference()
                let userchattingRoom = "u" + thisuser!.id!
                if let partnerid = self.partner_id{
                    ref.child("list").child(userchattingRoom).child("u" + partnerid).child("isBlock").setValue("true")
                    self.showLoadingView(vc: self)
                    ApiManager.addBlockUsers(blocks: partnerid) { (isSuccess, data) in
                        self.hideLoadingView()
                        if isSuccess{
                            dump(blockusers,name: "thisisblockusers")
                            DispatchQueue.main.async {
                                self.gotoTabIndex(0)
                            }
                        }
                    }
                }
             }
        }
    }
}
