//
//  VideoVerifyPopUpVC.swift
//  DatingKinky
//
//  Created by top Dev on 8/14/20.
//  Copyright © 2020 Ubuntu. All rights reserved.
//

import UIKit
import ZiggeoSwiftFramework
import SwiftyJSON
import Firebase
import FirebaseAuth
import FirebaseStorage
import FirebaseDatabase

class VideoVerifyPopUpVC: BaseVC, ZiggeoVideosDelegate{

    @IBOutlet weak var lbl_content: UILabel!
    @IBOutlet weak var uiv_dlg: UIView!
    @IBOutlet weak var lbl_privacyContent: UILabel!
    @IBOutlet weak var lbl_title: UILabel!
    @IBOutlet weak var uiv_back: UIView!
    @IBOutlet weak var cus_activity: UIActivityIndicatorView!
    var message_num4videorecording = "0"
    var m_ziggeo: Ziggeo! = nil
    let pathList = Database.database().reference().child("list")
    var partnerListroomId = ""
    var mestatusroomID = ""
    var meListroomId = "u" + "\(thisuser!.id!)"
    var messageNum: Int = 0
    var isBlock: String? = "false"
    let reset_path4content = Database.database().reference().child("message")
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.lbl_content.text = "Dating Kinky's Video Verification is a 20-second video that requires mutual consent & sharing.\n \nOnce you've recorded your video and approved it, your chat mate will be asked if they want to video verify. If they agree, they will have 3 minutes to record and approve their video. Then, both videos will be made available for 5 minutes for each of you before being deleted."
        self.lbl_privacyContent.text = "We care about your privacy here on Dating Kinky! Please consider that even thought videos can be deleted, that there are way to save them(like recording with another device). So, if your privacy/anonymity is important to you, please"
        m_ziggeo = Ziggeo(token: Constants.ziggeo_app_token)
        m_ziggeo.enableDebugLogs = true
        m_ziggeo.videos.delegate = self
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        uiv_dlg.roundCorners([.topLeft, .topRight], radius: 20)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.creatNav(.videoVerifyRecording)
        self.uiv_dlg.isHidden = false
        // set message part
        if let friendid = friendID{
            partnerListroomId = "u" + friendid
        }
        mestatusroomID = friendID! + "_" + "\(thisuser!.id!)"
        self.uiv_back.isHidden = true
        self.cus_activity.isHidden = true
    }
    
    
    @IBAction func guideCheckBtnClicked(_ sender: Any) {
        self.gotoWebViewWithProgressBarModal(Constants.VIDEOVERIFY_TERMS_LINK)
    }
    
    @IBAction func spaceBtnClicked(_ sender: Any) {
        self.closeMenu(.videoVerifyPop)
    }
    
    @IBAction func topBtnClicked(_ sender: Any) {
        self.closeMenu(.videoVerifyPop)
    }
    
    @IBAction func doitBtnClicked(_ sender: Any) {
        self.closeMenu(.videoVerifyPop)
        if let friendid = friendID{
            ApiManager.chatRequest(receiver_id: friendid, requestType: RequestType.chat_verifysend.rawValue) { (isSuccess, data) in
                if isSuccess{
                    self.gotoRecord()
                }
            }
        }
    }
    
    @IBAction func waitBtnClicked(_ sender: Any) {
        self.closeMenu(.videoVerifyPop)
    }
    
    func gotoRecord() {
        DispatchQueue.main.async {
            let recorder = ZiggeoRecorder(application: self.m_ziggeo);
            recorder.coverSelectorEnabled = false;
            recorder.recordedVideoPreviewEnabled = true;
            recorder.cameraFlipButtonVisible = true;
            recorder.cameraDevice = UIImagePickerController.CameraDevice.front;
            recorder.maxRecordedDurationSeconds = 20; //infinite
            recorder.showLightIndicator = false
            let customPreview = CustomPreviewController();
            recorder.videoPreview = customPreview;
            customPreview.previewDelegate = recorder;
            recorder.modalPresentationStyle = .fullScreen
            self.present(recorder, animated: true, completion: nil);
        }
    }
    
    public func videoUploadComplete(_ sourcePath: String, token: String, response: URLResponse?, error: NSError?, json:  NSDictionary?) {
        // set recordig end status and check receiver's status
        if let friendid = friendID{
            //self.showLoadingView(vc: self)
            DispatchQueue.main.async {
                self.cus_activity.isHidden = false
                self.uiv_back.isHidden = false
                self.cus_activity.startAnimating()
            }
            ApiManager.setRecordingEnd_CheckOtherStatus(receiver_id: friendid, owner: PARAMS.SENDER) { (recordingSuccess, data) in
                //self.hideLoadingView()
                DispatchQueue.main.async {
                    self.cus_activity.stopAnimating()
                }
                if recordingSuccess{
                    let other_status = JSON(data as Any)["other_status"].intValue
                    if other_status == 0{// receiver recording complete
                        // send message action for partner
                        self.sendMessage4Partner(token)
                        self.dismiss(animated: true, completion: nil)
                    }else{// receiver recording not completed
                        // save action
                        self.showToast("Please wait till other user upload their video.")
                        print("other user still don't record")
                        UserDefault.setString(key: PARAMS.VERIFY_TOKEN, value: token)
                        UserDefault.Sync()
                        self.dismiss(animated: true, completion: nil)
                    }
                }else{
                    print("server error")
                }
            }
        }
    }
    
    func sendMessage4Partner(_ token: String)  {
        let timeNow = Int(NSDate().timeIntervalSince1970) * 1000
        var chatObject = [String: String]()
        chatObject["message"]     = "video verification"
        chatObject["image"]       = token
        chatObject["photo"]       = thisuser!.userAvatar
        chatObject["sender_id"]   = "\(thisuser!.id!)"
        chatObject["time"]        = "\(timeNow)" as String
        chatObject["name"]        = "\(thisuser!.username)"
        let roomId  = friendID! + "_" + "\(thisuser!.id!)"
        FirebaseAPI.sendMessage(chatObject, roomId) { (status, message) in
            if status{
                self.resetContentAsFullPath(message)
            }else{
                print("firebase error")
            }
        }
    }
    
    func resetContentAsFullPath(_ full_path: String) {
        let fullpath_arr = full_path.split(separator: "/")
        let roomid = fullpath_arr[3]
        let lastpath = fullpath_arr.last
        if let lastpath = lastpath{
            reset_path4content.child(String(roomid)).child(String(lastpath)).child("message").setValue(lastpath)
        }
    }
}
