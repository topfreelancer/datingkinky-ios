//
//  VideoVerifyAcceptPopUpVC.swift
//  DatingKinky
//
//  Created by top Dev on 10/21/20.
//  Copyright © 2020 Ubuntu. All rights reserved.
//


import UIKit
import ZiggeoSwiftFramework
import SwiftyJSON
import Firebase
import FirebaseAuth
import FirebaseStorage
import FirebaseDatabase

class VideoVerifyAcceptPopUpVC : BaseVC, ZiggeoVideosDelegate{

    @IBOutlet weak var lbl_content: UILabel!
    @IBOutlet weak var uiv_dlg: UIView!
    @IBOutlet weak var lbl_privacyContent: UILabel!
    @IBOutlet weak var lbl_title: UILabel!
    @IBOutlet weak var cus_activity: UIActivityIndicatorView!
    @IBOutlet weak var uiv_back: UIView!
    var m_ziggeo: Ziggeo! = nil
    let pathList = Database.database().reference().child("list")
    var partnerListroomId = ""
    var mestatusroomID = ""
    var meListroomId = "u" + "\(thisuser!.id!)"
    var messageNum: Int = 0
    var isBlock: String? = "false"
    let reset_path4content = Database.database().reference().child("message")
    var ds_peoples = [PeopleModel]()
    var messsageNum4VideoRecording = "0"
    
    var my_chatting_roomid = ""
    var partner_chatting_roomid = ""
    override func viewDidLoad() {
        super.viewDidLoad()

        self.lbl_content.text = "To video verify with \(request_user_name ?? ""). They have already recorded and approved a 20-second video for you. If you'd like to see that video and verify yourself, record your own video, approve it, and BOTH videos will be unlocked, and you will have 5 minutes to view before deletion.\n \nIf you choose to decline, that's cool. Their video will simply be deleted."
        self.lbl_privacyContent.text = "We care about your privacy here on Dating Kinky! Please consider that even thought videos can be deleted, that there are way to save them(like recording with another device). So, if your privacy/anonymity is important to you, please"
        
        m_ziggeo = Ziggeo(token: Constants.ziggeo_app_token)
        m_ziggeo.enableDebugLogs = true
        m_ziggeo.videos.delegate = self
        let downSwipee = UISwipeGestureRecognizer(target: self, action: #selector(handleSwipess(_:)))
        downSwipee.direction = .down
        view.addGestureRecognizer(downSwipee)
    }
    
    @objc func handleSwipess(_ sender:UISwipeGestureRecognizer) {
        if (sender.direction == .down) {
            if let userid = friendID{
                self.showLoadingView(vc: self)
                ApiManager.chatRequest(receiver_id: userid, requestType: RequestType.chat_verifyreject.rawValue) { (isSuccess, data) in
                    self.hideLoadingView()
                    self.dismiss(animated: true, completion: nil)
                }
            }
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        uiv_dlg.roundCorners([.topLeft, .topRight], radius: 20)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.creatNav(.videoVerifyRecording)
        self.uiv_dlg.isHidden = false
        // set message part
        if let friendid = request_user_id{
            my_chatting_roomid   = "\(thisuser!.id!)" + "_" +  friendid
            partner_chatting_roomid = friendid + "_" + "\(thisuser!.id!)"
            partnerListroomId = "u" + friendid
            mestatusroomID = friendid + "_" + "\(thisuser!.id!)"
        }
        cus_activity.isHidden = true
        uiv_back.isHidden = true
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
    }
    
    @IBAction func guideCheckBtnClicked(_ sender: Any) {
        self.gotoWebViewWithProgressBarModal(Constants.VIDEOVERIFY_TERMS_LINK)
    }
    
    @IBAction func spaceBtnClicked(_ sender: Any) {
        if let userid = friendID{
            self.showLoadingView(vc: self)
            ApiManager.chatRequest(receiver_id: userid, requestType: RequestType.chat_verifyreject.rawValue) { (isSuccess, data) in
                self.hideLoadingView()
                self.dismiss(animated: true, completion: nil)
            }
        }
    }
    
    @IBAction func doitBtnClicked(_ sender: Any) {
        if let friendid = request_user_id{
            self.showLoadingView(vc: self)
            ApiManager.chatRequest(receiver_id: friendid, requestType: RequestType.chat_verifyaccept.rawValue) { (isSuccess, data) in
                self.hideLoadingView()
                if isSuccess{
                    self.gotoRecord()
                }
            }
        }
    }
    @IBAction func topBtnClicked(_ sender: Any) {
        if let userid = friendID{
            self.showLoadingView(vc: self)
            ApiManager.chatRequest(receiver_id: userid, requestType: RequestType.chat_verifyreject.rawValue) { (isSuccess, data) in
                self.hideLoadingView()
                self.dismiss(animated: true, completion: nil)
            }
        }
    }
    
    @IBAction func waitBtnClicked(_ sender: Any) {
        if let userid = friendID{
            self.showLoadingView(vc: self)
            ApiManager.chatRequest(receiver_id: userid, requestType: RequestType.chat_verifyreject.rawValue) { (isSuccess, data) in
                self.hideLoadingView()
                self.dismiss(animated: true, completion: nil)
            }
        }
    }
    
    func gotoRecord() {
        let recorder = ZiggeoRecorder(application: m_ziggeo);
        recorder.coverSelectorEnabled = false;
        recorder.recordedVideoPreviewEnabled = true;
        recorder.cameraFlipButtonVisible = true;
        recorder.cameraDevice = UIImagePickerController.CameraDevice.front;
        recorder.maxRecordedDurationSeconds = 20; //infinite
        let customPreview = CustomPreviewController();
        recorder.videoPreview = customPreview;
        customPreview.previewDelegate = recorder;
        recorder.modalPresentationStyle = .fullScreen
        self.present(recorder, animated: true, completion: nil);
    }
    
    public func videoUploadComplete(_ sourcePath: String, token: String, response: URLResponse?, error: NSError?, json:  NSDictionary?) {
        if let friendid = friendID{
            //self.showLoadingView(vc: self)
            DispatchQueue.main.async {
                self.cus_activity.isHidden = false
                self.uiv_back.isHidden = false
                self.cus_activity.startAnimating()
            }
            
            ApiManager.setRecordingEnd_CheckOtherStatus(receiver_id: friendid, owner: PARAMS.RECEIVER) { (recordingSuccess, data) in
                
                //self.hideLoadingView()
                DispatchQueue.main.async {
                    self.cus_activity.stopAnimating()
                }
                
                if recordingSuccess{
                    let other_status = JSON(data as Any)["other_status"].intValue
                    if other_status == 0{// receiver recording complete
                        // send message action for partner
                        self.sendMessage4Partner(token)
                    }else{// receiver recording not completed
                        // save action
                        UserDefault.setString(key: PARAMS.VERIFY_TOKEN, value: token)
                        UserDefault.Sync()
                        self.dismiss(animated: true, completion: nil)
                    }
                }else{
                    print("server error")
                }
            }
        }
    }
    
    func sendMessage4Partner(_ token: String)  {
        if let userid = friendID{
            let roomId   = userid + "_" + "\(thisuser!.id!)"
            let timeNow = Int(NSDate().timeIntervalSince1970) * 1000
            var chatObject = [String: String]()
            chatObject["message"]     = "video verification"
            chatObject["image"]       = token
            chatObject["photo"]       = thisuser!.userAvatar
            chatObject["sender_id"]   = "\(thisuser!.id!)"
            chatObject["time"]        = "\(timeNow)" as String
            chatObject["name"]        = "\(thisuser!.username)"
            FirebaseAPI.sendMessage(chatObject, roomId) { (status, message) in
                if status{
                    self.resetContentAsFullPath(message)
                }else{
                    print("firebase error")
                }
            }
        }
    }
    
    func resetContentAsFullPath(_ full_path: String) {
        let fullpath_arr = full_path.split(separator: "/")
        let roomid = fullpath_arr[3]
        let lastpath = fullpath_arr.last
        if let lastpath = lastpath{
            reset_path4content.child(String(roomid)).child(String(lastpath)).child("message").setValue(lastpath)
            self.dismiss(animated: true, completion: nil)
        }
    }
}

