//
//  VideoPopDlg.swift
//  DatingKinky
//
//  Created by top Dev on 8/29/20.
//  Copyright © 2020 Ubuntu. All rights reserved.
//

import UIKit
import AVFoundation

class VideoPopDlg: BaseVC{

    @IBOutlet weak var uiv_dlg: UIView!
    @IBOutlet weak var imv_profile: UIImageView!
    @IBOutlet weak var lbl_privacyContent: UILabel!
    
    var str_iamge_url: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if let url = self.str_iamge_url{
            setImageWithURL(url, imv: self.imv_profile)
        }
        self.lbl_privacyContent.text = "We care about your privacy here on Dating Kinky! Please consider that even thought videos can be deleted, that there are way to save them(like recording with another device). So, if your privacy/anonymity is important to you, please"
        let downSwipee = UISwipeGestureRecognizer(target: self, action: #selector(handleSwipess(_:)))
        downSwipee.direction = .down
        view.addGestureRecognizer(downSwipee)
    }
    
    @objc func handleSwipess(_ sender:UISwipeGestureRecognizer) {
        if (sender.direction == .down) {
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        //creatNav(.videoVerifyRecording)
        super.viewDidAppear(animated)
        uiv_dlg.roundCorners([.topLeft, .topRight], radius: 20)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    @IBAction func guideCheckBtnClicked(_ sender: Any) {
        self.gotoWebViewWithProgressBarModal(Constants.VIDEOVERIFY_TERMS_LINK)
    }
    
    @IBAction func shareBtnClicked(_ sender: Any) {
        if let friendid = friendID{
            self.showLoadingView(vc: self)
            ApiManager.chatRequest(receiver_id: friendid, requestType: RequestType.video_chat_send.rawValue){ (isSuccess, data) in
            self.hideLoadingView()
                if isSuccess{
                    let storyBoard : UIStoryboard = UIStoryboard(name: StoryBoards.CHAT, bundle: nil)
                    let toVC = storyBoard.instantiateViewController( withIdentifier: "VideoChatViewController") as! VideoChatViewController
                    var roomId = "video_default"
                    if let userid = thisuser!.id , let friendid = friendID{
                        if userid > friendid{
                            roomId = userid + "_" + friendid
                        }else{
                            roomId = friendid + "_" + userid
                        }
                    }
                    toVC.roomID = "video_" + roomId
                    print("this is room id for video chattingroom ===>", toVC.roomID)
                    let navViewController = UINavigationController(rootViewController: toVC)
                    navViewController.modalPresentationStyle = .fullScreen
                    self.present(navViewController, animated: false, completion: nil)
                }
            }
        }
    }
    @IBAction func endBtnClicked(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func topBtnClicked(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
}































