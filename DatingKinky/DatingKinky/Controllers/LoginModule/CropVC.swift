//
//  CropVC.swift
//  emoglass
//
//  Created by AngelDev on 7/19/20.
//  Copyright © 2020 Mac. All rights reserved.
//

import UIKit

class CropVC: UIViewController {
    
    var cropPhotoDelegate : CropPhotoDelegate?
    var editView : LyEditImageView?

    override func viewDidLoad() {
        super.viewDidLoad()
        
        NotificationCenter.default.addObserver(self, selector: #selector(gotoCrop),name: NSNotification.Name(rawValue: "Crop"), object: nil)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        
        //NotificationCenter.default.removeObserver(self)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        editView = LyEditImageView(frame: self.view.frame)
        
        //let image = UIImage(named: "IMG_2796.JPG")!
        if let image = tocropImage{
            editView!.initWithImage(image: image)
            
            self.view.addSubview(editView!)
            self.view.backgroundColor = UIColor.clear
        }
    }
    
    override func viewWillLayoutSubviews() {
        
    }
    
    @objc func gotoCrop(){
        
        tocropImage = nil
        croppedImage = editView!.getCroppedImage()
        NotificationCenter.default.post(name:Notification.Name(rawValue: "photosend"), object: nil)
        self.dismiss(animated: true, completion: nil)
        self.dismiss(animated: false, completion: nil)
         // getting action cropped image
        //cropPhotoDelegate?.didSelectImage(image: croppedImage)
        //cropPhotoDelegate = nil
        
    }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }

}
