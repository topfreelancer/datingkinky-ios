//
//  TellusVC.swift
//  DatingKinky
//
//  Created by Ubuntu on 7/24/20.
//  Copyright © 2020 Ubuntu. All rights reserved.
//

import UIKit

var str_yourGender = ""
var str_primaryKinkRole = ""
var str_yourPronouns = ""
var str_yourSexualOrientations = ""
var str_yourRelationShipStyle = ""
var str_yourRelationShipStatus = ""
var str_aboutYou = ""
var str_aboutYourRelationShipStatus = ""

class TellusVC: BaseVC, MSDropDownDelegate {
    
    var gender_Options = [KeyValueModel]()
    var kinkRole_Options = [KeyValueModel]()
    var pronouns_Options = [KeyValueModel]()
    var sexualOrientation_Options = [KeyValueModel]()
    var relationsShipStyle_Options = [KeyValueModel]()
    var relationsShipStatus_Options = [KeyValueModel]()
    
    func dropdownSelected(tagId: Int, answer: String, value: String, isSelected: Bool) {
        //print("\(answer) value= \(value)")
        switch tagId {
        case dropDownTag.yourRelationShipStatus.rawValue:
            print("\(value)")
            str_yourRelationShipStatus = value
            break
        case dropDownTag.yourRelationShipStyle.rawValue:
            print("\(value)")
            str_yourRelationShipStyle = value
            break
        case dropDownTag.yourSexualOrientation.rawValue:
            print("\(value)")
            str_yourSexualOrientations = value
            break
        case dropDownTag.yourPronouns.rawValue:
            print("\(value)")
            str_yourPronouns = value
            break
        case dropDownTag.yourPrimaryRole.rawValue:
            print("\(value)")
            str_primaryKinkRole = value
            break
        case dropDownTag.yourGender.rawValue:
            print("\(value)")
            str_yourGender = value
            break
        default:
            print("default")
        }
    }
    
    @IBOutlet weak var cus_gender: MSDropDown!
    @IBOutlet weak var cus_kinkroles: MSDropDown!
    @IBOutlet weak var cus_pronouns: MSDropDown!
    @IBOutlet weak var cus_sexual: MSDropDown!
    @IBOutlet weak var cus_relation_style: MSDropDown!
    @IBOutlet weak var cus_relation_status: MSDropDown!
    @IBOutlet weak var txv_aboutYou: UITextView!
    @IBOutlet weak var txv_relationShips: UITextView!
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        txv_aboutYou.textContainerInset = UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10)
        txv_relationShips.textContainerInset = UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10)
        
        NotificationCenter.default.addObserver(self, selector: #selector(getMoreIdentify),name: NSNotification.Name(rawValue: "getMoreIdentify"), object: nil)

    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        
       // NotificationCenter.default.removeObserver(self)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(animated)
        self.setDropDownDataSource()
        animVC = .moreIdentify
        creatNav(.moreIdentify)
    }
    
     @objc func getMoreIdentify(){
        // set action
        
     }
    
    func setDelegate4DropDowns() {
        
        self.cus_gender.keyvalueCount = self.gender_Options.count
        self.cus_gender.delegate = self
        self.cus_gender.keyValues = self.gender_Options
        self.cus_gender.isMultiSelect = false
        self.cus_gender.tag = dropDownTag.yourGender.rawValue

        self.cus_kinkroles.keyvalueCount = self.kinkRole_Options.count
        self.cus_kinkroles.delegate = self
        self.cus_kinkroles.keyValues = self.kinkRole_Options
        self.cus_kinkroles.isMultiSelect = false
        self.cus_kinkroles.tag = dropDownTag.yourPrimaryRole.rawValue

        self.cus_pronouns.keyvalueCount = self.pronouns_Options.count
        self.cus_pronouns.delegate = self
        self.cus_pronouns.keyValues = self.pronouns_Options
        self.cus_pronouns.isMultiSelect = false
        self.cus_pronouns.tag = dropDownTag.yourPronouns.rawValue

        self.cus_sexual.keyvalueCount = self.sexualOrientation_Options.count
        self.cus_sexual.delegate = self
        self.cus_sexual.keyValues = self.sexualOrientation_Options
        self.cus_sexual.isMultiSelect = false
        self.cus_sexual.tag = dropDownTag.yourSexualOrientation.rawValue

        self.cus_relation_style.keyvalueCount = self.relationsShipStyle_Options.count
        self.cus_relation_style.delegate = self
        self.cus_relation_style.keyValues = self.relationsShipStyle_Options
        self.cus_relation_style.isMultiSelect = false
        self.cus_relation_style.tag = dropDownTag.yourRelationShipStyle.rawValue

        self.cus_relation_status.keyvalueCount = self.relationsShipStatus_Options.count
        self.cus_relation_status.delegate = self
        self.cus_relation_status.keyValues = self.relationsShipStatus_Options
        self.cus_relation_status.isMultiSelect = false
        self.cus_relation_status.tag = dropDownTag.yourRelationShipStatus.rawValue
    }
    
    func setDropDownDataSource() {
        
        //self.showLoadingView(vc: self)
        
        self.gender_Options.removeAll()
        self.kinkRole_Options.removeAll()
        self.pronouns_Options.removeAll()
        self.sexualOrientation_Options.removeAll()
        self.relationsShipStyle_Options.removeAll()
        self.relationsShipStatus_Options.removeAll()
       
        var genderNum = 0
        var kinkRoleNum = 0
        var pronounsNum = 0
        var sexualNum = 0
        var relationStyleNum = 0
        var relationStatusNum = 0
        
        for one in DS_genders{
            genderNum += 1
            self.gender_Options.append(KeyValueModel(key: one.valueID!, value: one.valueName!))
            if genderNum == DS_genders.count{
                
                for one in DS_kinkRoles{
                    kinkRoleNum += 1
                    self.kinkRole_Options.append(KeyValueModel(key: one.valueID!, value: one.valueName!))
                    if kinkRoleNum == DS_kinkRoles.count{
                        
                        for one in DS_pronouns{
                            pronounsNum += 1
                            self.pronouns_Options.append(KeyValueModel(key: one.valueID!, value: one.valueName!))
                            if pronounsNum == DS_pronouns.count{
                                
                                for one in DS_orientations{
                                    sexualNum += 1
                                    self.sexualOrientation_Options.append(KeyValueModel(key: one.valueID!, value: one.valueName!))
                                    if sexualNum == DS_orientations.count{
                                        
                                        for one in DS_relationShipStyles{
                                            relationStyleNum += 1
                                            self.relationsShipStyle_Options.append(KeyValueModel(key: one.valueID!, value: one.valueName!))
                                            if relationStyleNum == DS_relationShipStyles.count{
                                                
                                                for one in DS_relationShipStatues{
                                                    relationStatusNum += 1
                                                    self.relationsShipStatus_Options.append(KeyValueModel(key: one.valueID!, value: one.valueName!))
                                                    if relationStatusNum == DS_relationShipStatues.count{
                                                        //self.hideLoadingView()
                                                        self.setDelegate4DropDowns()
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    
    @IBAction func nextBtnClicked(_ sender: Any) {
        animVC = nil
        str_aboutYou = self.txv_aboutYou.text
        str_aboutYourRelationShipStatus = self.txv_relationShips.text
        
        if str_yourGender == ""{
            self.showAlerMessage(message: Messages.SELECT_YOUR_GENDER)
            return
        }
        if str_primaryKinkRole == ""{
            self.showAlerMessage(message: Messages.SELECT_PRIMARY_KINKROLEOPTIONS)
            return
        }
        if str_yourPronouns == ""{
            self.showAlerMessage(message: Messages.SELECT_YOUR_PRONOUNS)
            return
        }
        if str_yourSexualOrientations == ""{
            self.showAlerMessage(message: Messages.SELECT_YOUR_SEXUALORIENTATION)
            return
        }
        if str_yourRelationShipStyle == ""{
            self.showAlerMessage(message: Messages.SELECT_YOUR_RELATIONSHIPSTYLE)
            return
        }
        if str_yourRelationShipStatus == ""{
            self.showAlerMessage(message: Messages.SELECT_YOUR_RELATIONSHIPSTATUS)
            return
        }
        if str_aboutYou == ""{
            self.showAlerMessage(message: Messages.WRITE_ABOUTYOU)
            return
        }
        if str_aboutYourRelationShipStatus == ""{
            self.showAlerMessage(message: Messages.WRITE_YOUR_RELATIONSHIPS)
            return
        }else{
            self.gotoNavPresent(VCs.RESISTER_LOOKING_FOR, fullscreen: true)
        }
        
    }
    
    @IBAction func showMulitSelection(_ sender: Any) {
        self.openMenu(.moreIdentify)
    }
    
    @IBAction func skipBtnClicked(_ sender: Any) {
        animVC = nil
        self.gotoNavPresent(VCs.RESISTER_LOOKING_FOR, fullscreen: true)
    }
}

/* "pronoun": "string",
  "location": "so",
  "orientation": "string",
  "coordinates": [
    "lat": 0,
    "lng": 0
  ],
  "gender": "string",
  "hereFor": "string",
  "relationshipStatus": "string",
  "relationshipStyle": "string",
  "relationshipStyleDetails": "string",
  "kinkRole": "string",
  "aboutMe": "string",
  "aboutUs": "string",
  "aboutYou": "string",
  "firstMeeting": "string",
  "appearance": [
    "ethnicity": "string",
    "ethnicDetails": "string",
    "height": "string",
    "bodyType": "string",
    "hairColour": "string",
    "eyeColour": "string"
  ],
  "lifeStyle": [
    "ownWords": "string",
    "smoke": "string",
    "drink": "string",
    "exerciseFrequency": "string",
    "religion": "string",
    "workDetails": "string",
    "eatingHabits": "string",
    "safeSex": "string",
    "safeSexDetails": "string",
    "fetlife": "string"
  ],
  "kinkStyle": [
    "fetlife": "string",
    "identifiesAs": [
      "string"
    ],
    "kinkActivities": [
      "string"
    ],
    "kinkSpheres": [
      "string"
    ],
    "sexAndKinks": [
      "string"
    ]
  ]
]*/

