//
//  BaseVC1.swift
//  Loyaltiyapp
//
//  Created by PSJ on 10/21/19.
//  Copyright © 2019 PSJ. All rights reserved.
//

import UIKit
import Toast_Swift
import SwiftyUserDefaults
import SwiftyJSON
import MBProgressHUD
import Foundation
import Firebase
import FirebaseAuth
import FirebaseStorage
import FirebaseDatabase
import IQKeyboardManagerSwift

var cropPhotoVC :CropPhotoVC!
var datePickerVC :DatePickerVC!
var moreRoleVC :MoreLinkRoleVC!
var moreIdentify :MoreIdentifyVC!
var randomChatPopVC :RandomChatPopUpVC?
var randomChatPopVC1 :RandomeChatPopUpVC1?
//var chatRequestPopUpVC :ChatRequestPopUpVC!
var blockPopVC :BlockPopUpVC!
var blockPopVC1 :BlockPopUpVC1!
var picturePopVC :PictureAttachVC!
var videoVerifyPopVC :VideoVerifyPopUpVC!
var videoVerifyAcceptPopUpVC :VideoVerifyAcceptPopUpVC!
var gifSearchVC:SearchVC!
var darkVC :DarkVC!

class BaseVC: BaseViewController {
    
    var gradientLayer: CAGradientLayer!
    var alertController : UIAlertController? = nil
    var hud: MBProgressHUD?
    //var tabBarVC: RAMAnimatedTabBarController! = nil
    var window: UIWindow?
    var ds_changedchatRequests = [ChatRequestModel]()
    var valueChangedHandle: UInt?
    let base_requestPath = Database.database().reference().child("request")
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
       /* if let tabBarController = self.storyboard?.instantiateViewController(withIdentifier: VCs.HOME_TAB_BAR) as? UITabBarController {
            tabBarController.selectedIndex = 1
        }*/
        IQKeyboardManager.shared.enable = true
        hideKeyboardWhenTappedAround()
        let downSwipe = UISwipeGestureRecognizer(target: self, action: #selector(handleSwipes(_:)))
        /*let rightSwipe = UISwipeGestureRecognizer(target: self, action: #selector(handleSwipes(_:)))*/
        downSwipe.direction = .down
        //rightSwipe.direction = .right
        if let id = thisuser?.id{
            self.chatRequestValueChangedListner( "u" + id)
        }
        view.addGestureRecognizer(downSwipe)
        //view.addGestureRecognizer(rightSwipe)
        //NotificationCenter.default.addObserver(self, selector: #selector(gotoChattingRoom),name:.gotoChattingRoom, object: nil)
        
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        
        //NotificationCenter.default.removeObserver(self)
        //NotificationCenter.default.removeObserver(self, name: .gotoChattingRoom, object: <#T##Any?#>)
        /*if let id = thisuser?.id, let valueChangedHandle = valueChangedHandle{
            FirebaseAPI.removeChattingRequestObserver(userRoomid: "u" + id, valueChangedHandle)
        }*/
    }
    
    func setChattingRoomId(_ partnerid: String) -> String! {
        var me_sum = 0
        var partner_sum = 0
        let stringArray1 = thisuser!.id!.components(separatedBy: CharacterSet.decimalDigits.inverted)
        for item in stringArray1 {
            if let number = Int(item) {
                //print("number: \(number)")
                me_sum += number
            }
        }
        //print("this is the mesumm ==> \(me_sum)")
        let stringArray2 = partnerid.components(separatedBy: CharacterSet.decimalDigits.inverted)
        for item in stringArray2 {
            if let number = Int(item) {
                //print("number: \(number)")
                partner_sum += number
            }
        }
        //print("this is the partner_sum ==> \(partner_sum)")
        let total = me_sum + partner_sum
        return "\(total)"
    }
    func gotoWebViewWithProgressBar(_ link: String)  {
        let browser = KAWebBrowser()
        show(browser, sender: nil)
        browser.loadURLString(link)
    }
    
    func gotoWebViewWithProgressBarModal(_ link: String)  {
        let navigationController = KAWebBrowser.navigationControllerWithBrowser()
        navigationController.modalPresentationStyle = .fullScreen
        show(navigationController, sender: nil)
        navigationController.webBrowser()?.loadURLString(link)
        //browser.loadURLString(link)
    }
    
    @objc func gotoChattingRoom(){
        let storyboad = UIStoryboard(name: StoryBoards.CHAT, bundle: nil)
        let messageNav = storyboad.instantiateViewController(withIdentifier: VCs.MESSAGESENDNAV)
        UIApplication.shared.keyWindow?.rootViewController = messageNav
    }
    // listener for the value changed
    func chatRequestValueChangedListner(_ userRoomid : String)  {
        self.ds_changedchatRequests.removeAll()
        //self.showLoadingView(vc: self)HUDHUD()
        var num = 0
        valueChangedHandle = FirebaseAPI.getChatRequestValueChanage(userRoomid : userRoomid ,handler: { (userlist) in
            //print("userlist == ",userlist)
            guard userlist.receiver_id != nil else{
                //self.hideLoadingView()
                return
            }
            num += 1
            self.ds_changedchatRequests.append(userlist)
            
            //if num == self.ds_changedchatRequests.count{
                acceptedUser = self.ds_changedchatRequests.first
                //print(acceptedUser?.state)
                //print(self.valueChangedHandle)
            // set fo rhe first chat handle
            if self.convertToInt(unsigned: self.valueChangedHandle ?? 0) <= 1 {
                if acceptedUser?.state == "accept"{
                    if let acceptedUser = acceptedUser{
                        memberName4ChatRequest = acceptedUser.userName
                        let gender = acceptedUser.userGender == "woman" ? "W" : "M"
                        let showhim = acceptedUser.userPronoun ?? "He/His"
                        let kinkRole = acceptedUser.userKinkRole ?? ""
                        let spaces = String(repeating: " ", count: 1)
                        memberProfile4ChatRequest = acceptedUser.userCurrentAge! + gender + spaces + kinkRole + spaces + "•" + spaces + showhim + spaces + "•" + spaces + acceptedUser.userLocation! + spaces + "•" + spaces + "Last seen"
                        
                        let lastSeenTimeStamp = acceptedUser.lastSeen
                        lastSeenChatRequest = ""
                        let now = Date()
                        let calendar = Calendar.current
                        if let lastDate = getDateAndTime(lastSeenTimeStamp!){
                            print(lastDate as Any)
                            let ageComponents = calendar.dateComponents([.second], from: lastDate, to: now)
                            let seconds = ageComponents.second ?? 0
                            print(seconds)
                            let day = secondsToHoursMinutesSeconds(seconds: seconds).0
                            let hour = secondsToHoursMinutesSeconds(seconds: seconds).1
                            let min = secondsToHoursMinutesSeconds(seconds: seconds).2
                            let secondss = secondsToHoursMinutesSeconds(seconds: seconds).3
                            lastSeenChatRequest! = " " + "\(secondss)" + "s"
                            if min > 0{
                                lastSeenChatRequest! = " " + "\(min)" + "m"
                            }
                            if hour > 0{
                                lastSeenChatRequest! = " " + "\(hour)" + "h"
                            }
                            if day > 0{
                                lastSeenChatRequest! = "\(day)" + "d"
                            }
                        }
                        partnerAge4ChatRequestAccept = acceptedUser.userCurrentAge
                        partnerAvatar4ChatRequest = acceptedUser.userAvatar
                        partnerGender4ChatRequest = gender
                        partnerKinkRole4ChatRequest = acceptedUser.userKinkRole
                        friendID = acceptedUser.receiver_id
                        chattingOption = .direct
                        //NotificationCenter.default.post(name:.gotoChattingRoom, object: nil)
                    }
                }
            }else{
                
            }
        })
    }
    
    func convertToInt(unsigned: UInt) -> Int {
        let signed = (unsigned <= UInt(Int.max)) ?
            Int(unsigned) :
            Int(unsigned - UInt(Int.max) - 1) + Int.min

        return signed
    }
    
    @objc func handleSwipes(_ sender:UISwipeGestureRecognizer) {
        
        if (sender.direction == .down) {
            if let animvc = animVC{
                self.closeMenu(animvc)
                openStatue = false
            }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
   
    func addNavBarImage() {
        let navController = navigationController!
        if #available(iOS 11.0, *) {
            navController.navigationBar.barTintColor = UIColor.init(named: "color_navbar_top")
        } else {
            navController.navigationBar.barTintColor = UIColor.blue
        }
        let image = UIImage(named: "nabar_top_title") //Your logo url here
        let imageView = UIImageView(image: image)
        let bannerWidth = navController.navigationBar.frame.size.width
        let bannerHeight = navController.navigationBar.frame.size.height
        let bannerX = bannerWidth / 2 - (image?.size.width)! / 2
        let bannerY = bannerHeight / 2 - (image?.size.height)! / 2
        imageView.frame = CGRect(x: bannerX, y: bannerY, width: bannerWidth / 2, height: bannerHeight / 2)
        imageView.contentMode = .scaleAspectFit
        navigationItem.titleView = imageView
    }
    
    func addBackButtonNavBar() {
        // if needed i will add
        let btn_back = UIButton(type: .custom)
        btn_back.setImage(UIImage (named: "logout")!.withRenderingMode(.alwaysTemplate), for: .normal)
        btn_back.addTarget(self, action: #selector(addTappedBack), for: .touchUpInside)
        btn_back.imageEdgeInsets = UIEdgeInsets(top: 5, left: 5, bottom: 5, right: 5)
        btn_back.tintColor = UIColor.white
        let barButtonItemBack = UIBarButtonItem(customView: btn_back)
        barButtonItemBack.customView?.widthAnchor.constraint(equalToConstant: 35).isActive = true
        barButtonItemBack.customView?.heightAnchor.constraint(equalToConstant: 35).isActive = true
        self.navigationItem.rightBarButtonItem = barButtonItemBack
//      self.navigationItem.rightBarButtonItems = [barButtonItemLogout/*, barButtonItem2*/]
    }
    
    func addLeftButton4NavBar() {
        // if needed i will add
        let btn_back = UIButton(type: .custom)
        btn_back.setImage(UIImage (named: "btn_back")!.withRenderingMode(.alwaysTemplate), for: .normal)
        btn_back.addTarget(self, action: #selector(addTappedLeftBtn), for: .touchUpInside)
        btn_back.imageEdgeInsets = UIEdgeInsets(top: 5, left: 5, bottom:8, right: 5)
        btn_back.tintColor = UIColor.white
        let barButtonItemBack = UIBarButtonItem(customView: btn_back)
        barButtonItemBack.customView?.widthAnchor.constraint(equalToConstant: 35).isActive = true
        barButtonItemBack.customView?.heightAnchor.constraint(equalToConstant: 35).isActive = true
        self.navigationItem.leftBarButtonItem = barButtonItemBack
//      self.navigationItem.rightBarButtonItems = [barButtonItemLogout/*, barButtonItem2*/]
    }
    
    @objc func addTappedBack() {
        let storyboad = UIStoryboard(name: "Main", bundle: nil)
        let loginNav = storyboad.instantiateViewController(withIdentifier: VCs.LOGINNAV)
        UserDefault.setBool(key: PARAMS.LOGOUT, value: true)
        thisuser!.clearUserInfo()
        UIApplication.shared.keyWindow?.rootViewController = loginNav
    }
    
    @objc func addTappedLeftBtn() {
        if let currentVC = currentVC{
            if currentVC == VCs.MESSAGESEND{
                if let friendid = friendID{
                    self.removeNode4me4finish(friendid)
                    let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                    let toVC = storyBoard.instantiateViewController( withIdentifier: VCs.HOME_TAB_BAR) as! UITabBarController
                    UIApplication.shared.keyWindow?.rootViewController = toVC
                }
            }
        }
    }
    
    func setBaseStatus4me(_ state: String, partnerId: String, completion: @escaping (_ success: Bool) -> ()) {
        base_requestPath.child("u" + thisuser!.id!).child("u" + partnerId).child("state").setValue(state)
        completion(true)
    }
    
    func removeNode4me4finish(_ partnerId: String){
        base_requestPath.child("u" + thisuser!.id!).child("u" + partnerId).removeValue()
    }
    
    func showProgressHUDHUD(view : UIView, mode: MBProgressHUDMode = .annularDeterminate) -> MBProgressHUD {
        let hud = MBProgressHUD .showAdded(to:view, animated: true)
        hud.mode = mode
        hud.label.text = "Loading";
        hud.animationType = .zoomIn
        hud.tintColor = UIColor.red
        hud.contentColor = .darkGray
        return hud
    }
    
    func hideLoadingView() {
       if let hud = hud {
           hud.hide(animated: true)
       }
    }
    
    func removeSpecialCharsFromString(text: String) -> String {
        let okayChars : Set<Character> =
            Set("abcdefghijklmnopqrstuvwxyz ABCDEFGHIJKLKMNOPQRSTUVWXYZ1234567890+-*=(),.:!_")
        return String(text.filter {okayChars.contains($0) })
    }
    
    func showLoadingView(vc: UIViewController, label: String = "") {
        hud = MBProgressHUD .showAdded(to: vc.view, animated: true)
        if label != "" {
            hud!.label.text = label
        }
        hud!.mode = .indeterminate
        hud!.animationType = .zoomIn
        hud!.bezelView.color = .clear
        hud!.bezelView.style = .solidColor
    }
    
    func showLoadingView4RandomChatting(vc: UIViewController, label: String = "") {
        hud = MBProgressHUD .showAdded(to: vc.view, animated: true)
        if label != "" {
            hud!.label.text = label
        }
        hud!.mode = .indeterminate
        hud!.animationType = .zoomIn
        hud!.bezelView.color = .white
    }
    
    func gotoStoryBoardVC(_ storyBoardName: String,name: String, fullscreen: Bool) {
       let storyboad = UIStoryboard(name: storyBoardName, bundle: nil)
       let targetVC = storyboad.instantiateViewController(withIdentifier: name)
       if fullscreen{
           targetVC.modalPresentationStyle = .fullScreen
       }
       self.present(targetVC, animated: false, completion: nil)
    }
    
    func createViewControllerwithStoryBoardName(_ storyBoardName: String, controllerName:
    String) -> UIViewController{
       let storyboad = UIStoryboard(name: storyBoardName, bundle: nil)
       let targetVC = storyboad.instantiateViewController(withIdentifier: controllerName)
       return targetVC
    }
    
    func gotoStoryBoardVCModal(_ name: String) {
        let storyboad = UIStoryboard(name: name, bundle: nil)
        let targetVC = storyboad.instantiateViewController(withIdentifier: name)
        targetVC.modalPresentationStyle = .popover
        //targetVC.modalTransitionStyle = .crossDissolve
        self.present(targetVC, animated: false, completion: nil)
    }
    
    func gotoVCModal(_ name: String) {
        let storyboad = UIStoryboard(name: "Main", bundle: nil)
        let targetVC = storyboad.instantiateViewController(withIdentifier: name)
        targetVC.modalPresentationStyle = .overFullScreen
        //targetVC.modalTransitionStyle = .crossDissolve
        self.present(targetVC, animated: false, completion: nil)
    }
    
    func setEdtPlaceholder(_ edittextfield : UITextField , placeholderText : String, placeColor : UIColor, padding: UITextField.PaddingSide)  {
        edittextfield.attributedPlaceholder = NSAttributedString(string: placeholderText,
        attributes: [NSAttributedString.Key.foregroundColor: placeColor])
        edittextfield.addPadding(padding)
    }
    
    // MARK: UIAlertView Controller
    func alertMake(_ msg : String) -> UIAlertController {
        alertController = UIAlertController(title: nil, message: msg, preferredStyle: .alert)
        alertController!.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
        return alertController!
    }
    
    func alertDisplay(alertController: UIAlertController) {
        self.present(alertController, animated: true, completion: nil)
    }
    
    func gotoVC(_ nameVC: String){
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let toVC = storyBoard.instantiateViewController( withIdentifier: nameVC)
        toVC.modalPresentationStyle = .fullScreen
        self.present(toVC, animated: false, completion: nil)
    }
    
    func gotoNavPresent(_ storyname : String, fullscreen: Bool) {
        let toVC = self.storyboard?.instantiateViewController(withIdentifier: storyname)
        if fullscreen{
            toVC?.modalPresentationStyle = .fullScreen
        }else{
            toVC?.modalPresentationStyle = .popover
        }
        self.navigationController?.pushViewController(toVC!, animated: true)
    }
    
    func goBack() {
        self.navigationController?.popViewController(animated: false)
    }
    
    func goBackHome() {
        self.navigationController?.popToRootViewController(animated: false)
    }
    
       
    func navBarHidden() {
       self.navigationController?.isNavigationBarHidden = true
    }

    func navBarShow() {
        self.navigationController?.isNavigationBarHidden = false
    }
    
   func navBarTransparent() { self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
      self.navigationController?.navigationBar.shadowImage = UIImage()
      self.navigationController?.navigationBar.isTranslucent = true
      self.navigationController?.view.backgroundColor = .clear
    }
    
    func navBarwithColor() { self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
         self.navigationController?.navigationBar.shadowImage = UIImage()
         self.navigationController?.navigationBar.isTranslucent = true
         self.navigationController?.view.backgroundColor = .blue
   }
    
    //MARK:- Toast function
    func showToast(_ message : String) {
        self.view.makeToast(message)
    }
    
    func showToast(_ message : String, duration: TimeInterval = ToastManager.shared.duration, position: ToastPosition = .bottom) {
        self.view.makeToast(message, duration: duration, position: position)
    }
    
    func showToastCenter(_ message : String, duration: TimeInterval = ToastManager.shared.duration) {
        showToast(message, duration: duration, position: .center)
    }
    
    func createRandomChat()  {
        randomChatPopVC = (UIStoryboard(name: StoryBoards.CHAT, bundle: nil).instantiateViewController(withIdentifier: "RandomChatPopUpVC") as! RandomChatPopUpVC)
        if let randomChatPopVC = randomChatPopVC{
            randomChatPopVC.view.frame = CGRect(x:0 , y: UIScreen.main.bounds.height, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
        }
        darkVC = (UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "DarkVC") as! DarkVC)
        darkVC.view.frame = CGRect(x: 0, y: UIScreen.main.bounds.height, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
    }
    
    func creatNav(_ vcType: AnimVC) {
        if vcType == .photoCrop{
            cropPhotoVC = (UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "CropPhotoVC") as! CropPhotoVC)
            cropPhotoVC.view.frame = CGRect(x:0 , y: UIScreen.main.bounds.height, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
            
            darkVC = (UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "DarkVC") as! DarkVC)
            darkVC.view.frame = CGRect(x: 0, y: UIScreen.main.bounds.height, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
        }else if vcType == .moreLinkRole{
            moreRoleVC = (UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "MoreLinkRoleVC") as! MoreLinkRoleVC)
            moreRoleVC.view.frame = CGRect(x:0 , y: UIScreen.main.bounds.height, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
            
            darkVC = (UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "DarkVC") as! DarkVC)
            darkVC.view.frame = CGRect(x: 0, y: UIScreen.main.bounds.height, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
        }else if vcType == .moreIdentify{
            moreIdentify = (UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "MoreIdentifyVC") as! MoreIdentifyVC)
            moreIdentify.view.frame = CGRect(x:0 , y: UIScreen.main.bounds.height, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
            
            darkVC = (UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "DarkVC") as! DarkVC)
            darkVC.view.frame = CGRect(x: 0, y: UIScreen.main.bounds.height, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
        }else if vcType == .randomChat1{
            randomChatPopVC1 = (UIStoryboard(name: StoryBoards.CHAT, bundle: nil).instantiateViewController(withIdentifier: "RandomeChatPopUpVC1") as! RandomeChatPopUpVC1)
            if let randomChatPopVC1 = randomChatPopVC1{
                randomChatPopVC1.view.frame = CGRect(x:0 , y: UIScreen.main.bounds.height, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
            }
        }else if vcType == .datePicker{
            datePickerVC = (UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "DatePickerVC") as! DatePickerVC)
            datePickerVC.view.frame = CGRect(x:0 , y: UIScreen.main.bounds.height, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
            
            darkVC = (UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "DarkVC") as! DarkVC)
            darkVC.view.frame = CGRect(x: 0, y: UIScreen.main.bounds.height, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
        }/*else if vcType == .chatRequest{
            chatRequestPopUpVC = (UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ChatRequestPopUpVC") as! ChatRequestPopUpVC)
            chatRequestPopUpVC.view.frame = CGRect(x:0 , y: UIScreen.main.bounds.height, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
            
            darkVC = (UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "DarkVC") as! DarkVC)
            darkVC.view.frame = CGRect(x: 0, y: UIScreen.main.bounds.height, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
        }*/else if vcType == .gifSearch{
            gifSearchVC = (UIStoryboard(name: StoryBoards.CHAT, bundle: nil).instantiateViewController(withIdentifier: VCs.GIFSEARCH) as! SearchVC)
            gifSearchVC.view.frame = CGRect(x:0 , y: UIScreen.main.bounds.height, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
            
            darkVC = (UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "DarkVC") as! DarkVC)
            darkVC.view.frame = CGRect(x: 0, y: UIScreen.main.bounds.height, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
        }else if vcType == .blockPop{
            blockPopVC = (UIStoryboard(name: StoryBoards.CHAT, bundle: nil).instantiateViewController(withIdentifier: VCs.BLOCKPOPUP) as! BlockPopUpVC)
            blockPopVC.view.frame = CGRect(x:0 , y: UIScreen.main.bounds.height, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
            
            darkVC = (UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "DarkVC") as! DarkVC)
            darkVC.view.frame = CGRect(x: 0, y: UIScreen.main.bounds.height, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
        }else if vcType == .blockPop1{
            blockPopVC1 = (UIStoryboard(name: StoryBoards.CHAT, bundle: nil).instantiateViewController(withIdentifier: VCs.BLOCKPOPUP1) as! BlockPopUpVC1)
            blockPopVC1.view.frame = CGRect(x:0 , y: UIScreen.main.bounds.height, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
            
            /*darkVC = (UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "DarkVC") as! DarkVC)
            darkVC.view.frame = CGRect(x: 0, y: UIScreen.main.bounds.height, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)*/
        }else if vcType == .picturePop{
            picturePopVC = (UIStoryboard(name: StoryBoards.CHAT, bundle: nil).instantiateViewController(withIdentifier: VCs.PICTUREATTACHPOPUP) as! PictureAttachVC)
            picturePopVC.view.frame = CGRect(x:0 , y: UIScreen.main.bounds.height, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
            
            darkVC = (UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "DarkVC") as! DarkVC)
            darkVC.view.frame = CGRect(x: 0, y: UIScreen.main.bounds.height, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
        }else if vcType == .videoVerifyPop{
            videoVerifyPopVC = (UIStoryboard(name: StoryBoards.CHAT, bundle: nil).instantiateViewController(withIdentifier: VCs.VIDEOVERIFYPOP) as! VideoVerifyPopUpVC)
            videoVerifyPopVC.view.frame = CGRect(x:0 , y: UIScreen.main.bounds.height, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
            
            darkVC = (UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "DarkVC") as! DarkVC)
            darkVC.view.frame = CGRect(x: 0, y: UIScreen.main.bounds.height, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
        }
    }
    
    func openRandomChat()  {
        darkVC.view.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
        self.view.addSubview(darkVC.view)
        self.addChild(darkVC)
        let h = self.navigationController?.navigationBar.height
        
        if let randomChatPopVC = randomChatPopVC{
            UIView.animate(withDuration: 0.5, animations: {
                randomChatPopVC.view.frame = CGRect(x: 0, y: -h!, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
                self.view.addSubview(randomChatPopVC.view)
                self.addChild(randomChatPopVC)
            })
        }
    }
    
    func openMenu(_ vcType: AnimVC){
        if vcType == .photoCrop{
            darkVC.view.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
            self.view.addSubview(darkVC.view)
            self.addChild(darkVC)
            _ = self.navigationController?.navigationBar.height
            UIView.animate(withDuration: 0.5, animations: {
                cropPhotoVC.view.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
                self.view.addSubview(cropPhotoVC.view)
                self.addChild(cropPhotoVC)
            })
        }else if vcType == .moreLinkRole{
            darkVC.view.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
            self.view.addSubview(darkVC.view)
            self.addChild(darkVC)
            _ = self.navigationController?.navigationBar.height
            UIView.animate(withDuration: 0.5, animations: {
                moreRoleVC.view.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
                self.view.addSubview(moreRoleVC.view)
                self.addChild(moreRoleVC)
            })
        }else if vcType == .moreIdentify{
            darkVC.view.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
            self.view.addSubview(darkVC.view)
            self.addChild(darkVC)
            _ = self.navigationController?.navigationBar.height
            UIView.animate(withDuration: 0.5, animations: {
                moreIdentify.view.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
                self.view.addSubview(moreIdentify.view)
                self.addChild(moreIdentify)
            })
        }else if vcType == .randomChat1{
           /* darkVC.view.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
            self.view.addSubview(darkVC.view)
            self.addChild(darkVC)*/
           // let h = self.navigationController?.navigationBar.height
            
            if let randomChatPopVC1 = randomChatPopVC1{
                UIView.animate(withDuration: 0.5, animations: {
                    randomChatPopVC1.view.frame = CGRect(x: 0, y:0, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
                    self.view.addSubview(randomChatPopVC1.view)
                    self.addChild(randomChatPopVC1)
                })
            }
        }else if vcType == .datePicker{
            darkVC.view.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
            self.view.addSubview(darkVC.view)
            self.addChild(darkVC)
            _ = self.navigationController?.navigationBar.height
            UIView.animate(withDuration: 0.5, animations: {
                datePickerVC.view.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
                self.view.addSubview(datePickerVC.view)
                self.addChild(datePickerVC)
            })
        }/*else if vcType == .chatRequest{
            darkVC.view.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
            self.view.addSubview(darkVC.view)
            self.addChild(darkVC)
            var h:CGFloat? = 0
            h = self.navigationController?.navigationBar.height
            UIView.animate(withDuration: 0.5, animations: {
                chatRequestPopUpVC.view.frame = CGRect(x: 0, y: -(h ?? 0), width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
                self.view.addSubview(chatRequestPopUpVC.view)
                self.addChild(chatRequestPopUpVC)
            })
        }*/else if vcType == .gifSearch{
            darkVC.view.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
            self.view.addSubview(darkVC.view)
            self.addChild(darkVC)
            _ = self.navigationController?.navigationBar.height
            UIView.animate(withDuration: 0.5, animations: {
                gifSearchVC.view.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
                self.view.addSubview(gifSearchVC.view)
                self.addChild(gifSearchVC)
            })
        }else if vcType == .blockPop{
            darkVC.view.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
            self.view.addSubview(darkVC.view)
            self.addChild(darkVC)
            _ = self.navigationController?.navigationBar.height
            UIView.animate(withDuration: 0.5, animations: {
                blockPopVC.view.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
                self.view.addSubview(blockPopVC.view)
                self.addChild(blockPopVC)
            })
        }else if vcType == .blockPop1{
            /*darkVC.view.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
            self.view.addSubview(darkVC.view)
            self.addChild(darkVC)*/
            _ = self.navigationController?.navigationBar.height
            UIView.animate(withDuration: 0.5, animations: {
                blockPopVC1.view.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
                self.view.addSubview(blockPopVC1.view)
                self.addChild(blockPopVC1)
            })
        }else if vcType == .picturePop{
            darkVC.view.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
            self.view.addSubview(darkVC.view)
            self.addChild(darkVC)
            _ = self.navigationController?.navigationBar.height
            UIView.animate(withDuration: 0.5, animations: {
                picturePopVC.view.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
                self.view.addSubview(picturePopVC.view)
                self.addChild(picturePopVC)
            })
        }else if vcType == .videoVerifyPop{
            darkVC.view.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
            self.view.addSubview(darkVC.view)
            self.addChild(darkVC)
            _ = self.navigationController?.navigationBar.height
            UIView.animate(withDuration: 0.5, animations: {
                videoVerifyPopVC.view.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
                self.view.addSubview(videoVerifyPopVC.view)
                self.addChild(videoVerifyPopVC)
            })
        }
    }
    
    func closeRandomChat(random_switch: Int) {
        let userdata: [String: Any] = ["is_random":random_switch ]
        NotificationCenter.default.post(name:Notification.Name(rawValue: "setRandomSwitch"), object: nil, userInfo: userdata)
        
        darkVC.view.frame = CGRect(x:0 , y: UIScreen.main.bounds.height, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
        darkVC.view.removeFromSuperview()
        
        if let randomChatPopVC = randomChatPopVC{
            UIView.animate(withDuration: 0.5, animations: {
                randomChatPopVC.view.frame = CGRect(x:0 , y: UIScreen.main.bounds.height, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
                randomChatPopVC.view.willRemoveSubview(randomChatPopVC.view)
            })
        }
    }
    
    func closeMenu(_ vcType: AnimVC, randomSwitch: Int = 0){
        if vcType == .photoCrop{
            darkVC.view.frame = CGRect(x:0 , y: UIScreen.main.bounds.height, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
            darkVC.view.removeFromSuperview()
            UIView.animate(withDuration: 0.5, animations: {
                cropPhotoVC.view.frame = CGRect(x:0 , y: UIScreen.main.bounds.height, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
                cropPhotoVC.view.willRemoveSubview(cropPhotoVC.view)
            })
        }else if vcType == .moreLinkRole{
            NotificationCenter.default.post(name:Notification.Name(rawValue: "getMoreLinkRole"), object: nil)
            darkVC.view.frame = CGRect(x:0 , y: UIScreen.main.bounds.height, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
            darkVC.view.removeFromSuperview()
            UIView.animate(withDuration: 0.5, animations: {
                moreRoleVC.view.frame = CGRect(x:0 , y: UIScreen.main.bounds.height, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
                moreRoleVC.view.willRemoveSubview(moreRoleVC.view)
            })
        }else if vcType == .moreIdentify{
            NotificationCenter.default.post(name:Notification.Name(rawValue: "getMoreIdentify"), object: nil)
            darkVC.view.frame = CGRect(x:0 , y: UIScreen.main.bounds.height, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
            darkVC.view.removeFromSuperview()
            UIView.animate(withDuration: 0.5, animations: {
                moreIdentify.view.frame = CGRect(x:0 , y: UIScreen.main.bounds.height, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
                moreIdentify.view.willRemoveSubview(moreIdentify.view)
            })
        }else if vcType == .randomChat1{
            if let randomChatPopVC1 = randomChatPopVC1{
                UIView.animate(withDuration: 0.5, animations: {
                    randomChatPopVC1.view.frame = CGRect(x:0 , y: UIScreen.main.bounds.height, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
                    randomChatPopVC1.view.willRemoveSubview(randomChatPopVC1.view)
                })
            }
            self.closeRandomChat(random_switch: randomSwitch)
        }else if vcType == .datePicker{
            
            NotificationCenter.default.post(name:Notification.Name(rawValue: "sendBirthday"), object: nil)
            
            darkVC.view.frame = CGRect(x:0 , y: UIScreen.main.bounds.height, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
            darkVC.view.removeFromSuperview()
            UIView.animate(withDuration: 0.5, animations: {
                datePickerVC.view.frame = CGRect(x:0 , y: UIScreen.main.bounds.height, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
                datePickerVC.view.willRemoveSubview(datePickerVC.view)
            })
        }/*else if vcType == .chatRequest{
            
            //NotificationCenter.default.post(name:Notification.Name(rawValue: "sendBirthday"), object: nil)
            
            darkVC.view.frame = CGRect(x:0 , y: UIScreen.main.bounds.height, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
            darkVC.view.removeFromSuperview()
            UIView.animate(withDuration: 0.5, animations: {
                chatRequestPopUpVC.view.frame = CGRect(x:0 , y: UIScreen.main.bounds.height, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
                chatRequestPopUpVC.view.willRemoveSubview(chatRequestPopUpVC.view)
            })
        }*/else if vcType == .gifSearch{
            
            NotificationCenter.default.post(name:.gifSearch, object: nil)
            isGifShow = false
            darkVC.view.frame = CGRect(x:0 , y: UIScreen.main.bounds.height, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
            darkVC.view.removeFromSuperview()
            UIView.animate(withDuration: 0.5, animations: {
                gifSearchVC.view.frame = CGRect(x:0 , y: UIScreen.main.bounds.height, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
                gifSearchVC.view.willRemoveSubview(gifSearchVC.view)
            })
        }else if vcType == .blockPop{
            
            //NotificationCenter.default.post(name:.gifSearch, object: nil)
            
            darkVC.view.frame = CGRect(x:0 , y: UIScreen.main.bounds.height, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
            darkVC.view.removeFromSuperview()
            UIView.animate(withDuration: 0.5, animations: {
                blockPopVC.view.frame = CGRect(x:0 , y: UIScreen.main.bounds.height, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
                blockPopVC.view.willRemoveSubview(blockPopVC.view)
            })
        }else if vcType == .blockPop1{
            
            //NotificationCenter.default.post(name:.gifSearch, object: nil)
            
            /*darkVC.view.frame = CGRect(x:0 , y: UIScreen.main.bounds.height, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
            darkVC.view.removeFromSuperview()*/
            UIView.animate(withDuration: 0.5, animations: {
                blockPopVC1.view.frame = CGRect(x:0 , y: UIScreen.main.bounds.height, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
                blockPopVC1.view.willRemoveSubview(blockPopVC1.view)
            })
            self.closeMenu(.blockPop)
        }else if vcType == .picturePop{
            darkVC.view.frame = CGRect(x:0 , y: UIScreen.main.bounds.height, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
            darkVC.view.removeFromSuperview()
            UIView.animate(withDuration: 0.5, animations: {
                picturePopVC.view.frame = CGRect(x:0 , y: UIScreen.main.bounds.height, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
                picturePopVC.view.willRemoveSubview(picturePopVC.view)
            })
        }else if vcType == .videoVerifyPop{
            
            //NotificationCenter.default.post(name:.gifSearch, object: nil)
            
            darkVC.view.frame = CGRect(x:0 , y: UIScreen.main.bounds.height, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
            darkVC.view.removeFromSuperview()
            UIView.animate(withDuration: 0.5, animations: {
                videoVerifyPopVC.view.frame = CGRect(x:0 , y: UIScreen.main.bounds.height, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
                videoVerifyPopVC.view.willRemoveSubview(videoVerifyPopVC.view)
            })
        }else if vcType == .videoVerifyAcceptPop{
            
            //NotificationCenter.default.post(name:.gifSearch, object: nil)
            
            darkVC.view.frame = CGRect(x:0 , y: UIScreen.main.bounds.height, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
            darkVC.view.removeFromSuperview()
            UIView.animate(withDuration: 0.5, animations: {
                videoVerifyAcceptPopUpVC.view.frame = CGRect(x:0 , y: UIScreen.main.bounds.height, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
                videoVerifyAcceptPopUpVC.view.willRemoveSubview(videoVerifyAcceptPopUpVC.view)
            })
        }
    }
    
    func showAlerMessagewithCompletion(message:String){
        let alertController = UIAlertController(title: nil, message:message, preferredStyle: .alert)
        let action1 = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction) in
            if UserDefault.getBool(key: PARAMS.FIRST_LOGIN, defaultValue: true){
                self.gotoVC(VCs.WELCOMENAV)
                UserDefault.setBool(key: PARAMS.FIRST_LOGIN, value: false)
            }else{
                self.gotoVC(VCs.LOGINNAV)
            }
        }
        alertController.addAction(action1)
        self.present(alertController, animated: true, completion: nil)
    }
}


class DarkVC: BaseVC {
    override func viewDidLoad() {
        super.viewDidLoad()
    }
}



