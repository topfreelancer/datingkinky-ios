//
//  Constants.swift
//  emoglass
//
//  Created by Mac on 7/7/20.
//  Copyright © 2020 Mac. All rights reserved.
//

import Foundation
import UIKit
    
struct Constants {
    static let MAPBOX_ACCESS_TOKEN = "pk.eyJ1IjoiaG15b25nc29uZyIsImEiOiJjazc3aTJ6amwwODZnM2hsa2VwZnU5OGxvIn0.WzP_Y-tKr0U1jQWTJMCBeQ"
    static let PHOTO_TERMS_LINK    = "https://datingkinky.com/photo-media-faqs/"
    static let PROGRESS_COLOR      = UIColor.black
    static let SAVE_ROOT_PATH      = "save_root"
    static let SCREEN_HEIGHT       = UIScreen.main.bounds.height
    static let SCREEN_WIDTH        = UIScreen.main.bounds.width
    static let TENOR_API_KEY       = "8QGMQYVHRCBT"
    static let TERMSOF_USE_LINK    = "https://datingkinky.com/terms-of-use/"
    static let GETSTARTED_VIDEO_TOUR_LINK    = "https://datingkinky.com/video-tour/"
    static let COOL_SHIT_SITES_LINK    = "https://datingkinky.com/cool-shit/"
    static let IS_DATINGKINKY_REALLY_FREE_LINK    = "https://datingkinky.com/really-free-kinky-dating-bdsm-dating-not-quite-vanilla-dating/"
    static let SPREAD_WORLD_DATINGKINKY_LINK    = "https://datingkinky.com/spread-the-word/"
    static let READ_MORE_LINK    = "https://datingkinky.com/plus-membership"
    static let VIDEOVERIFY_TERMS_LINK    = "https://datingkinky.com/video-privacy-tips/"
    static let CONTACTUS_LINK    = "https://datingkinky.com/contact/"
    static let deviceToken         = UIDevice.current.identifierForVendor!.uuidString
    static let ziggeo_client_token   = "e814f44890bb072cca7c6c8763ec2b35"
    static let ziggeo_app_token   = "cc8595b93b69d88f7b7572179996fca0"
    static let AppName   = "Dating Kinky"
    static let agoraAppID = "a07aa44092ca4fe8afa40963aac5d7a1"
    //static let agoraAppID = "91d64b25d18b46d7b1bcc822f78b6923"
    //new: bdfdeeea64274b7b9862d8a8e321ec85
    //91d64b25d18b46d7b1bcc822f78b6923
    // learnme: a07aa44092ca4fe8afa40963aac5d7a1
}

struct VCs {
    static let AVATAR               = "AvatarVC"
    static let BLOCKPOPUP           = "BlockPopUpVC"
    static let BLOCKPOPUP1          = "BlockPopUpVC1"
    static let CHAT                 = "ChatVC"
    static let CHAT_REQUEST_SEND     = "ChatRequestSendVC"
    static let CROPPHOTOVC          = "CropPhotoVC"
    static let DATEPICKERVC         = "DatePickerVC"
    static let FORGOT               = "ForgotVC"
    static let FULLPROFILE          = "FullProfileVC"
    static let GIFSEARCH            = "SearchVC"
    static let HOME_TAB_BAR         = "HomeTabBarVC"
    static let IMAGESHOW            = "ImageShowVC"
    static let LOGIN                = "LoginVC"
    static let LOGINNAV             = "LoginNav"
    static let MESSAGESEND          = "MessageSendVC"
    static let MESSAGESENDNAV       = "MessageSendNav"
    static let MORELINKROLE         = "MoreLinkRoleVC"
    static let PEOPLE               = "PeopleVC"
    static let PICTUREATTACHPOPUP   = "PictureAttachVC"
    static let PLUS                 = "PlusVC"
    static let RANDOMCHATPOPUP      = "RandomChatPopUpVC"
    static let REGISTER             = "RegisterVC"
    static let RESET_PWD            = "ResetPwdVC"
    static let RESISTER_LOOKING_FOR = "RegisterLookingFor"
    static let SETTINGS             = "SettingsVC"
    static let SPLASH               = "SplashVC"
    static let TELLS_US             = "TellusVC"
    static let UPLOAD_PHOTO         = "UploadPhotoVC"
    static let VIDEOVERIFYPOP       = "VideoVerifyPopUpVC"
    static let VIDEOVERIFYRECORDING = "VideoVerifyRecordingPopVC"
    static let WELCOMENAV           = "WelcomeNav"
    static let WELCOME_1            = "Welcome1VC"
    static let WELCOME_2            = "Welcome2VC"
    static let WELCOME_3            = "Welcome3VC"
    static let FULLPROFILENAV       = "FullProfileNav"
    static let VIDEOSHOWVC          = "VideoShowVC"
    static let VIDEOVERIFY_ACCEPTVC = "VideoVerifyAcceptPopUpVC"
    static let FIRSTDAY_PROMOTIONVC = "FirstDayPromotionVC"
    static let LAST2_DAY_PROMOTIONVC = "Last2DayPromotionVC"
    static let LAST_DAY_PROMOTIONVC = "LastDayPromotionVC"
    static let VIDEO_CHAT_REQUEST_ACCEPT_POP_VC = "VideoChatRequestAcceptPopVC"
    static let VOICE_CHAT_REQUEST_ACCEPT_POP_VC = "VoiceChatRequestAcceptPopVC"
    static let REPORT_VC = "ReportVC"
}

struct StoryBoards {
    static let CHAT     = "Chat"
    static let MAIN     = "Main"
    static let PLUS     = "Plus"
    static let SETTINGS = "Settings"
}

struct ChattingRuquest {
    static let ACCEPT  = "accept"
    static let FINISH  = "finish"
    static let PENDING = "pending"
    static let REJECT  = "reject"
    static let SEND    = "send"
}

struct Messages {
    static let ALREADY_CONFIRMED              = "Already Confirmed!"
    static let BIRTHDAY_REQUIRE               = "Please input your birthdate!"
    static let BIRTHDAY_VALIDATE              = "Please input your correct birthdate!"
    static let CONFIRMED                      = "Confirmed! Please login again."
    static let CONFIRM_PWD_CHECK              = "Please cofirm your password."
    static let CONFIRM_PWD_REQUIRE            = "Please input your confirmation password."
    static let EMAIL_REQUIRE                  = "Please input your email!"
    static let EMAIL_VALIDATION               = "Invalid email!"
    static let LOCATION_REQUIRE               = "Please input your location!"
    static let NETWORK_ISSUE                  = "Network issue!"
    static let PASSWORD_REQUIRE               = "Please input your password!"
    static let PHONENUMBER_REQUIRE            = "Please input your phonenumber!"
    static let SELECT_LOOKINGFOR_DISTANCE     = "Plese select your lookingfor distance!"
    static let SELECT_LOOKINGFOR_GENDER       = "Plese select your lookingfor gender!"
    static let SELECT_LOOKINGFOR_HERE         = "Plese select your lookingfor option!"
    static let SELECT_PRIMARY_KINKROLEOPTIONS = "Plese select your primary kink role!"
    static let SELECT_YOUR_GENDER             = "Plese select your gender!"
    static let SELECT_YOUR_PRONOUNS           = "Plese select your pronouns!"
    static let SELECT_YOUR_RELATIONSHIPSTATUS = "Plese select your relationship status!"
    static let SELECT_YOUR_RELATIONSHIPSTYLE  = "Plese select your relationship style!"
    static let SELECT_YOUR_SEXUALORIENTATION  = "Plese select your sexual orientation!"
    static let SERVERERROR                    = "Network issue!"
    static let TERMS_CHECK                    = "Whoops! You need to accept our Terms of Use to register."
    static let TOKEN_REQUIRE                  = "Please input your token!"
    static let USERNAME_CONDITION             = "Username must be more than 4 letters and less than 50 letters!"
    static let USERNAME_REQUIRE               = "Please input your username!"
    static let VERIFY_CHECK                   = "To continue, you'll need to verify that you are creating this account for yourself. Thanks!"
    static let WRITE_ABOUTYOU                 = "Plese write about you!"
    static let WRITE_YOUR_RELATIONSHIPS       = "Plese write about your relationships!"
}

enum AnimVC: Int {
    case photoCrop
    case datePicker
    case moreLinkRole
    case moreIdentify
    //case randomChat
    case randomChat1
    //case chatRequest
    case acceptRequest
    case gifSearch
    case blockPop
    case blockPop1
    case picturePop
    case videoVerifyPop
    case videoVerifyRecording
    case videoVerifyAcceptPop
    case normal
}

var animVC: AnimVC? = .normal

var openStatue: Bool = false

var tocropImage: UIImage?
var croppedImage: UIImage?
var cameraPath: String?

enum PickerType: Int {
    case camera
    case library
    case normal
}

enum dropDownTag: Int {
    case lookingDistance        = 1009
    case lookingGender          = 1008
    case lookingfor             = 1007
    case yourGender             = 1001
    case yourPrimaryRole        = 1002
    case yourPronouns           = 1003
    case yourRelationShipStatus = 1006
    case yourRelationShipStyle  = 1005
    case yourSexualOrientation  = 1004
}

var tellUsRole              = [MultiCheckModel]()
var lookingforRole          = [MultiCheckModel]()

// for the send avatar fileName and url for the upload
var fileName: String?
var fileURL: String?
var fileId: String?
let randomInt               = Int.random(in: 0 ..< 9)

var DS_bodyTypes            = [OptionsModel]()
var DS_drink                = [OptionsModel]()
var DS_eatingHabits         = [OptionsModel]()
var DS_ethnicities          = [OptionsModel]()
var DS_exercisesFrequencies = [OptionsModel]()
var DS_eyeColours           = [OptionsModel]()
var DS_genders              = [OptionsModel]()
var DS_hairColours          = [OptionsModel]()
var DS_heights              = [OptionsModel]()
var DS_hereFor              = [OptionsModel]()
var DS_indentifiesAs        = [OptionsModel]()
var DS_kinkactivities       = [OptionsModel]()
var DS_kinkRoles            = [OptionsModel]()
var DS_knikSperes           = [OptionsModel]()
var DS_orientations         = [OptionsModel]()
var DS_pronouns             = [OptionsModel]()
var DS_relationShipStatues  = [OptionsModel]()
var DS_relationShipStyles   = [OptionsModel]()
var DS_religions            = [OptionsModel]()
var DS_safeSex              = [OptionsModel]()
var DS_sexAndKinks          = [OptionsModel]()
var DS_smoke                = [OptionsModel]()

extension NSNotification.Name {
    //static let acceptRequest            = Notification.Name("acceptRequest")
    //static let chatRequest                = Notification.Name("chatRequest")
    static let gifSearch                  = Notification.Name("gifSearch")
    static let gifSend                    = Notification.Name("gifSend")
    static let gifSendSuccess             = Notification.Name("gifSendSuccess")
    //static let gotoChattingRoom           = Notification.Name("gotoChattingRoom")// when goto chatting room when chatting has been accepted from other user
    static let opneGallery                = Notification.Name("openGallery")
    static let reloadTableView            = Notification.Name("reloadTableView")
    static let showVideoVerifyPopDialog   = Notification.Name("showVideoVerifyPopDialog")
    static let randomChattingPopDialog    = Notification.Name("randomChattingPopDialog")
    static let generalChatRequestReceived = Notification.Name("generalChatRequestReceived")
    static let videoChatRequestReceived = Notification.Name("videoChatRequestReceived")
    static let voiceChatRequestReceived = Notification.Name("voiceChatRequestReceived")
    static let videoChatRequestAccepted = Notification.Name("videoChatRequestAccepted")
    static let voiceChatRequestAccepted = Notification.Name("voiceChatRequestAccepted")
    static let videoVerifyRequestReceived = Notification.Name("videoVerifyRequestReceived")
    static let videoVerifyRequestAccepted = Notification.Name("videoVerifyRequestAccepted")
}

var roomID: String?
var memberName4ChatRequest: String?
var memberProfile4ChatRequest: String?
var partnerAge4ChatRequestAccept: String?
var partnerAvatar4ChatRequest: String?
var partnerGender4ChatRequest: String?
var partnerKinkRole4ChatRequest: String?
var partnerPronoun4ChatRequest: String?
var partnerLocation4ChatRequest: String?

var memberName4AccepttRequest: String?
var currentVC: String?
// var selectedGifURL : String?  =  nil

var selectedUser: PeopleModel? // for setting user full profile view and selected chatting user both of them
var lastSeenChatRequest: String?

// MARK: message part

var friendID: String?
var chattingUserName: String?
var chattingUserProfile: String?
//var msgIndexId = ""
enum ChattingOption: Int {
    case chatList // 0
    case direct // 1
    case normal
}

var chattingOption: ChattingOption = .normal
var acceptedUser: ChatRequestModel? // i have used it but i am not sure why i have created this variable

/*
 $5/month: Apple ID: 1539004101 Product ID: dk_5_month
 $30 Gift: Apple ID: 1539004910 Product ID: dk_30_gift
 $60 Year Apple ID: 1539005207 Product ID: dk_60_year
 */

let AUTO_RENEW_SUBSCRIPTION_1MONTH_PRODUCT_ID = "dk_5_month"
let AUTO_RENEW_SUBSCRIPTION_6MONTH_PRODUCT_ID = "dk_30_gift"
let AUTO_RENEW_SUBSCRIPTION_1YEAR_PRODUCT_ID = "dk_60_year"

public let GOOGLE_PLACES_API_KEY = "AIzaSyC_wPfCDzDrdc6veGGgnAZ1qhDHAGwNf2Q"
