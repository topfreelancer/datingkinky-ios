
import Foundation
import Firebase
import SwiftyUserDefaults
import FirebaseDatabase

class FirebaseAPI {
    
    static let ref = Database.database().reference()
    
    //static let CONV_REFERENCE = ""
    static let MESSAGE = "message"
    static let LIST = "list"
    static let STATUS = "status"
    static let CHAT_REQUEST = "request"
    static let RANDOM_CHAT = "random_chat"
    
    // MARK: - random Chat
    static func setRandomChattingDefault(_ userid: String) {
        var random_chatting_default = [String: String]()
        random_chatting_default["id"] = userid
        random_chatting_default["status"] = "1"// status: 0-false, 1-true
        random_chatting_default["room_id"] = ""
        random_chatting_default["response"] = "0"// response: 0-false, 1-true
        ref.child(RANDOM_CHAT).child(userid).setValue(random_chatting_default) { (error, dataRef) in
            if let error = error {
                print(error)
            } else {
                print("randomchatting setting default success")
            }
        }
    }
    
     //MARK: - Set/remove Add/change observer
    static func setMessageListener(_ roomId: String, handler:@escaping (_ msg: ChatModel)->()) -> UInt {
        return ref.child(MESSAGE).child(roomId).observe(.childAdded) { (snapshot, error) in

            let childref = snapshot.value as? NSDictionary
            //print(childref)
            if let childRef = childref {
                let msg = parseMsg(childRef)
                handler(msg)
            }
        }
    }
    
    static func setMessageReadState4Partner(_ roomId: String){
        ref.child(MESSAGE).child(roomId).observe(.childAdded) { (snapshot, error) in

            let childref = snapshot.value as? NSDictionary
            if childref != nil {
                let id = snapshot.key
                let messageFullPath = Database.database().reference().child(MESSAGE).child(roomId).child(id)
                messageFullPath.observeSingleEvent(of: .value, with: { (snapshot1) in
                    if !snapshot1.hasChild("read_time"){
                        messageFullPath.child("read_time").setValue("\(Int(NSDate().timeIntervalSince1970) * 1000)")
                    }
                })
            }
        }
    }
    
    static func removeChattingRoomObserver(_ roomId: String, _ handle : UInt) {
        ref.child(MESSAGE).child(roomId).removeObserver(withHandle: handle)
    }
    
    static func getPartnerOnlinestatus(_ path: String, handler:@escaping (_ msg: StatusModel)->()) -> UInt {
        return ref.child(STATUS).child(path).observe(.value) { (snapshot, error) in

            let childref = snapshot.value as? NSDictionary
            if let childRef = childref {
                //print(childRef)
                let msg = parseStatus(childRef)
                handler(msg)
            } else {
            }
        }
    }
    
    static func removePartnerOnlineStatusObserver(_ path: String, _ handle : UInt) {
        ref.child(STATUS).child(path).removeObserver(withHandle: handle)
    }
    
    static func parseStatus(_ snapshot: NSDictionary) -> StatusModel {
        let status = StatusModel()
        //print(snapshot)
        status.online = snapshot["online"] as! String
        status.sender_id = snapshot["sender_id"] as! String
        status.timesVal = snapshot["time"] as! String
        
        return status
    }
    
    static func parseMsg(_ snapshot: NSDictionary) -> ChatModel {
        let message = ChatModel()
        //print(snapshot)
        
        message.image = snapshot["image"] as! String
        message.msgContent = snapshot["message"] as! String
        message.name = snapshot["name"] as! String
        message.photo = snapshot["photo"] as! String
        message.sender_id = snapshot["sender_id"] as! String
        if let read_time = snapshot["read_time"] as? String{
            message.read_time = read_time
        }
        
        if "\(thisuser!.id!)" == message.sender_id{
            message.me = true
        }
        else{
            message.me = false
        }
        message.timestamp = snapshot["time"] as! String
        return message
    }
    
    static func getChatRequest(userRoomid : String, handler:@escaping (_ chatRequestList: ChatRequestModel)->()) -> UInt {
        // in the firebase don't need to pick the terminal node.. if select top node, then will get all values automatically.
        return ref.child(CHAT_REQUEST).child(userRoomid).observe(.childAdded) { (snapshot, error) in

            let childref = snapshot.value as? NSDictionary
            if let childRef = childref {
                //print(childRef)
                let chatRequestList = parseList4ChatRequest(childRef)
                handler(chatRequestList)
            } else {
                handler(ChatRequestModel())
            }
        }
    }
    
    static func removeChattingRequestObserver(userRoomid: String, _ handle : UInt) {
        ref.child(CHAT_REQUEST).child(userRoomid).removeObserver(withHandle: handle)
    }
    
    static func getChatRequestValueChanage(userRoomid : String, handler:@escaping (_ chatRequestList: ChatRequestModel)->()) -> UInt {
        // in the firebase don't need to pick the terminal node.. if select top node, then will get all values automatically.
        return ref.child(CHAT_REQUEST).child(userRoomid).observe(.childChanged) { (snapshot, error) in

            let childref = snapshot.value as? NSDictionary
            if let childRef = childref {
                //print(childRef)
                let chatRequestList = parseList4ChatRequest(childRef)
                handler(chatRequestList)
            } else {
                handler(ChatRequestModel())
            }
        }
    }
    
    static func parseList4ChatRequest(_ snapshot: NSDictionary) -> ChatRequestModel {

        let chatRequestUser = ChatRequestModel()
        //print("userList ==" ,snapshot)
        if  let snapshotsenderid = snapshot["receiver_id"] as? String, let snapshotuserName = snapshot["receiver_name"] as? String, let snapshotsenderAvatar = snapshot["receiver_avatar"] as? String, let snapshotsenderCurrentAge = snapshot["receiver_currentAge"] as? String, let snapshotsenderGender = snapshot["receiver_gender"] as? String, let snapshotsenderKinkRole = snapshot["receiver_kinkRole"] as? String, let snapshotsenderPronoun = snapshot["receiver_pronoun"] as? String, let snapshotsenderLocation = snapshot["receiver_location"] as? String, let state  = snapshot["state"] as? String, let lastSeen = snapshot["last_seen"] as? String {
            
            chatRequestUser.receiver_id = snapshotsenderid
            chatRequestUser.userName = snapshotuserName
            chatRequestUser.userAvatar = snapshotsenderAvatar
            chatRequestUser.userCurrentAge = snapshotsenderCurrentAge
            chatRequestUser.userGender = snapshotsenderGender
            chatRequestUser.userKinkRole = snapshotsenderKinkRole
            chatRequestUser.userPronoun = snapshotsenderPronoun
            chatRequestUser.userLocation = snapshotsenderLocation
            chatRequestUser.state = state
            chatRequestUser.lastSeen = lastSeen
        }
        return chatRequestUser
    }
    
    static func getUserList(userRoomid : String, handler:@escaping (_ userList: AllChatModel?)->()) -> UInt{
        // in the firebase don't need to pick the terminal node.. if select top node, then will get all values automatically.
        return ref.child(LIST).child(userRoomid).observe(.childAdded) { (snapshot, error) in
            let childref = snapshot.value as? NSDictionary
            if let childRef = childref {
                //print(childRef)
                if let userlist = parseUserList(childRef){
                    handler(userlist)
                }
                
            } else {
                handler(nil)
            }
        }
    }
    
    static func removeUserlistObserver(userRoomid: String, _ handle : UInt) {
        ref.child(LIST).child(userRoomid).removeObserver(withHandle: handle)
    }
    
    static func parseUserList(_ snapshot: NSDictionary) -> AllChatModel? {
        if  let id = snapshot["id"] as? String,let userName = snapshot["sender_name"] as? String, let userAvatar = snapshot["sender_photo"] as? String, let messageTime = snapshot["time"] as? String, let content = snapshot["message"] as? String, let currentAge = snapshot["sender_age"] as? String,let gender = snapshot["sender_gender"] as? String,let kinkRole = snapshot["sender_kinkRole"] as? String, let messageNum = snapshot["messageNum"] as? String,let location = snapshot["sender_location"] as? String, let pronoun = snapshot["sender_pronoun"] as? String, let lastSeen = snapshot["time"] as? String, let isBlock = snapshot["isBlock"] as? String  {
            let partner = AllChatModel(id: id, userName: userName, userAvatar: userAvatar, duration: messageTime, content: content,currentAge: currentAge,kinkRole: kinkRole, gender: gender, messageNum: messageNum, pronoun: pronoun,location: location, lastSeen: lastSeen, isBlock: isBlock)
            return partner
        }else{
            return nil
        }
    }

    // MARK: - send Chat
    static func sendMessage(_ chat:[String:String], _ roomId: String, completion: @escaping (_ status: Bool, _ message: String) -> ()) {

        ref.child(MESSAGE).child(roomId).childByAutoId().setValue(chat) { (error, dataRef) in
            if let error = error {
                completion(false, error.localizedDescription)
            } else {
                print(dataRef)
                let totalPath: String = "\(dataRef)"
//                let totalPathArr = totalPath.split(separator: "/")
//                print(totalPathArr.first)
//                print(totalPathArr.last)
                completion(true, totalPath)
            }
        }
    }
    
    static func sendListUpdate(_ chat:[String:String], _ myid: String,partnerid : String, completion: @escaping (_ status: Bool, _ message: String) -> ()) {

        ref.child(LIST).child(myid).child(partnerid).setValue(chat) { (error, dataRef) in
            if let error = error {
                completion(false, error.localizedDescription)
            } else {
                completion(true, "Userlist updated successfully.")
            }
        }
    }
    
    static func sendChatRequestListUpdate(_ chatRequest:[String:String], myid: String,partnerid : String, completion: @escaping (_ status: Bool, _ message: String) -> ()) {

        ref.child(CHAT_REQUEST).child(myid).child(partnerid).setValue(chatRequest) { (error, dataRef) in
            if let error = error {
                completion(false, error.localizedDescription)
            } else {
                completion(true, "UserChatRequestlist updated successfully.")
            }
        }
    }
//
//    static func registerUser(_ userinfo:[String:String] , _ id: String, completion: @escaping (_ status: Bool, _ message: String) -> ()) {
//        ref.child(CONV_REFERENCE).child(USER_LIST).childByAutoId().setValue(userinfo) { (error, dataRef) in
//            if let error = error {
//                completion(false, error.localizedDescription)
//            } else {
//                completion(true, "register success!")
//            }
//        }
//    }
//    static func removeRoyaltyObserver(_ roomId: String, _ handle : UInt) {
//        ref.child(CONV_REFERENCE).child(CHAT_CONTENT).child(roomId).removeObserver(withHandle: handle)
//    }
    
    static func detectChatRoomValueChanage(userRoomid : String, handler:@escaping (_ msg: ChatModel)->()) -> UInt {
        // in the firebase don't need to pick the terminal node.. if select top node, then will get all values automatically.
        return ref.child(MESSAGE).child(userRoomid).observe(.childChanged) { (snapshot, error) in

            let childref = snapshot.value as? NSDictionary
            if let childRef = childref {
                //print(childRef)
                let chatModel = parseMsg(childRef)
                handler(chatModel)
            } else {
                handler(ChatModel())
            }
        }
    }
    
    static func removeChangeRoomObserver(_ roomId: String, _ handle : UInt) {
        ref.child(MESSAGE).child(roomId).removeObserver(withHandle: handle)
    }
    
    // random chatting node
    static func getRandomUserList( handler:@escaping (_ userList: RandomUserModel?)->()) -> UInt{
        // in the firebase don't need to pick the terminal node.. if select top node, then will get all values automatically.
        return ref.child(RANDOM_CHAT).observe(.value) { (snapshot, error) in
            let childref = snapshot.value as? NSDictionary
            if let childRef = childref {
                //print(childRef)
                for one in childRef{
                    if let userlist = parseRandomUserList(one.value as! NSDictionary){
                        handler(userlist)
                    }
                }
            } else {
                handler(nil)
            }
        }
    }
    
    static func removeRandomUserlistObserver(_ handle : UInt) {
        ref.child(RANDOM_CHAT).removeObserver(withHandle: handle)
    }
    
    static func parseRandomUserList(_ snapshot: NSDictionary) -> RandomUserModel? {
        if  let id = snapshot["id"] as? String,let response = snapshot["response"] as? String, let room_id = snapshot["room_id"] as? String, let status = snapshot["status"] as? String{
            let partner = RandomUserModel(id: id, response: response, room_id: room_id, status: status)
            return partner
        }else{
            return nil
        }
    }
    
    static func getPartnerBlockStatusValueChanage(handler:@escaping ()->()) -> UInt {
        // in the firebase don't need to pick the terminal node.. if select top node, then will get all values automatically.
    return ref.child(LIST).observe(.childChanged) { (snapshot, error) in
            let key = snapshot.key
            var roomid = ""
            if let id = thisuser?.id{
                roomid = "u" + id
            }
            
            if key != roomid{
                let childref = snapshot.value as? NSDictionary
                if let childRef = childref {
                    //print(childRef)
                    //print(childRef.allKeys)
                    //print(childRef.allValues)
                    if let node = childRef.allKeys.first,let key1 = node as? String, key1 == roomid{
                        if let value = childRef.allValues.first, let dict = value as? NSDictionary, let is_block = dict["isBlock"] as? String{
                            if is_block == "true"{
                                print("user \(key)===>blcked")
                                
                                if var blocked = blockusers.getBlockedUsers(), let userid = key.split(separator: "u").first{
                                    if blocked.count != 0{
                                        var exist = false
                                        var num = 0
                                        for one in blocked{
                                            num += 1
                                            if one == userid{
                                                exist = true
                                            }
                                            if num == blocked.count{
                                                if !exist{
                                                    blocked.append(String(userid))
                                                    UserDefault.setString(key: PARAMS.BLOCKEDUSERS, value: blocked.joined(separator: ","))
                                                    UserDefault.Sync()
                                                    blockusers.loadUserInfo()
                                                    blockusers.saveUserInfo()
                                                }
                                            }
                                        }
                                    }else{
                                        UserDefault.setString(key: PARAMS.BLOCKEDUSERS, value: String(userid))
                                        UserDefault.Sync()
                                        blockusers.loadUserInfo()
                                        blockusers.saveUserInfo()
                                    }
                                }
                            }else if is_block == "false"{
                                if var blocked = blockusers.getBlockedUsers(), let userid = key.split(separator: "u").first{
                                    if blocked.count != 0{
                                        var exist = false
                                        var num = 0
                                        var indexx = -1
                                        for one in blocked{
                                            num += 1
                                            if one == userid{
                                                exist = true
                                                indexx = num - 1
                                            }
                                            if num == blocked.count{
                                                if exist{
                                                    if indexx != -1{
                                                        blocked.remove(at: indexx)
                                                        UserDefault.setString(key: PARAMS.BLOCKEDUSERS, value: blocked.joined(separator: ","))
                                                        UserDefault.Sync()
                                                        blockusers.loadUserInfo()
                                                        blockusers.saveUserInfo()
                                                    }
                                                }
                                            }
                                        }
                                    }else{
                                        UserDefault.setString(key: PARAMS.BLOCKEDUSERS, value: "")
                                        UserDefault.Sync()
                                        blockusers.loadUserInfo()
                                        blockusers.saveUserInfo()
                                    }
                                }
                            }
                        }
                    }
                    handler()
                } else {
                    handler()
                }
            }
        }
    }
    
    static func removePartnerBlockStatusValueChanage(partnerid: String, _ handle : UInt) {
        ref.child(LIST).child("u" + partnerid).child("u" + thisuser!.id!).removeObserver(withHandle: handle)
    }
}
