//
//  RegisterModel.swift
//  DatingKinky
//
//  Created by Ubuntu on 7/29/20.
//  Copyright © 2020 Ubuntu. All rights reserved.
//

import Foundation


class RegisterModel: NSObject{
    
    var username: String?
    var password: String?
    var email: String?
    var dateofBirth: String?
    var location: String?
    var coordinates: CoordinateModel?
    var accountType: String?
    var role: String?
    var fosta: Bool = true
    
    override init() {
        super.init()
        username = ""
        password = ""
        email = ""
        dateofBirth = ""
        location = ""
        coordinates = nil
        accountType = ""
        role = ""
        fosta = true
    }
    
    init(username: String, password: String, email: String, dateofBirth: String, location: String, coordinate: CoordinateModel?, accountType: String,role: String, fosta: Bool){
        self.username = username
        self.password = password
        self.email = email
        self.dateofBirth = dateofBirth
        self.location = location
        self.coordinates = coordinate
        self.accountType = accountType
        self.role = role
        self.fosta = fosta
    }
}

class CoordinateModel: NSObject {
    var lat: String?
    var lng: String?
    
    override init() {
        super.init()
        lat = ""
        lng = ""
    }
    init(lat: String, lng: String){
        self.lat = lat
        self.lng = lng
    }
}

class PhoneModel: NSObject {
    var dialingCode: String?
    var phoneNumber: String?
    
    override init() {
        super.init()
        dialingCode = ""
        phoneNumber = ""
    }
    
    init(dialingCode: String, phoneNumber: String){
        self.dialingCode = dialingCode
        self.phoneNumber = phoneNumber
    }
}
