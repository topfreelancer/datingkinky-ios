//
//  LocationModel.swift
//  DatingKinky
//
//  Created by Ubuntu on 7/26/20.
//  Copyright © 2020 Ubuntu. All rights reserved.
//

import Foundation
import SwiftyJSON

class LocationModel {
    
    var str_locationName = ""
    var str_locationLat = ""
    var str_locationLong = ""
    
    init(_ jsonData : JSON) {
        
        self.str_locationName = jsonData["place_name"].stringValue
        self.str_locationLat = jsonData["center"].arrayValue[0].stringValue
        self.str_locationLong = jsonData["center"].arrayValue[1].stringValue
    }
}
