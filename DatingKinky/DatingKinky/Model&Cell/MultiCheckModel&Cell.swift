//
//  MultiCheckModel&Cell.swift
//  DatingKinky
//
//  Created by Ubuntu on 7/27/20.
//  Copyright © 2020 Ubuntu. All rights reserved.
//

import Foundation
import SwiftyJSON
import GDCheckbox

class MultiCheckModel {
    
    var str_checkOption = ""
    var str_checkId = ""
    var checked = false
    /*init(_ jsonData : JSON) {
        
        self.str_locationLat = jsonData["center"].arrayValue[0].stringValue
        self.str_locationLong = jsonData["center"].arrayValue[1].stringValue
    }*/
    init(_ checkOption: String,checkId: String,  checked: Bool){
        
        self.str_checkOption = checkOption
        self.str_checkId = checkId
        self.checked = checked
    }
}

class MultiCheckCell: UICollectionViewCell {
    
    @IBOutlet weak var cus_checkBox: GDCheckbox!
    @IBOutlet weak var lbl_checkOption: UILabel!
    
    
    var entity:MultiCheckModel!{
        didSet{
            self.lbl_checkOption.text = entity.str_checkOption
            self.cus_checkBox.isOn = entity.checked
        }
    }
}
