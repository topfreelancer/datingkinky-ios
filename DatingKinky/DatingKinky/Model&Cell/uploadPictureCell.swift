//
//  uploadPictureCell.swift
//  DatingKinky
//
//  Created by top Dev on 11.12.2020.
//  Copyright © 2020 Ubuntu. All rights reserved.
//

import Foundation
import SwiftyJSON

class PortfolioModel: NSObject {
        var id: String?
        var imageFile: UIImage?
    
       override init() {
           super.init()
            id = "0"
            imageFile = nil
       }
        init(_ imagefile: UIImage) {
            self.imageFile = imagefile
        }
}


class uploadPictureCell: UICollectionViewCell {
    
    @IBOutlet var imv_portfolio: UIImageView!
    @IBOutlet var btn_close: UIButton!
    
    var entity: PortfolioModel!{
        
        didSet{
            if let image = entity.imageFile{
                imv_portfolio.image = image
            }
        }
    }
}
