//
//  UserModel.swift
//  EveraveUpdate
//
//  Created by Ubuntu on 1/18/20.
//  Copyright © 2020 Ubuntu. All rights reserved.
//

import Foundation
import SwiftyJSON

class UserModel: NSObject {
    
    var id: String? = nil
    var hasAvatar: Bool = false
    var hasfilledaboutMe: Bool = false
    var hasFilledMatchingData: Bool = false
    var hasLoggedInonce: Bool = false
    var hasPlusMembership: Bool = false
    var hasPrimaryAudioGreeting: Bool = false
    var hasPrimaryVideoGreetiing: Bool = false
    var loginCount: Int = 0
    var plusPaymentRecord: PlusPaymentRecordModel?
    var user: usercoreModel?
    var username = ""
    var password = ""
    var userAvatar = ""
    var gender = ""
    var pronoun = ""
    var location = ""
    var currentAge = ""
    var kinkRole = ""
    
    override init() {
       super.init()
        id = ""
        hasAvatar = false
        hasfilledaboutMe = false
        hasFilledMatchingData = false
        hasLoggedInonce = false
        hasPlusMembership = false
        hasPrimaryAudioGreeting = false
        hasPrimaryVideoGreetiing = false
        loginCount = 0
        plusPaymentRecord = nil
        user = nil
        password = ""
        username = ""
        userAvatar = ""
        gender = ""
        pronoun = ""
        location = ""
        currentAge = ""
        kinkRole = ""
    }
       
    init(_ json : JSON) {
        self.id = UserDefault.getString(key: PARAMS._ID, defaultValue: "") ?? ""
        self.hasAvatar = json[PARAMS.HAS_AVATAR].boolValue
        self.hasfilledaboutMe = json[PARAMS.HAS_FILLED_ABOUTME].boolValue
        self.hasFilledMatchingData = json[PARAMS.HAS_FILLED_MATCHING_DATA].boolValue
        self.hasLoggedInonce = json[PARAMS.HAS_LOGGEDIN_ONCE].boolValue
        self.hasPlusMembership = json[PARAMS.HAS_PLUS_MEMBERSHIP].boolValue
        self.hasPrimaryAudioGreeting = json[PARAMS.HAS_PRIMARY_AUDIO_GREETING].boolValue
        self.hasPrimaryVideoGreetiing = json[PARAMS.HAS_PRIMARY_VIDEO_GREETING].boolValue
        self.loginCount = json[PARAMS.LOGIN_COUNT].intValue
        self.plusPaymentRecord = PlusPaymentRecordModel(JSON(json[PARAMS.PLUS_PAYMENT_RECORD].object))
        self.user = usercoreModel(JSON(json[PARAMS.USER].object))
        self.username = UserDefault.getString(key: PARAMS.USERNAME, defaultValue: "") ?? ""
        self.password = UserDefault.getString(key: PARAMS.PASSWORD, defaultValue: "") ?? ""
        self.userAvatar = UserDefault.getString(key: PARAMS.AVATAR, defaultValue: "") ?? ""
        self.gender = UserDefault.getString(key: PARAMS.GENDER, defaultValue: "") ?? ""
        self.pronoun = UserDefault.getString(key: PARAMS.PRONOUN, defaultValue: "") ?? ""
        self.location = UserDefault.getString(key: PARAMS.LOCATION, defaultValue: "") ?? ""
        self.currentAge = UserDefault.getString(key: PARAMS.CURRENTAGE, defaultValue: "") ?? ""
        self.kinkRole = UserDefault.getString(key: PARAMS.KINKROLE, defaultValue: "") ?? ""
    }
    // Check and returns if user is valid user or not
   var isValid: Bool {
        return self.user?._id != nil && self.user?._id != ""
   }
    
    // Save user credential to UserDefault
    func saveUserInfo() {
        UserDefault.setString(key: PARAMS._ID , value: id)
        UserDefault.setBool(key: PARAMS.HAS_AVATAR, value: hasAvatar)
        UserDefault.setBool(key: PARAMS.HAS_FILLED_ABOUTME, value: hasfilledaboutMe)
        UserDefault.setBool(key: PARAMS.HAS_FILLED_MATCHING_DATA, value: hasFilledMatchingData)
        UserDefault.setBool(key: PARAMS.HAS_LOGGEDIN_ONCE, value: hasLoggedInonce)
        UserDefault.setBool(key: PARAMS.HAS_PLUS_MEMBERSHIP, value: hasPlusMembership)
        UserDefault.setBool(key: PARAMS.HAS_PRIMARY_AUDIO_GREETING, value: hasPrimaryAudioGreeting)
        UserDefault.setBool(key: PARAMS.HAS_PRIMARY_VIDEO_GREETING, value: hasPrimaryVideoGreetiing)
        UserDefault.setInt(key: PARAMS.LOGIN_COUNT, value: loginCount)
        let userDefaults = UserDefaults.standard
        let encodedData4plus: Data = NSKeyedArchiver.archivedData(withRootObject: plusPaymentRecord as Any)
        userDefaults.set(encodedData4plus, forKey: PARAMS.PLUS_PAYMENT_RECORD)
        let encodedData4user: Data = NSKeyedArchiver.archivedData(withRootObject: user as Any)
        userDefaults.set(encodedData4user, forKey: PARAMS.USER)
        userDefaults.synchronize()
        UserDefault.setString(key: PARAMS.USERNAME, value: username)
        UserDefault.setString(key: PARAMS.PASSWORD, value: password)
        UserDefault.setString(key: PARAMS.GENDER, value: gender)
        UserDefault.setString(key: PARAMS.PRONOUN, value: pronoun)
        UserDefault.setString(key: PARAMS.LOCATION, value: location)
        UserDefault.setString(key: PARAMS.CURRENTAGE, value: currentAge)
        UserDefault.setString(key: PARAMS.KINKROLE, value: kinkRole)
    }
    
    // Recover user credential from UserDefault
    func loadUserInfo() {
        id = UserDefault.getString(key: PARAMS._ID,defaultValue: "") ?? ""
        hasAvatar = UserDefault.getBool(key: PARAMS.HAS_AVATAR, defaultValue: false)
        hasfilledaboutMe = UserDefault.getBool(key: PARAMS.HAS_FILLED_ABOUTME, defaultValue: false)
        hasFilledMatchingData = UserDefault.getBool(key: PARAMS.HAS_FILLED_MATCHING_DATA, defaultValue: false)
        hasLoggedInonce = UserDefault.getBool(key: PARAMS.HAS_LOGGEDIN_ONCE, defaultValue: false)
        hasPlusMembership = UserDefault.getBool(key: PARAMS.HAS_PLUS_MEMBERSHIP, defaultValue: false)
        hasPrimaryAudioGreeting = UserDefault.getBool(key: PARAMS.HAS_PRIMARY_AUDIO_GREETING, defaultValue: false)
        hasPrimaryVideoGreetiing = UserDefault.getBool(key: PARAMS.HAS_PRIMARY_VIDEO_GREETING, defaultValue: false)
        loginCount = UserDefault.getInt(key: PARAMS.LOGIN_COUNT,defaultValue: 0)
        
        let userDefaults = UserDefaults.standard
        let decoded4plus  = userDefaults.data(forKey: PARAMS.PLUS_PAYMENT_RECORD)
        if let decoded = decoded4plus{
             plusPaymentRecord = NSKeyedUnarchiver.unarchiveObject(with: decoded) as? PlusPaymentRecordModel
        }
        let decoded4user  = userDefaults.data(forKey: PARAMS.USER)
        if let decoded = decoded4user{
             user = NSKeyedUnarchiver.unarchiveObject(with: decoded) as? usercoreModel
        }
        
        username = UserDefault.getString(key: PARAMS.USERNAME,defaultValue: "") ?? ""
        password = UserDefault.getString(key: PARAMS.PASSWORD,defaultValue: "") ?? ""
        gender = UserDefault.getString(key: PARAMS.GENDER,defaultValue: "") ?? ""
        pronoun = UserDefault.getString(key: PARAMS.PRONOUN,defaultValue: "") ?? ""
        location = UserDefault.getString(key: PARAMS.LOCATION,defaultValue: "") ?? ""
        currentAge = UserDefault.getString(key: PARAMS.CURRENTAGE,defaultValue: "") ?? ""
        kinkRole = UserDefault.getString(key: PARAMS.KINKROLE,defaultValue: "") ?? ""
    }
    
    // Clear save user credential
    func clearUserInfo() {
        id = ""
        hasAvatar = false
        hasfilledaboutMe = false
        hasFilledMatchingData = false
        hasLoggedInonce = false
        hasPlusMembership = false
        hasPrimaryAudioGreeting = false
        hasPrimaryVideoGreetiing = false
        loginCount = 0
        plusPaymentRecord = nil
        user = nil
        username = ""
        password = ""
        gender = ""
        pronoun = ""
        location = ""
        currentAge = ""
        kinkRole = ""
        
        UserDefault.setString(key: PARAMS._ID, value: nil)
        UserDefault.setBool(key: PARAMS.HAS_AVATAR, value: false)
        UserDefault.setBool(key: PARAMS.HAS_FILLED_ABOUTME, value: false)
        UserDefault.setBool(key: PARAMS.HAS_FILLED_MATCHING_DATA, value: false)
        UserDefault.setBool(key: PARAMS.HAS_LOGGEDIN_ONCE, value: false)
        UserDefault.setBool(key: PARAMS.HAS_PLUS_MEMBERSHIP, value: false)
        UserDefault.setBool(key: PARAMS.HAS_PRIMARY_AUDIO_GREETING, value: false)
        UserDefault.setBool(key: PARAMS.HAS_PRIMARY_VIDEO_GREETING, value: false)
        UserDefault.setInt(key: PARAMS.LOGIN_COUNT, value: 0)
        UserDefault.setObject(key: PARAMS.PLUS_PAYMENT_RECORD, value: nil)
        UserDefault.setObject(key: PARAMS.USER, value: nil)
        UserDefault.setString(key: PARAMS.USERNAME, value: "")
        UserDefault.setString(key: PARAMS.PASSWORD, value: "")
        UserDefault.setString(key: PARAMS.GENDER, value: "")
        UserDefault.setString(key: PARAMS.PRONOUN, value: "")
        UserDefault.setString(key: PARAMS.LOCATION, value: "")
        UserDefault.setString(key: PARAMS.CURRENTAGE, value: "")
        UserDefault.setString(key: PARAMS.KINKROLE, value: "")
    }
    
    func updateUserInfo(thisuser: UserModel) {
        id = thisuser.id
        hasAvatar = thisuser.hasAvatar
        hasfilledaboutMe = thisuser.hasfilledaboutMe
        hasFilledMatchingData = thisuser.hasFilledMatchingData
        hasLoggedInonce = thisuser.hasLoggedInonce
        hasPlusMembership = thisuser.hasPlusMembership
        hasPrimaryAudioGreeting = thisuser.hasPrimaryAudioGreeting
        hasPrimaryVideoGreetiing = thisuser.hasPrimaryVideoGreetiing
        loginCount = thisuser.loginCount
        plusPaymentRecord = thisuser.plusPaymentRecord
        user = thisuser.user
        username = thisuser.username
        password = thisuser.password
        gender = thisuser.gender
        pronoun = thisuser.pronoun
        location = thisuser.location
        currentAge = thisuser.currentAge
        kinkRole = thisuser.kinkRole
    }
}
