//
//  BlockedUserModel&Cell.swift
//  DatingKinky
//
//  Created by top Dev on 17.11.2020.
//  Copyright © 2020 Ubuntu. All rights reserved.
//

import Foundation
import SwiftyJSON

class BlockedUserModel: NSObject{
    
    var id: String?
    var userAvatar: String?
    
    override init() {
        self.id = ""
        self.userAvatar = ""
    }
    
    init(id: String, userAvatar : String) {
        self.id = id
        self.userAvatar = userAvatar
    }
}


class BlockedCell: UICollectionViewCell {
    
    @IBOutlet var imv_profile: UIImageView!
    @IBOutlet weak var unBlockBtn: UIButton!
    
    var entity: BlockedUserModel!{
        
        didSet{
            let url = URL(string: entity.userAvatar ?? "")
            imv_profile.kf.setImage(with: url,placeholder: UIImage.init(named: "placeholder"))
        }
    }
}
