//
//  RandomUserModel.swift
//  DatingKinky
//
//  Created by top Dev on 07.12.2020.
//  Copyright © 2020 Ubuntu. All rights reserved.
//

import Foundation
import SwiftyJSON

class RandomUserModel: NSObject{
    
    var id: String?
    var response: String?
    var room_id: String?
    var status: String?
    
    init(id: String, response : String, room_id : String, status: String) {
        
        self.id = id
        self.response = response
        self.room_id = room_id
        self.status = status
    }
}
