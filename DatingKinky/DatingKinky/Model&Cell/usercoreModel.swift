//
//  user.swift
//  DatingKinky
//
//  Created by Ubuntu on 7/28/20.
//  Copyright © 2020 Ubuntu. All rights reserved.
//

import Foundation
import SwiftyJSON

class usercoreModel: NSObject, NSCoding {
    
    var _id = ""
    var username = ""
    var status = ""
    var role: RoleModel?
    var fosta : Bool = true
    var email = ""
    var dateofBirth = ""
    
    
    init(_id : String, username : String, status : String, role: RoleModel?, fosta : Bool, email : String, dateofBirth : String) {
        
        self._id = _id
        self.username = username
        self.status = status
        self.role = role
        self.fosta = fosta
        self.email = email
        self.dateofBirth = dateofBirth
    }
    
    init(_ jsonData : JSON) {
        
        self._id = jsonData[PARAMS._ID].stringValue
        self.username = jsonData[PARAMS.USERNAME].stringValue
        self.status = jsonData[PARAMS.STATUS].stringValue
        self.role = RoleModel(JSON(jsonData[PARAMS.ROLE].object))
        self.fosta = jsonData[PARAMS.FOSTA].boolValue
        self.email = jsonData[PARAMS.EMAIL].stringValue
        self.dateofBirth = jsonData[PARAMS.DATE_OF_BIRTH].stringValue
    }
    
    required convenience init(coder aDecoder: NSCoder) {
        let _id = aDecoder.decodeObject(forKey: PARAMS._ID) as! String
        let username = aDecoder.decodeObject(forKey: PARAMS.USERNAME) as! String
        let status = aDecoder.decodeObject(forKey: PARAMS.STATUS) as! String
        let role = aDecoder.decodeObject(forKey: PARAMS.ROLE) as! RoleModel
        let fosta = aDecoder.decodeObject(forKey: PARAMS.FOSTA) as? Bool ?? true
        let email = aDecoder.decodeObject(forKey: PARAMS.EMAIL) as! String
        let dateofBirth = aDecoder.decodeObject(forKey: PARAMS.DATE_OF_BIRTH) as! String
        
        
        self.init(_id : _id, username : username, status : status, role: role, fosta : fosta, email : email, dateofBirth : dateofBirth)
    }

    func encode(with aCoder: NSCoder) {
        
        aCoder.encode(_id,forKey: PARAMS._ID)
        aCoder.encode(username,forKey: PARAMS.USERNAME)
        aCoder.encode(status,forKey: PARAMS.STATUS)
        aCoder.encode(role,forKey: PARAMS.ROLE)
        aCoder.encode(fosta,forKey: PARAMS.FOSTA)
        aCoder.encode(email,forKey: PARAMS.EMAIL)
        aCoder.encode(dateofBirth,forKey: PARAMS.DATE_OF_BIRTH)
        
    }
}
