//
//  BlockParameterModel.swift
//  DatingKinky
//
//  Created by top Dev on 12.12.2020.
//  Copyright © 2020 Ubuntu. All rights reserved.
//


import Foundation
import SwiftyJSON

class BlockParameterModel: NSObject {
    
    var isblock_video_verify: String?
    var isblock_chat_request: String?
    var snooze_account: String?
    var isblock_random_chat: String?
    var isblock_video_chat: String?
    var isblock_audio_chat: String?
    var snooze_accounts: String?
    
    override init() {
       super.init()
        isblock_video_verify = "0"
        isblock_chat_request = "0"
        snooze_account = "0"
        isblock_random_chat = "0"
        isblock_video_chat = "0"
        isblock_audio_chat = "0"
        snooze_accounts = ""
    }
       
    init(_ json : JSON) {
        let blockstatus = JSON(json["blockstatus"].arrayObject?.first as Any)
        self.isblock_video_verify = blockstatus[PARAMS.ISBLOCK_VIDEOVERIFY].stringValue
        self.isblock_chat_request = blockstatus[PARAMS.ISBLOCK_CHATREQUEST].stringValue
        self.snooze_account = blockstatus[PARAMS.SNOOZE_ACCOUNT].stringValue
        self.isblock_random_chat = blockstatus[PARAMS.ISBLOCK_RANDOMCHAT].stringValue
        self.isblock_video_chat = blockstatus[PARAMS.ISBLOCK_VIDEOCHAT].stringValue
        self.isblock_audio_chat = blockstatus[PARAMS.ISBLOCK_AUDIOCHAT].stringValue
        if let snooze_account = json["snooze_account"].arrayObject{
            if snooze_account.count != 0{
                var snooze_array = [String]()
                snooze_array.removeAll()
                var num = 0
                for one in snooze_account{
                    num += 1
                    let json = JSON(one as Any)
                    snooze_array.append(json["user_id"].stringValue)
                    if num == snooze_array.count{
                        self.snooze_accounts = snooze_array.joined(separator: ",")
                    }
                }
            }else{
                self.snooze_accounts = ""
            }
        }else{
            self.snooze_accounts = ""
        }
    }
    
    // Save user credential to UserDefault
    func saveUserInfo() {
        UserDefault.setString(key: PARAMS.ISBLOCK_VIDEOVERIFY, value: isblock_video_verify)
        UserDefault.setString(key: PARAMS.ISBLOCK_CHATREQUEST, value: isblock_chat_request)
        UserDefault.setString(key: PARAMS.SNOOZE_ACCOUNT, value: snooze_account)
        UserDefault.setString(key: PARAMS.ISBLOCK_RANDOMCHAT, value: isblock_random_chat)
        UserDefault.setString(key: PARAMS.ISBLOCK_VIDEOCHAT, value: isblock_video_chat)
        UserDefault.setString(key: PARAMS.ISBLOCK_AUDIOCHAT, value: isblock_audio_chat)
        UserDefault.setString(key: PARAMS.SNOOZE_ACCOUNTS, value: snooze_accounts)
        UserDefault.Sync()
    }
    
    // Recover user credential from UserDefault
    func loadUserInfo() {
        self.isblock_video_verify = UserDefault.getString(key: PARAMS.ISBLOCK_VIDEOVERIFY, defaultValue: "")
        self.isblock_chat_request = UserDefault.getString(key: PARAMS.ISBLOCK_CHATREQUEST, defaultValue: "")
        self.snooze_account = UserDefault.getString(key: PARAMS.SNOOZE_ACCOUNT, defaultValue: "")
        self.isblock_random_chat = UserDefault.getString(key: PARAMS.ISBLOCK_RANDOMCHAT, defaultValue: "")
        self.isblock_video_chat = UserDefault.getString(key: PARAMS.ISBLOCK_VIDEOCHAT, defaultValue: "")
        self.isblock_audio_chat = UserDefault.getString(key: PARAMS.ISBLOCK_AUDIOCHAT, defaultValue: "")
        self.snooze_accounts = UserDefault.getString(key: PARAMS.SNOOZE_ACCOUNTS, defaultValue: "")
    }
    
    // Clear save user credential
    func clearUserInfo() {
        isblock_video_verify = ""
        isblock_chat_request = ""
        snooze_account = ""
        isblock_random_chat = ""
        isblock_video_chat = ""
        isblock_audio_chat = ""
        snooze_accounts = ""
        
        UserDefault.setString(key: PARAMS.ISBLOCK_VIDEOVERIFY, value: nil)
        UserDefault.setString(key: PARAMS.ISBLOCK_CHATREQUEST, value: nil)
        UserDefault.setString(key: PARAMS.SNOOZE_ACCOUNT, value: nil)
        UserDefault.setString(key: PARAMS.ISBLOCK_RANDOMCHAT, value: nil)
        UserDefault.setString(key: PARAMS.ISBLOCK_VIDEOCHAT, value: nil)
        UserDefault.setString(key: PARAMS.ISBLOCK_AUDIOCHAT, value: nil)
        UserDefault.setString(key: PARAMS.SNOOZE_ACCOUNTS, value: nil)
        UserDefault.Sync()
    }
}

