//
//  AppDelegate.swift
//  DatingKinky
//
//  Created by Ubuntu on 7/22/20.
//  Copyright © 2020 Ubuntu. All rights reserved.
//

import UIKit
import CoreData
import FirebaseCore
import AlamofireNetworkActivityIndicator
import IQKeyboardManagerSwift
import FirebaseMessaging
//import EBBannerView
import UserNotifications
import BRYXBanner
import SwiftyJSON
import GooglePlaces

var thisuser:UserModel?
var block_parameters: BlockParameterModel?
var blockusers: BlockuserModel!
var usermembership: UserMembershipModel!
var deviceTokenString = ""
var request_user_name: String?
var request_user_id: String?

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    let application = UIApplication.shared

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        IQKeyboardManager.shared.enable = true
        IQKeyboardManager.shared.enableAutoToolbar = false
        IQKeyboardManager.shared.enableDebugging = false
        FirebaseApp.configure()
        
        // user setting
        thisuser = UserModel()
        thisuser?.loadUserInfo()
        //TODO: must be change later
        thisuser?.saveUserInfo()
        // for tenor checking
        block_parameters = BlockParameterModel()
        blockusers = BlockuserModel()
        blockusers.clearUserInfo()
        usermembership = UserMembershipModel()
        usermembership.clearUserInfo()
        Configuration.checkConfiguration()
        NetworkActivityIndicatorManager.shared.isEnabled = true
        // set local verification token as nil
        UserDefault.setString(key: PARAMS.VERIFY_TOKEN, value: nil)
        // push settings
        let notificationTypes: UIUserNotificationType = [UIUserNotificationType.alert,UIUserNotificationType.badge, UIUserNotificationType.sound]
        let pushNotificationSettings = UIUserNotificationSettings(types: notificationTypes, categories: nil)

        application.registerUserNotificationSettings(pushNotificationSettings)
        //UIApplication.shared.applicationIconBadgeNumber = 0
        registerForPushNotifications()
        application.applicationIconBadgeNumber = 0
        // Override point for customization after application launch.
        do {
            try AVAudioSession.sharedInstance().setCategory(AVAudioSession.Category.playAndRecord, options: [AVAudioSession.CategoryOptions.duckOthers, AVAudioSession.CategoryOptions.defaultToSpeaker]);
        }
        catch {}
        
        GMSPlacesClient.provideAPIKey(GOOGLE_PLACES_API_KEY)
        
        return true
    }
    // MARK: UISceneSession Lifecycle

    @available(iOS 13.0, *)
    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    }

    @available(iOS 13.0, *)
    func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
    }
    
    //MARK:-   set push notifations
    func registerForPushNotifications() {
        
        if #available(iOS 10.0, *) {
            UNUserNotificationCenter.current().delegate = self

            let authOptions: UNAuthorizationOptions = [.alert, .sound, .badge]//[.alert, .badge, .sound]
            UNUserNotificationCenter.current().requestAuthorization(options: authOptions, completionHandler: {(granted, error) in
                if granted {
                    print("Permission granted: \(granted)")
                    DispatchQueue.main.async() {
                        UIApplication.shared.registerForRemoteNotifications()
                    }
                }
            })
            
            Messaging.messaging().delegate = self
            //Messaging.messaging().shouldEstablishDirectChannel = true
            
        } else {
            let settings: UIUserNotificationSettings = UIUserNotificationSettings(types: [.alert, .sound], categories: nil)
            UIApplication.shared.registerUserNotificationSettings(settings)
        }
    }
        
    func getRegisteredPushNotifications() {
        UNUserNotificationCenter.current().getNotificationSettings(completionHandler: { settings in
            switch settings.authorizationStatus {
                case .authorized, .provisional:
                    print("The user agrees to receive notifications.")
                    DispatchQueue.main.async {
                        UIApplication.shared.registerForRemoteNotifications()
                    }
                case .denied:
                    print("Permission denied.")
                    // The user has not given permission. Maybe you can display a message remembering why permission is required.
                case .notDetermined:
                    print("The permission has not been determined, you can ask the user.")
                    self.getRegisteredPushNotifications()
                default:
                    return
            }
        })
    }

    func application(_ application: UIApplication, didRegister notificationSettings: UIUserNotificationSettings) {
        if notificationSettings.types != [] {
            application.registerForRemoteNotifications()
        }
    }
        
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        print("Successfully registered for notifications!")
        let tokenChars = (deviceToken as NSData).bytes.bindMemory(to: CChar.self, capacity: deviceToken.count)
        var tokenString = ""

        for i in 0..<deviceToken.count {
            tokenString += String(format: "%02.2hhx", arguments: [tokenChars[i]])
        }
    }

    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        print("Failed to register for notifications: \(error.localizedDescription)")
    }
    
    // MARK: - Core Data stack

    /*lazy var persistentContainer: NSPersistentCloudKitContainer = {
        /*
         The persistent container for the application. This implementation
         creates and returns a container, having loaded the store for the
         application to it. This property is optional since there are legitimate
         error conditions that could cause the creation of the store to fail.
        */
        let container = NSPersistentCloudKitContainer(name: "EveraveUpdate")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                 
                /*
                 Typical reasons for an error here include:
                 * The parent directory does not exist, cannot be created, or disallows writing.
                 * The persistent store is not accessible, due to permissions or data protection when the device is locked.
                 * The device is out of space.
                 * The store could not be migrated to the current model version.
                 Check the error message to determine what the actual problem was.
                 */
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()*/

    // MARK: - Core Data Saving support

    /*func saveContext () {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }*/
}

extension AppDelegate : UNUserNotificationCenterDelegate {
    
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                willPresent notification: UNNotification,
                                withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        let userInfo = notification.request.content.userInfo
        print("thisis userinfo===>",userInfo)
        if let sender_id = userInfo["sender_id"] as? AnyHashable, let sender_name = userInfo["sender_name"] as? AnyHashable,let sender_avatar = userInfo["sender_avatar"] as? AnyHashable,let sender_age = userInfo["sender_age"] as? AnyHashable,let sender_gender = userInfo["sender_gender"] as? AnyHashable,let sender_kinkrole = userInfo["sender_kinkrole"] as? AnyHashable,let sender_location = userInfo["sender_location"] as? AnyHashable,let sender_pronoun = userInfo["sender_pronoun"] as? AnyHashable{
            if let _sender_id = sender_id as? String,let _sender_name = sender_name as? String,let _sender_avatar = sender_avatar as? String,let _sender_age = sender_age as? String,let _sender_gender = sender_gender as? String,let _sender_kinkrole = sender_kinkrole as? String,let _sender_location = sender_location as? String,let _sender_pronoun = sender_pronoun as? String{
                // for list update
                request_user_id = _sender_id
                friendID = _sender_id
                memberName4ChatRequest = _sender_name
                partnerAvatar4ChatRequest = _sender_avatar
                partnerAge4ChatRequestAccept = _sender_age
                partnerGender4ChatRequest = _sender_gender
                partnerKinkRole4ChatRequest = _sender_kinkrole
                partnerLocation4ChatRequest = _sender_location
                partnerPronoun4ChatRequest = _sender_pronoun
                // for messagesend vc parameters
                let gender = _sender_gender == "woman" ? "W" : "M"
                let spaces = String(repeating: " ", count: 1)
                memberProfile4ChatRequest = _sender_age + gender + spaces + _sender_kinkrole + spaces + "•" + spaces + _sender_pronoun + spaces + "•" + spaces + _sender_location + spaces + "•" + spaces + "Last seen"
                lastSeenChatRequest = "now"
            }
        }
        
        let aps             = userInfo["aps"] as? [AnyHashable : Any]
        //let badgeCount      = aps!["badge"] as? Int ?? 0
        
        let alertMessage    = aps!["alert"] as? [AnyHashable : Any]
        if let alertMessage = alertMessage{
            let bodyMessage     = alertMessage["body"] as? String
            let titleMessage    = alertMessage["title"] as? String
            if let bodyMessage = bodyMessage, let titleMessage = titleMessage{
                print(titleMessage)
                print(bodyMessage)
                
                request_user_name = String(bodyMessage.split(separator: "!")[1].split(separator: " ").first ?? "")
                NotificationCenter.default.post(name: Notification.Name("changedBadgeCount"), object: nil)
                
                /*let banner = EBBannerView.banner({ (make) in
                    make?.style     = EBBannerViewStyle(rawValue: 60)
                    make?.icon      = UIImage(named: "AppIcon")
                    make?.title     = titleMessage
                    make?.content   = bodyMessage
                    make?.date      = "Now"
                })
                 
                banner?.show()*/
                var banner = Banner()
                if #available(iOS 11.0, *) {
                     banner = Banner(title: titleMessage, subtitle: bodyMessage, image: UIImage(named: "placeholder"), backgroundColor: UIColor.init(named: "color_navbar_top")!)
                } else {
                    // Fallback on earlier versions
                    banner = Banner(title: titleMessage, subtitle: bodyMessage, image: UIImage(named: "placeholder"), backgroundColor: UIColor.blue)
                }
                banner.dismissesOnTap = true
                banner.show()
                if block_parameters!.snooze_account == "1"{
                    banner.dismiss()
                }else{
                    if titleMessage == BadgeTitleType.verify_request_received.rawValue{
                        if block_parameters!.isblock_video_verify != "1"{
                            banner.didTapBlock = {
                                NotificationCenter.default.post(name: .videoVerifyRequestReceived, object: nil)
                            }
                        }else{
                            banner.dismiss()
                        }
                    }else if titleMessage == BadgeTitleType.verify_request_accepted.rawValue{
                        banner.didTapBlock = {
                            NotificationCenter.default.post(name: .videoVerifyRequestAccepted, object: nil)
                        }
                    }else if titleMessage == BadgeTitleType.random_chat_received.rawValue{
                        if block_parameters!.isblock_random_chat != "1"{
                            banner.didTapBlock = {
                                NotificationCenter.default.post(name: .randomChattingPopDialog, object: nil)
                            }
                        }else{
                            banner.dismiss()
                        }
                    }else if titleMessage == BadgeTitleType.chat_request_received.rawValue{// general chatting request
                        if block_parameters!.isblock_chat_request != "1"{
                            banner.didTapBlock = {
                                NotificationCenter.default.post(name: .generalChatRequestReceived, object: nil)
                            }
                        }else{
                            banner.dismiss()
                        }
                    }else if titleMessage == BadgeTitleType.chat_request_accepted.rawValue{
                        // here must update list and goto message send vc
                        // MARK: for list view for my list object - - listobject
                        banner.didTapBlock = {
                            var listObject = [String: String]()
                            let timeNow = Int(NSDate().timeIntervalSince1970) * 1000
                            listObject["id"]   = friendID!
                            listObject["message"]        = "Hi!"
                            if let friendid = friendID{
                               listObject["sender_id"]     = friendid
                            }
                            if let senderName = memberName4ChatRequest{
                               listObject["sender_name"]    = senderName
                            }
                            if let senderPhoto = partnerAvatar4ChatRequest{
                               listObject["sender_photo"]  = senderPhoto
                            }
                            if let senderAge = partnerAge4ChatRequestAccept{
                               listObject["sender_age"]  = senderAge
                            }
                            if let senderGender = partnerGender4ChatRequest{
                               listObject["sender_gender"] = senderGender == "woman" ? "W" : "M"
                            }
                            if let senderKinkrole = partnerKinkRole4ChatRequest{
                               listObject["sender_kinkRole"]  = senderKinkrole
                            }
                            if let senderLocation = partnerLocation4ChatRequest{
                               listObject["sender_location"]  = senderLocation
                            }
                            if let senderPronoun = partnerPronoun4ChatRequest{
                               listObject["sender_pronoun"]  = senderPronoun
                            }
                            listObject["time"]           = "\(timeNow)" as String
                            let messageNumLocal = "0"
                            listObject["messageNum"]         = messageNumLocal
                            listObject["isBlock"]            = "false"
                            FirebaseAPI.sendListUpdate(listObject, "u" + "\(thisuser!.id!)", partnerid: "u" + friendID!){
                                (status,message) in
                                if status{
                                    var listObject1 = [String: String]()
                                    let partnerid   = "\(friendID ?? "0")"
                                    // MARK:  for list view for partner's list object - listobject1
                                    listObject1["id"]              = "\(thisuser!.id!)"
                                    listObject1["message"]         = "Hi!"
                                    listObject1["sender_id"]       = "\(thisuser!.id!)"
                                    listObject1["sender_name"]     = "\(thisuser!.username)"
                                    listObject1["sender_photo"]    = thisuser!.userAvatar
                                    listObject1["sender_age"]      = thisuser!.currentAge
                                    listObject1["sender_gender"]   = thisuser!.gender  == "woman" ? "W" : "M"
                                    listObject1["sender_kinkRole"] = thisuser!.kinkRole
                                    listObject1["sender_location"] = thisuser!.location
                                    listObject1["sender_pronoun"]  = thisuser!.pronoun
                                    listObject1["time"]            = "\(timeNow)" as String
                                    listObject1["messageNum"]      = messageNumLocal
                                    listObject1["isBlock"]         = "false"
                                    FirebaseAPI.sendListUpdate(listObject1, "u" + partnerid, partnerid: "u" + "\(thisuser!.id!)"){
                                       (status,message) in
                                       if status{
                                            let storyboad = UIStoryboard(name: StoryBoards.CHAT, bundle: nil)
                                            let messageNav = storyboad.instantiateViewController(withIdentifier: VCs.MESSAGESENDNAV)
                                            UIApplication.shared.keyWindow?.rootViewController = messageNav
                                        }
                                    }
                                }
                            }
                        }
                    }else if titleMessage == BadgeTitleType.videochat_request_received.rawValue{
                        if block_parameters!.isblock_video_chat != "1"{
                            banner.didTapBlock = {
                                NotificationCenter.default.post(name: .videoChatRequestReceived, object: nil)
                            }
                        }else{
                            banner.dismiss()
                        }
                    }else if titleMessage == BadgeTitleType.voicechat_request_received.rawValue{
                        if block_parameters!.isblock_audio_chat != "1"{
                            banner.didTapBlock = {
                                NotificationCenter.default.post(name: .voiceChatRequestReceived, object: nil)
                            }
                        }else{
                            banner.dismiss()
                        }
                    }else if titleMessage == BadgeTitleType.videochat_request_accepted.rawValue{
                        banner.didTapBlock = {
                            NotificationCenter.default.post(name: .videoChatRequestAccepted, object: nil)
                        }
                    }else if titleMessage == BadgeTitleType.voicechat_request_accepted.rawValue{
                        banner.didTapBlock = {
                            NotificationCenter.default.post(name: .voiceChatRequestAccepted, object: nil)
                        }
                    }
                }
                completionHandler([])
            }
        }
    }
}
//
extension AppDelegate : MessagingDelegate {
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String?) {
        Messaging.messaging().subscribe(toTopic: "all")
        if let fcmToken = fcmToken{
            deviceTokenString = fcmToken
            print("fcmToken: ", fcmToken)
        }
        
        
        
//        let sender = PushNotificationSender()
//        let tk = "ffK7OmRn30cRt2vja4kT9P:APA91bEsrvHzlqF3k6iLNNVJJuU4DuXqNxfrICShG5sOK1HkAGaKKN6Sr6vM85VII2Av31D-QdLGwGCuCRREQAyqTwZoXGn8j1oHFLc3kzg8vd3oDHNzKLdiVrBrQUuddkNvuKzGWI1Y"
//        sender.sendPushNotification(to: tk, title: APP_NAME, body: CONSTANT.NOTI_BODY, badgeCount: 5)
    }
}

// for first show within 20 mins admove show
extension AppDelegate {
    
}

enum BadgeTitleType: String {
    case verify_request_accepted    = "Verify request accept"
    case verify_request_rejected    = "Verify request reject"
    case verify_request_received    = "New verify request"
    case chat_request_accepted      = "Chat request accept"
    case chat_request_rejected      = "Chat request reject"
    case chat_request_received      = "New chat request"
    case random_chat_received       = "RandomChat Request"
    case videochat_request_received = "Video chat request"
    case videochat_request_rejected = "Video chat request reject"
    case videochat_request_accepted = "Video chat request accept"
    case voicechat_request_received = "Voice chat request"
    case voicechat_request_rejected = "Voice chat request reject"
    case voicechat_request_accepted = "Voice chat request accept"
}


