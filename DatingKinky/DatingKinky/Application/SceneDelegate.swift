//
//  SceneDelegate.swift
//  DatingKinky
//
//  Created by Ubuntu on 7/22/20.
//  Copyright © 2020 Ubuntu. All rights reserved.
//

import UIKit

class SceneDelegate: UIResponder, UIWindowSceneDelegate {

    var window: UIWindow?


    @available(iOS 13.0, *)
    func scene(_ scene: UIScene, willConnectTo session: UISceneSession, options connectionOptions: UIScene.ConnectionOptions) {
        guard let _ = (scene as? UIWindowScene) else { return }
    }

    @available(iOS 13.0, *)
    func sceneDidDisconnect(_ scene: UIScene) {
        print("sceneDidDisconnect")
    }

    @available(iOS 13.0, *)
    func sceneDidBecomeActive(_ scene: UIScene) {
        print("sceneDidBecomeActive")
        ApiManager.uploadToken { (isSuccess, data) in
            if isSuccess{
                print("upload again")
            }
        }
    }

    @available(iOS 13.0, *)
    func sceneWillResignActive(_ scene: UIScene) {
        print("sceneWillResignActive")
    }

    @available(iOS 13.0, *)
    func sceneWillEnterForeground(_ scene: UIScene) {
        print("sceneWillEnterForeground")
    }

    @available(iOS 13.0, *)
    func sceneDidEnterBackground(_ scene: UIScene) {
        print("sceneDidEnterBackground")
    }
}

